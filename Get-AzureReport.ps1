#########################################
#                                       #
#            VARIABLES                  #
#                                       #
#########################################
# Parameter help description
Param (
    [Parameter(ValueFromPipelineByPropertyName)]
    [system.object]$Office365Access
)

#Location the report will be saved to
$ReportSavePath = "C:\Automation\"

#Variable to filter licenses out, in current state will only get licenses with a count less than 9,000 this will help filter free/trial licenses
$LicenseFilter = "9000"

##Variable needed for report
$LicenseTable = New-Object 'System.Collections.Generic.List[System.Object]'
$IsLicensedUsersTable = New-Object 'System.Collections.Generic.List[System.Object]'
$global:GlobalAdminTable = New-Object 'System.Collections.Generic.List[System.Object]'
$global:CompanyInfoTable = New-Object 'System.Collections.Generic.List[System.Object]'
$global:DomainTable = New-Object 'System.Collections.Generic.List[System.Object]'
$global:LicenseGroupsTable = New-Object 'System.Collections.Generic.List[System.Object]'
$global:NoAgatGroupsTable = New-Object 'System.Collections.Generic.List[System.Object]'

##Fixed groups names for WIP, MCAS, AGAT and Business Voice
$global:WIPGroupName = "M365-Intune WIP Pilot"
$global:MCASGroupName = "M365_MCAS_Pilot"
$global:PhoneSystemGroupName = "App-M365-BusinessVoice"

##Microsoft 365 licenses
$Sku = @{
    "O365_BUSINESS_ESSENTIALS"           = "Office 365 Business Essentials"
    "O365_BUSINESS_PREMIUM"              = "Office 365 Business Premium"
    "DESKLESSPACK"                       = "Office 365 (Plan K1)"
    "DESKLESSWOFFPACK"                   = "Office 365 (Plan K2)"
    "LITEPACK"                           = "Office 365 (Plan P1)"
    "EXCHANGESTANDARD"                   = "Office 365 Exchange Online Only"
    "STANDARDPACK"                       = "Enterprise Plan E1"
    "STANDARDWOFFPACK"                   = "Office 365 (Plan E2)"
    "ENTERPRISEPACK"                     = "Enterprise Plan E3"
    "ENTERPRISEPACKLRG"                  = "Enterprise Plan E3"
    "ENTERPRISEWITHSCAL"                 = "Enterprise Plan E4"
    "STANDARDPACK_STUDENT"               = "Office 365 (Plan A1) for Students"
    "STANDARDWOFFPACKPACK_STUDENT"       = "Office 365 (Plan A2) for Students"
    "ENTERPRISEPACK_STUDENT"             = "Office 365 (Plan A3) for Students"
    "ENTERPRISEWITHSCAL_STUDENT"         = "Office 365 (Plan A4) for Students"
    "STANDARDPACK_FACULTY"               = "Office 365 (Plan A1) for Faculty"
    "STANDARDWOFFPACKPACK_FACULTY"       = "Office 365 (Plan A2) for Faculty"
    "ENTERPRISEPACK_FACULTY"             = "Office 365 (Plan A3) for Faculty"
    "ENTERPRISEWITHSCAL_FACULTY"         = "Office 365 (Plan A4) for Faculty"
    "ENTERPRISEPACK_B_PILOT"             = "Office 365 (Enterprise Preview)"
    "STANDARD_B_PILOT"                   = "Office 365 (Small Business Preview)"
    "VISIOCLIENT"                        = "Visio Pro Online"
    "POWER_BI_ADDON"                     = "Office 365 Power BI Addon"
    "POWER_BI_INDIVIDUAL_USE"            = "Power BI Individual User"
    "POWER_BI_STANDALONE"                = "Power BI Stand Alone"
    "POWER_BI_STANDARD"                  = "Power-BI Standard"
    "PROJECTESSENTIALS"                  = "Project Lite"
    "PROJECTCLIENT"                      = "Project Professional"
    "PROJECTONLINE_PLAN_1"               = "Project Online"
    "PROJECTONLINE_PLAN_2"               = "Project Online and PRO"
    "ProjectPremium"                     = "Project Online Premium"
    "ECAL_SERVICES"                      = "ECAL"
    "EMS"                                = "Enterprise Mobility Suite"
    "RIGHTSMANAGEMENT_ADHOC"             = "Windows Azure Rights Management"
    "MCOMEETADV"                         = "PSTN conferencing"
    "SHAREPOINTSTORAGE"                  = "SharePoint storage"
    "PLANNERSTANDALONE"                  = "Planner Standalone"
    "CRMIUR"                             = "CMRIUR"
    "BI_AZURE_P1"                        = "Power BI Reporting and Analytics"
    "INTUNE_A"                           = "Windows Intune Plan A"
    "PROJECTWORKMANAGEMENT"              = "Office 365 Planner Preview"
    "ATP_ENTERPRISE"                     = "Exchange Online Advanced Threat Protection"
    "EQUIVIO_ANALYTICS"                  = "Office 365 Advanced eDiscovery"
    "AAD_BASIC"                          = "Azure Active Directory Basic"
    "RMS_S_ENTERPRISE"                   = "Azure Active Directory Rights Management"
    "AAD_PREMIUM"                        = "Azure Active Directory Premium"
    "MFA_PREMIUM"                        = "Azure Multi-Factor Authentication"
    "STANDARDPACK_GOV"                   = "Microsoft Office 365 (Plan G1) for Government"
    "STANDARDWOFFPACK_GOV"               = "Microsoft Office 365 (Plan G2) for Government"
    "ENTERPRISEPACK_GOV"                 = "Microsoft Office 365 (Plan G3) for Government"
    "ENTERPRISEWITHSCAL_GOV"             = "Microsoft Office 365 (Plan G4) for Government"
    "DESKLESSPACK_GOV"                   = "Microsoft Office 365 (Plan K1) for Government"
    "ESKLESSWOFFPACK_GOV"                = "Microsoft Office 365 (Plan K2) for Government"
    "EXCHANGESTANDARD_GOV"               = "Microsoft Office 365 Exchange Online (Plan 1) only for Government"
    "EXCHANGEENTERPRISE_GOV"             = "Microsoft Office 365 Exchange Online (Plan 2) only for Government"
    "SHAREPOINTDESKLESS_GOV"             = "SharePoint Online Kiosk"
    "EXCHANGE_S_DESKLESS_GOV"            = "Exchange Kiosk"
    "RMS_S_ENTERPRISE_GOV"               = "Windows Azure Active Directory Rights Management"
    "OFFICESUBSCRIPTION_GOV"             = "Office ProPlus"
    "MCOSTANDARD_GOV"                    = "Lync Plan 2G"
    "SHAREPOINTWAC_GOV"                  = "Office Online for Government"
    "SHAREPOINTENTERPRISE_GOV"           = "SharePoint Plan 2G"
    "EXCHANGE_S_ENTERPRISE_GOV"          = "Exchange Plan 2G"
    "EXCHANGE_S_ARCHIVE_ADDON_GOV"       = "Exchange Online Archiving"
    "EXCHANGE_S_DESKLESS"                = "Exchange Online Kiosk"
    "SHAREPOINTDESKLESS"                 = "SharePoint Online Kiosk"
    "SHAREPOINTWAC"                      = "Office Online"
    "YAMMER_ENTERPRISE"                  = "Yammer for the Starship Enterprise"
    "EXCHANGE_L_STANDARD"                = "Exchange Online (Plan 1)"
    "MCOLITE"                            = "Lync Online (Plan 1)"
    "SHAREPOINTLITE"                     = "SharePoint Online (Plan 1)"
    "OFFICE_PRO_PLUS_SUBSCRIPTION_SMBIZ" = "Office ProPlus"
    "EXCHANGE_S_STANDARD_MIDMARKET"      = "Exchange Online (Plan 1)"
    "MCOSTANDARD_MIDMARKET"              = "Lync Online (Plan 1)"
    "SHAREPOINTENTERPRISE_MIDMARKET"     = "SharePoint Online (Plan 1)"
    "OFFICESUBSCRIPTION"                 = "Office ProPlus"
    "YAMMER_MIDSIZE"                     = "Yammer"
    "DYN365_ENTERPRISE_PLAN1"            = "Dynamics 365 Customer Engagement Plan Enterprise Edition"
    "ENTERPRISEPREMIUM_NOPSTNCONF"       = "Enterprise E5 (without Audio Conferencing)"
    "ENTERPRISEPREMIUM"                  = "Enterprise E5 (with Audio Conferencing)"
    "MCOSTANDARD"                        = "Skype for Business Online Standalone Plan 2"
    "PROJECT_MADEIRA_PREVIEW_IW_SKU"     = "Dynamics 365 for Financials for IWs"
    "STANDARDWOFFPACK_IW_STUDENT"        = "Office 365 Education for Students"
    "STANDARDWOFFPACK_IW_FACULTY"        = "Office 365 Education for Faculty"
    "EOP_ENTERPRISE_FACULTY"             = "Exchange Online Protection for Faculty"
    "EXCHANGESTANDARD_STUDENT"           = "Exchange Online (Plan 1) for Students"
    "OFFICESUBSCRIPTION_STUDENT"         = "Office ProPlus Student Benefit"
    "STANDARDWOFFPACK_FACULTY"           = "Office 365 Education E1 for Faculty"
    "STANDARDWOFFPACK_STUDENT"           = "Microsoft Office 365 (Plan A2) for Students"
    "DYN365_FINANCIALS_BUSINESS_SKU"     = "Dynamics 365 for Financials Business Edition"
    "DYN365_FINANCIALS_TEAM_MEMBERS_SKU" = "Dynamics 365 for Team Members Business Edition"
    "FLOW_FREE"                          = "Microsoft Flow Free"
    "POWER_BI_PRO"                       = "Power BI Pro"
    "O365_BUSINESS"                      = "Office 365 Business"
    "DYN365_ENTERPRISE_SALES"            = "Dynamics Office 365 Enterprise Sales"
    "RIGHTSMANAGEMENT"                   = "Rights Management"
    "PROJECTPROFESSIONAL"                = "Project Professional"
    "VISIOONLINE_PLAN1"                  = "Visio Online Plan 1"
    "EXCHANGEENTERPRISE"                 = "Exchange Online Plan 2"
    "DYN365_ENTERPRISE_P1_IW"            = "Dynamics 365 P1 Trial for Information Workers"
    "DYN365_ENTERPRISE_TEAM_MEMBERS"     = "Dynamics 365 For Team Members Enterprise Edition"
    "CRMSTANDARD"                        = "Microsoft Dynamics CRM Online Professional"
    "EXCHANGEARCHIVE_ADDON"              = "Exchange Online Archiving For Exchange Online"
    "EXCHANGEDESKLESS"                   = "Exchange Online Kiosk"
    "SPZA_IW"                            = "App Connect"
    "WINDOWS_STORE"                      = "Windows Store for Business"
    "MCOEV"                              = "Microsoft Phone System"
    "VIDEO_INTEROP"                      = "Polycom Skype Meeting Video Interop for Skype for Business"
    "SPE_E5"                             = "Microsoft 365 E5"
    "SPE_E3"                             = "Microsoft 365 E3"
    "ATA"                                = "Advanced Threat Analytics"
    "MCOPSTN2"                           = "Domestic and International Calling Plan"
    "FLOW_P1"                            = "Microsoft Flow Plan 1"
    "FLOW_P2"                            = "Microsoft Flow Plan 2"
    "WIN_DEF_ATP"                        = "Windows Defender ATP"
}
########################################

##
Function Get-ModuleInstalled {
    <#
    .SYNOPSIS
    Check installed modules

    .DESCRIPTION
    Function is needed to check all modules, needed for script execution.

    .EXAMPLE
    An example

    .NOTES
    General notes
    #>

    Write-Host "Checking installed module..." -ForegroundColor Yellow

    if (!(Get-InstalledModule -Name ReportHTML)) {

        Install-Module ReportHTML -Confirm:$false
    }
    else {
    
        Write-Host "Module ReportHTML is already installed"
    
    }
    if (!(Get-InstalledModule -Name Az.Accounts)) {

        Install-Module Az.Accounts -Confirm:$false
    }
    else {
    
        Write-Host "Module Az.Accounts is already installed"
    
    }
    if (!(Get-InstalledModule -Name AzureAD)) {
        
        Install-Module AzureAD -Confirm:$false 
    }
    else {
    
        Write-Host "Module AzureAD is already installed"
    
    }
    if (!(Get-InstalledModule -Name MSOnline)) {
        
        Install-Module MSOnline -Confirm:$false
    }
    else {
    
        Write-Host "Module MSOnline is already installed"
    
    }
    Write-Host "Modules has been checked." -ForegroundColor Green
}
Function Get-GlobalAdmins {
    <#
    .SYNOPSIS
    Get the list of global administrator

    .DESCRIPTION
    Function will collect the information about users, who have global administrator permissions in M365 tenant

    .EXAMPLE
    An example

    .NOTES
    General notes
    #>
    write-host "Getting global administrators..." -ForegroundColor Yellow

    #Get Tenant Global Admins
    $role = Get-AzureADDirectoryRole | Where-Object { $_.DisplayName -match "Global Administrator" } -ErrorAction SilentlyContinue
    If ($null -ne $role) {
        $Admins = Get-AzureADDirectoryRoleMember -ObjectId $role.ObjectId -ErrorAction SilentlyContinue | Where-Object { $_.DisplayName -ne "CloudConsoleGrapApi" }
        Foreach ($Admin in $Admins) {
		
            $MFAS = ((Get-MsolUser -objectid $Admin.ObjectID -ErrorAction SilentlyContinue).StrongAuthenticationRequirements).State
		
            if ($Null -ne $MFAS) {
                $MFASTATUS = "Enabled"
            }
            else {
                $MFASTATUS = "Disabled"
            }
		
            $Name = $Admin.DisplayName
            $EmailAddress = $Admin.Mail
            if (($admin.assignedlicenses.SkuID) -ne $Null) {
                $Licensed = $True
            }
            else {
                $Licensed = $False
            }
            $obj = [PSCustomObject]@{
                'Name'           = $Name
                'MFA Status'     = $MFAStatus
                'Is Licensed'    = $Licensed
                'E-Mail Address' = $EmailAddress
            }
		
            $GlobalAdminTable.add($obj)
        }
    }

    write-host "Information about global administrators has been collected" -ForegroundColor Green
}
Function Get-Domains {
    <#
    .SYNOPSIS
    Get the infomation about domains in M365 tenant

    .DESCRIPTION
    Get the infomation about domains in M365 tenant

    .EXAMPLE
    An example

    .NOTES
    General notes
    #>

    ###---DOMAINS---###

    Write-Host "Getting Tenant Domains..." -ForegroundColor Yellow

    #Tenant Domain
    $Domains = Get-AzureAdDomain

    foreach ($Domain in $Domains) {
        $DomainName = $Domain.Name
        $Verified = $Domain.IsVerified
        $DefaultStatus = $Domain.IsDefault
        $AuthType = $Domain.AuthenticationType
	
        $obj = [PSCustomObject]@{
            'Domain Name'         = $DomainName
            'Verification Status' = $Verified
            'Default'             = $DefaultStatus
            'Authentication Type' = $AuthType
        }
	
        $DomainTable.add($obj)
    }

    ###---DOMAINS---###

    write-host "Information about domains has been collected" -ForegroundColor Green

}
Function Get-CompanyInformation {
    <#
    .SYNOPSIS
    Get the information about tenant

    .DESCRIPTION
    Get the information about tenant

    .EXAMPLE
    An example

    .NOTES
    General notes
    #>
    Write-Host "Getting Company Information..." -ForegroundColor Yellow

    #Company Information
    $CompanyInfo = Get-AzureADTenantDetail -ErrorAction SilentlyContinue
    $CompanyName = $CompanyInfo.DisplayName
    $TechEmail = $CompanyInfo.TechnicalNotificationMails | Out-String
    $DirSync = $CompanyInfo.DirSyncEnabled
    $LastDirSync = $CompanyInfo.CompanyLastDirSyncTime


    If ($DirSync -eq $Null) {
        $LastDirSync = "Not Available"
        $DirSync = "Disabled"
    }
    If ($PasswordSync -eq $Null) {
        $LastPasswordSync = "Not Available"
    }

    $obj = [PSCustomObject]@{
        'Name'                = $CompanyName
        'Technical E-mail'    = $TechEmail
        'Directory Sync'      = $DirSync
        'Last Directory Sync' = $LastDirSync
    }

    $CompanyInfoTable.add($obj)

    write-host "Information about company has been collected" -ForegroundColor Green

}
Function Get-Licenses {
    <#
    .SYNOPSIS
    Get the list of Microsoft 365 licenses, availible in the tenant

    .DESCRIPTION
    Get the list of Microsoft 365 licenses, availible in the tenant

    .EXAMPLE
    An example

    .NOTES
    General notes
    #>
    ###---LICENSES---###

    Write-Host "Getting Licenses..." -ForegroundColor Yellow

    #Get all licenses
    $Licenses = Get-AzureADSubscribedSku
    #Split licenses at colon
    Foreach ($License in $Licenses) {
        $TextLic = $null
        $ASku = ($License).SkuPartNumber
        $TextLic = $Sku.Item("$ASku")
        If (!($TextLic)) {
            $OLicense = $License.SkuPartNumber
        }
        Else {
            $OLicense = $TextLic
        }
	
        $TotalAmount = $License.PrepaidUnits.enabled
        $Assigned = $License.ConsumedUnits
        $Unassigned = ($TotalAmount - $Assigned)

        If ($TotalAmount -lt $LicenseFilter) {
            $obj = [PSCustomObject]@{
                'Name'                = $Olicense
                'Total Amount'        = $TotalAmount
                'Assigned Licenses'   = $Assigned
                'Unassigned Licenses' = $Unassigned
            }
		
            $licensetable.add($obj)
        }
    }
    If (($licensetable).count -eq 0) {
        $licensetable = [PSCustomObject]@{
            'Information' = 'Information: No Licenses were found in the tenant'
        }
    }


    $IsLicensed = ($AllUsers | Where-Object { $_.assignedlicenses.count -gt 0 }).Count
    $objULic = [PSCustomObject]@{
        'Name'  = 'Users Licensed'
        'Count' = $IsLicensed
    }
    $IsLicensedUsersTable.add($objULic)

    $ISNotLicensed = ($AllUsers | Where-Object { $_.assignedlicenses.count -eq 0 }).Count
    $objULic = [PSCustomObject]@{
        'Name'  = 'Users Not Licensed'
        'Count' = $IsNotLicensed
    }

    $IsLicensedUsersTable.add($objULic)
    If (($IsLicensedUsersTable).count -eq 0) {
        $IsLicensedUsersTable = [PSCustomObject]@{
            'Information' = 'Information: No Licenses were found in the tenant'
        }
    }

    write-host "Information about licenses has been collected" -ForegroundColor Green

}
Function Get-AllGroups {
    <#
    .SYNOPSIS
    Get all groups in Azure Active Directory

    .DESCRIPTION
    Get all groups in Azure Active Directory

    .EXAMPLE
    An example

    .NOTES
    General notes
    #>

    write-host "Getting the list of tenant groups..." -ForegroundColor Yellow

    $azGroups = Get-AzureADGroup -All $true
    return $azGroups

}
Function Get-GroupObject {
    <#
    .SYNOPSIS
    Function is needed to build group object, required for reporting
    
    .DESCRIPTION
    Function is needed to build group object, required for reporting
    
    .PARAMETER groupID
    The ID of the azure ad group
    
    .PARAMETER groupName
    The name of the azure ad group
    
    .PARAMETER type
    Internal parameter is required for report only
    
    .EXAMPLE
    An example
    
    .NOTES
    General notes
    #>  
    param (
        [string]$groupID,
        [string]$groupName,
        [string]$type
    )

    $objGroups = $null;
    $objUsers = @();
    $objAll = $null;

    $Users = Get-AzureADGroupMember -ObjectId $groupID -All $true

    if ($Users) {

        $Users | ForEach-Object {

            $LicenseAssignedCount ++

            $objGroups = [PSCustomObject] @{
                Name                = "$type"
                GroupName           = $groupName
                'Total Users Count' = $LicenseAssignedCount
            } 
            $objUsers += [PSCustomObject] @{
                Name              = "$type USERS"
                GroupName         = $groupName
                UserDisplayName   = ($_.DisplayName)
                UserPrincipalName = ($_.UserPrincipalName)
            } 
            $objAll = [PSCustomObject]@{
                Groups = $objGroups
                Users  = $objUsers
            }
        }
    }
    $LicenseGroupsTable.Add($objAll) 
}
Function Get-OtherGroups {
    <#
.SYNOPSIS
Get the list of required groups

.DESCRIPTION
Get the list of required groups

.PARAMETER azGroups
An array with all groups in azure ad 

.EXAMPLE
An example

.NOTES
General notes
#>
    param (
        [system.object]$azGroups 
    )

    write-host "Getting Information about groups..." -ForegroundColor Yellow

    $Groups = $azGroups | Where-Object { `
        ($_.DisplayName -like "*-TeamsUsers") -or `
        ($_.DisplayName -like "*-IntuneUsers") -or `
        ($_.DisplayName -eq $WIPGroupName) -or `
        ($_.DisplayName -eq $MCASGroupName) -or `
        ($_.DisplayName -match '^AGAT-\w\d$') -or `
        ($_.DisplayName -eq $PhoneSystemGroupName)
    }

    $Groups | ForEach-Object {

        $groupId = $_.ObjectId
        $groupName = $_.displayName 

        ##Find only Teams groups
        if ($groupName -like "*-TeamsUsers") {

            Get-GroupObject -groupID $groupId -groupName $groupName -type "TEAMS" 
        }

        ##Find only Intune groups
        if ($groupName -like "*-IntuneUsers") {

            Get-GroupObject -groupID $groupId -groupName $groupName -type "INTUNE" 

        }
        ##Find WIP group and users 
        if ($groupName -eq $WIPGroupName) {

            Get-GroupObject -groupID $groupId -groupName $groupName -type "WIP" 

        }
        ##Find MCAS group and users
        if ($groupName -eq $MCASGroupName) {

            Get-GroupObject -groupID $groupId -groupName $groupName -type "MCAS" 

        }

        ##Find AGAT groups and users, use regular expression
        if ($groupName -match '^AGAT-\w\d$') {

            Get-GroupObject -groupID $groupId -groupName $groupName -type "AGAT" 

        }
        ##Find Business Voice group and users
        if ($groupName -eq $PhoneSystemGroupName) {

            Get-GroupObject -groupID $groupId -groupName $groupName -type "PHONE SYSTEM" 
        }
    }

    write-host "Information about groups has been collected." -ForegroundColor Yellow

}
Function Get-MissedGroups {
    <#
    .SYNOPSIS
    Get the list of users who are the members of Intune groups and not members of AGAT groups

    .DESCRIPTION
    Get the list of users who are the members of Intune groups and not members of AGAT groups

    .PARAMETER IntuneUsers
    Array with Intune users

    .PARAMETER AGATUsers
    Array with AGAT users

    .EXAMPLE
    An example

    .NOTES
    General notes
    #>
    param (
        [system.object]$IntuneUsers,
        [system.object]$AGATUsers
    )

    $objNonAgatUsers = @()
    $objNonAgatAll = $null;
    $objNonAgatGroups = $null;

    foreach ($a1 in $IntuneUsers | Sort-Object UserPrincipalName -Unique) {

        if ((($AGATUsers.UserPrincipalName).tolower()).IndexOf($a1.UserPrincipalName.ToLower()) -eq "-1") {

            $LicenseAssignedCount++

            $objNonAgatGroups = [PSCustomObject] @{
                Name                = "No AGAT"
                'Total Users Count' = $LicenseAssignedCount
            } 

            $objNonAgatUsers += [PSCustomObject] @{
                Name              = "No AGAT"
                GroupName         = $a1.GroupName
                UserDisplayName   = $a1.UserDisplayName
                UserPrincipalName = $a1.UserPrincipalName
            } 
            $objNonAgatAll = [PSCustomObject] @{
                Groups = $objNonAgatGroups
                Users  = $objNonAgatUsers
            }
        }
    }
    $NoAgatGroupsTable.Add($objNonAgatAll)
}
Function Get-Report {

    Write-Host "Generating HTML Report..." -ForegroundColor Yellow

    #basic Properties 

    $rpt = New-Object 'System.Collections.Generic.List[System.Object]'
    $rpt += @"
    $LogoContent
    <div class="pageTitle">Office 365 Tenant Report</div>
    <hr />
    <div class="ReportCreated">Report created on $(Get-Date -Format 'd MMM yyyy hh:mm tt')</div>
"@
    $rpt += get-htmlopenpage -TitleText 'Office 365 Tenant Report' -HideLogos -HideTitle
    $rpt += Get-HtmlContentOpen -HeaderText "Office 365 Dashboard"

    $rpt += Get-HTMLContentOpen -HeaderText "Domains"
    $rpt += Get-HTMLContentTable $DomainTable
    #Get-htmlcontentdatatable
    $rpt += Get-HTMLContentClose

    $rpt += Get-HTMLContentOpen -HeaderText "Office 365 Licenses"
    $rpt += Get-HTMLContentTable $LicenseTable
  
    $rpt += Get-HTMLContentClose

    $rpt += Get-HTMLContentOpen -HeaderText "Global Administrators"
    $rpt += Get-HTMLContentTable $GlobalAdminTable 

    $rpt += Get-HTMLContentClose

    ##TEAMS##

    $rpt += Get-HTMLContentOpen -HeaderText "TEAMS"

    if ($LicenseGroupsTable.Groups | Where-Object { $_.Name -ieq "TEAMS" }) {
    
        $rpt += Get-HTMLContentOpen -HeaderText "TEAMS groups"
        $rpt += Get-HtmlContenttable $($LicenseGroupsTable.Groups | `
                Where-Object { $_.Name -ieq "TEAMS" } | Select-Object GroupName, 'Total Users Count') -HideFooter -DisableInfo
        $rpt += Get-HTMLContentClose

        $rpt += Get-HTMLContentOpen -HeaderText "TEAMS groups summary"
        $rpt += Get-HtmlContenttable -ArrayOfObjects $($LicenseGroupsTable.Groups | `
                Where-Object { $_.Name -ieq "TEAMS" } | `Measure-Object -sum 'Total Users Count' | `
                Select-Object -property @{Name = 'Name'; Expression = { "{0}" -f $_.Property } }, @{Name = 'Value'; Expression = { "{0}" -f $_.sum } })
        $rpt += Get-HTMLContentClose

        $rpt += Get-HTMLContentOpen -HeaderText "TEAMS Users"
        $rpt += get-htmlcontentdatatable $($LicenseGroupsTable.Users | `
                Where-Object { $_.Name -ieq "TEAMS USERS" } |  Select-Object UserDisplayName, UserPrincipalName, GroupName) -HideFooter
        $rpt += Get-HTMLContentClose
    }
    $rpt += Get-HTMLContentClose

    ##INTUNE##

    $rpt += Get-HTMLContentOpen -HeaderText "INTUNE"
    
    if ($LicenseGroupsTable.Groups | Where-Object { $_.Name -ieq "INTUNE" }) {

        $rpt += Get-HTMLContentOpen -HeaderText "INTUNE groups"
        $rpt += Get-HtmlContenttable $($LicenseGroupsTable.Groups | `
                Where-Object { $_.Name -ieq "INTUNE" } | Select-Object GroupName, 'Total Users Count') -HideFooter -DisableInfo
        $rpt += Get-HTMLContentClose

        $rpt += Get-HTMLContentOpen -HeaderText "INTUNE groups summary"
        $rpt += Get-HtmlContenttable -ArrayOfObjects $($LicenseGroupsTable.Groups | `
                Where-Object { $_.Name -ieq "INTUNE" } | Measure-Object -sum 'Total Users Count' | `
                Select-Object -property @{Name = 'Name'; Expression = { "{0}" -f $_.Property } }, @{Name = 'Value'; Expression = { "{0}" -f $_.sum } })
        $rpt += Get-HTMLContentClose

        $rpt += Get-HTMLContentOpen -HeaderText "INTUNE Users"
        $rpt += get-htmlcontentdatatable $($LicenseGroupsTable.Users | `
                Where-Object { $_.Name -ieq "INTUNE USERS" } |  Select-Object UserDisplayName, UserPrincipalName, GroupName) -HideFooter
        $rpt += Get-HTMLContentClose
    }
    $rpt += Get-HTMLContentClose

    ##WIP##

    $rpt += Get-HTMLContentOpen -HeaderText "WIP"
    
    if ($LicenseGroupsTable.Groups | Where-Object { $_.Name -ieq "WIP" }) {

        $rpt += Get-HTMLContentOpen -HeaderText "WIP groups"
        $rpt += Get-HtmlContenttable  -ArrayOfObjects $($LicenseGroupsTable.Groups | `
                Where-Object { $_.Name -ieq "WIP" } | Select-Object GroupName, 'Total Users Count') -HideFooter -DisableInfo
        $rpt += Get-HTMLContentClose
        <#
        $rpt += Get-HTMLContentOpen -HeaderText "WIP groups summary"
        $rpt += Get-HtmlContenttable -ArrayOfObjects $($LicenseGroupsTable.Groups | `
                Where-Object { $_.Name -ieq "WIP" } | `Measure-Object -sum 'Total Users Count' | `
                Select-Object -property @{Name = 'Name'; Expression = { "{0}" -f $_.Property } }, @{Name = 'Value'; Expression = { "{0}" -f $_.sum } })
        $rpt += Get-HTMLContentClose
        #>
        $rpt += Get-HTMLContentOpen -HeaderText "WIP Users"
        $rpt += get-htmlcontentdatatable  -ArrayOfObjects $($LicenseGroupsTable.Users | `
                Where-Object { $_.Name -ieq "WIP USERS" } |  Select-Object UserDisplayName, UserPrincipalName) -HideFooter
        $rpt += Get-HTMLContentClose
    }
    $rpt += Get-HTMLContentClose

    ##MCAS##

    $rpt += Get-HTMLContentOpen -HeaderText "MCAS"
    
    if ($LicenseGroupsTable.Groups | Where-Object { $_.Name -ieq "MCAS" }) {
    
        $rpt += Get-HTMLContentOpen -HeaderText "MCAS groups"
        $rpt += Get-HtmlContenttable  -ArrayOfObjects $($LicenseGroupsTable.Groups | `
                Where-Object { $_.Name -ieq "MCAS" } | Select-Object GroupName, 'Total Users Count') -HideFooter -DisableInfo
        $rpt += Get-HTMLContentClose
        <#
        $rpt += Get-HTMLContentOpen -HeaderText "MCAS groups summary"
        $rpt += Get-HtmlContenttable -ArrayOfObjects $($LicenseGroupsTable.Groups | `
                Where-Object { $_.Name -ieq "MCAS" } | `Measure-Object -sum 'Total Users Count' | `
                Select-Object -property @{Name = 'Name'; Expression = { "{0}" -f $_.Property } }, @{Name = 'Value'; Expression = { "{0}" -f $_.sum } })
        $rpt += Get-HTMLContentClose
        #>
        $rpt += Get-HTMLContentOpen -HeaderText "MCAS Users"
        $rpt += get-htmlcontentdatatable  -ArrayOfObjects $($LicenseGroupsTable.Users | `
                Where-Object { $_.Name -ieq "MCAS USERS" } |  Select-Object UserDisplayName, UserPrincipalName) -HideFooter
        $rpt += Get-HTMLContentClose
    }
    $rpt += Get-HTMLContentClose

    ##TEAMS VOICE
    $rpt += Get-HTMLContentOpen -HeaderText "Teams Voice"
    
    if ($LicenseGroupsTable.Groups | Where-Object { $_.Name -ieq "PHONE SYSTEM" }) {
    
        $rpt += Get-HTMLContentOpen -HeaderText "Teams Voice Groups"
        $rpt += Get-HtmlContenttable $($LicenseGroupsTable.Groups | `
                Where-Object { $_.Name -ieq "PHONE SYSTEM" } | Select-Object GroupName, 'Total Users Count') -HideFooter -DisableInfo
        $rpt += Get-HTMLContentClose
        <#
        $rpt += Get-HTMLContentOpen -HeaderText "Teams Voice groups summary"
        $rpt += Get-HtmlContenttable -ArrayOfObjects $($LicenseGroupsTable.Groups | `
                Where-Object { $_.Name -ieq "PHONE SYSTEM" } | `Measure-Object -sum 'Total Users Count' | `
                Select-Object -property @{Name = 'Name'; Expression = { "{0}" -f $_.Property } }, @{Name = 'Value'; Expression = { "{0}" -f $_.sum } })
        $rpt += Get-HTMLContentClose
        #>
        $rpt += Get-HTMLContentOpen -HeaderText "Teams Voice Users"
        $rpt += get-htmlcontentdatatable $($LicenseGroupsTable.Users | `
                Where-Object { $_.Name -ieq "PHONE SYSTEM USERS" } |  Select-Object UserDisplayName, UserPrincipalName, GroupName) -HideFooter
        $rpt += Get-HTMLContentClose
    }

    $rpt += Get-HTMLContentClose

    ##AGAT

    $rpt += Get-HTMLContentOpen -HeaderText "AGAT"
    
    if ($LicenseGroupsTable.Groups | Where-Object { $_.Name -ieq "AGAT" }) {

        $rpt += Get-HTMLContentOpen -HeaderText "AGAT groups"
        $rpt += Get-HtmlContenttable  -ArrayOfObjects $($LicenseGroupsTable.Groups | `
                Where-Object { $_.Name -ieq "AGAT" } | Select-Object GroupName, 'Total Users Count') -HideFooter -DisableInfo
        $rpt += Get-HTMLContentClose

        $rpt += Get-HTMLContentOpen -HeaderText "AGAT groups summary"
        $rpt += Get-HtmlContenttable -ArrayOfObjects $($LicenseGroupsTable.Groups | `
                Where-Object { $_.Name -ieq "AGAT" } | `Measure-Object -sum 'Total Users Count' | `
                Select-Object -property @{Name = 'Name'; Expression = { "{0}" -f $_.Property } }, @{Name = 'Value'; Expression = { "{0}" -f $_.sum } })
        $rpt += Get-HTMLContentClose

        $rpt += Get-HTMLContentOpen -HeaderText "AGAT Users"
        $rpt += get-htmlcontentdatatable  -ArrayOfObjects $($LicenseGroupsTable.Users | `
                Where-Object { $_.Name -ieq "AGAT USERS" } |  Select-Object UserDisplayName, UserPrincipalName, GroupName) -HideFooter
        $rpt += Get-HTMLContentClose
    }
    $rpt += Get-HTMLContentClose


    ##NoAGAT
    $rpt += Get-HTMLContentOpen -HeaderText "Missing AGAT Users"

    if ($NoAgatGroupsTable.Groups | Where-Object { $_.Name -ieq "No AGAT" }) {

        $rpt += Get-HTMLContentOpen -HeaderText "Missing AGAT summary"
        $rpt += Get-HtmlContenttable -ArrayOfObjects $($NoAgatGroupsTable.Groups | `Measure-Object -sum 'Total Users Count' | `
                Select-Object -property @{Name = 'Name'; Expression = { "{0}" -f $_.Property } }, @{Name = 'Value'; Expression = { "{0}" -f $_.sum } })
        $rpt += Get-HTMLContentClose

        $rpt += Get-HTMLContentOpen -HeaderText "Missing AGAT Users"
        $rpt += get-htmlcontentdatatable  -ArrayOfObjects $($NoAgatGroupsTable.Users |  Select-Object UserDisplayName, UserPrincipalName) -HideFooter
        $rpt += Get-HTMLContentClose

    }
    $rpt += Get-HTMLContentClose


    $rpt += Get-HtmlContentClose 
    # $rpt += get-htmltabcontentclose

    $rpt += Get-HTMLClosePage

    $Day = (Get-Date).Day
    $Month = (Get-Date).Month
    $Year = (Get-Date).Year
    $ReportName = ( "$Month" + "-" + "$Day" + "-" + "$Year" + "-" + "O365 Tenant Report")
    Save-HTMLReport -ReportContent $rpt -ShowReport -ReportName $ReportName -ReportPath $ReportSavePath

    Write-Host "HTML Report has been generated" -ForegroundColor Green

}
Function Main {

    Get-ModuleInstalled

    Write-Host "Connecting to Azure Active Directory..." -ForegroundColor Yellow
    
    if ($Office365Access) {
        
        connect-azuread -Credential $Office365Access | Out-Null
    
    }
    else {

        Connect-AzureAD | Out-Null
    
    }
    Write-Host "Successfully connected to Azure Active Directory" -ForegroundColor Green

    Get-GlobalAdmins
    Get-Domains
    Get-CompanyInformation
    Get-Licenses
    Get-OtherGroups -azGroups $(Get-AllGroups)

    Get-MissedGroups `
        -IntuneUsers $($LicenseGroupsTable.Users | Where-Object { $_.Name -ieq "INTUNE USERS" }) `
        -AGATUsers $($LicenseGroupsTable.Users | Where-Object { $_.Name -ieq "AGAT USERS" })
    
    Get-Report
}
Main
