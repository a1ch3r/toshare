[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls -bor [Net.SecurityProtocolType]::Tls11 -bor [Net.SecurityProtocolType]::Tls12

$batchNumber = read-host "Please provide batch number"
if (!$OnPremcredentials) {
    $OnPremcredentials = Get-Credential
}
if (!$cloudCredentials) {
    $cloudCredentials = Get-Credential
}

$BatchFolderPath = "C:\_EOPMigration\EOL_BATCH$batchNumber\"
$BatchName = $BatchFolderPath.Split("\")[5]

$BatchUsersPath = Get-ChildItem -Path $BatchFolderPath | Where-Object { $_.Name -match "EOL_BATCH$batchNumber.csv" }
$validMailboxesPath = $BatchFolderPath + "$BatchName-valid.csv"
$DisabledMailboxesPath = $BatchFolderPath + "$BatchName-disabled.csv"

$users = Import-Csv $BatchUsersPath.FullName -Delimiter "|"
$users.Count

Function Connect-Exchange ($onPremCreds, $cloudCreds, $env) {

    if ($env -eq "onprem") {
    
        Get-PSSession | where { $_.Name -eq "ONPREMISE" } | Remove-PSSession

        try {
            $sessionOption = New-PSSessionOption -SkipCACheck -SkipCNCheck
            $Session = New-PSSession `
                -ConfigurationName Microsoft.Exchange `
                -ConnectionUri https://gs0233.domain.net/powershell/ `
                -Credential $onPremCreds  `
                -Authentication Default `
                -SessionOption $sessionOption `
                -Name "ONPREMISE" #-AllowRedirection
            Import-PSSession $session -DisableNameChecking  -AllowClobber -Verbose -Prefix "p" # out-null #-CommandName  Get-Mailbox, Get-RecipientPermission, Get-MailboxPermission, Get-MailboxStatistics
            Write-host "Connected to Exchange OnPremise" -ForegroundColor Magenta     
        }
        catch {
            $ErrorMessage = $_.Exception.Message
            $FailedItem = $_.Exception.ItemName
            write-host $ErrorMessage -ForegroundColor Red
            break;
        }

    }

    elseif ($env -eq "cloud") {
  
        Connect-ExchangeOnline -Credential $cloudCreds

    }
}

Function Get-UserGroups {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory = $true, ValueFromPipeline = $true)]
        [alias("SamAccountName", "Name")]
        [String]$user,
        [switch]$sam,
        [switch]$adsiInterface = $false
    ) # param
 
    Begin { 
        Try {
            Import-Module ActiveDirectory -ErrorAction Stop
        }
        Catch {
            $adsiInterface = $true
        }
    }# Begin
 
    Process {
        If ($adsiInterface) {

            $objDomain = New-Object System.DirectoryServices.DirectoryEntry
            $domainDN = $objDomain.distinguishedName
            $searchRoot = "LDAP://$domainDN"
           
            $root = [ADSI]''			
            $Searcher = New-Object DirectoryServices.DirectorySearcher($root)
            $Searcher.Filter = "(&(objectCategory=person)(SamAccountName=$user))"
            $retObject = $Searcher.FindAll()
           
            $userObj = [ADSI]$retObject.Path
            $Groups = $userObj.MemberOf | ForEach-Object { [ADSI]"LDAP://$_" }
        }
        Else {
            #Get user Data
            Try {
                $Groups = (Get-ADUser $user -properties MemberOf -ErrorAction Stop).MemberOf | Get-ADGroup
            }
            Catch {
                Write-Warning "$user not found`n"
                $error.Clear()
                Break
            }
        }
    }# Process
 
    End {
        if ($Groups) {
            return $Groups.Name
        }
    } # End
}# function

$e3Group = "U-APP-Cloud-O365-E3 Basic"
$e1Group = "U-APP-Cloud-O365-E1 Basic"

Connect-Exchange -onPremCreds $OnPremcredentials -env "onprem"
Connect-Exchange -cloudCreds $cloudCredentials -env "cloud"
Connect-MsolService -Credential $cloudCredentials

$validMailboxes = @()

foreach ($user in $users) {

    write-host  "$($user.Mail)" -ForegroundColor Green

    $onPremMailbox = $null;
    $onPremRecipient = $null;
    $CloudMailbox = $null;
    $CloudRecipient = $null;
    
    #GetUser from AD
    $adUser = Get-ADUser -Filter "UserPrincipalName -eq '$($user.Mail)'" | Select-Object SamAccountName, Enabled
    
    if ($adUser) {

        #Check user status
        if ($adUser.Enabled -eq $true) {

            $isEnabled = $true    
        }    
        else {
            $isEnabled = $false
        }
        #Check user status
    
        #GetUserGroups from AD
        $userGroups = $null;
        $userGroups = Get-UserGroups -user $($user.SamAccountName) -adsiInterface

        if ($userGroups) {

            if ($UserGroups.IndexOf("$e3Group") -ne "-1") {
                $isInE3Group = $true
            } 
            else {
                $isInE3Group = $false
            }
            if ($UserGroups.IndexOf("$e1Group") -ne "-1") {
                $isInE1Group = $true
            } 
            else {
                $isInE1Group = $false
            }
        }

        #Get OnPremiseMailbox
        $onPremMailbox = Get-pMailbox -Identity $($user.Mail) -ErrorAction SilentlyContinue | `
            Select-Object  SamAccountName, UserPrincipalName, DisplayName, PrimarySmtpAddress, RecipientType, RecipientTypeDetails, Name
        $onPremRecipient = Get-pRecipient -Identity $($user.Mail) -ErrorAction SilentlyContinue | `
            Select-Object RecipientType, RecipientTypeDetails

        #Get Cloud Mailbox    
        $CloudMailbox = Get-EXOMailbox -Identity $($user.Mail) -ErrorAction SilentlyContinue
        $CloudRecipient = Get-EXORecipient -Identity $($user.Mail) -PropertySets All -ErrorAction SilentlyContinue | `
            Select-Object RecipientType, RecipientTypeDetails

        #Get Cloud User    
        $CloudUser = $null;
        $CloudUser = Get-MsolUser -UserPrincipalName $($user.Mail) -ErrorAction SilentlyContinue

        if ($CloudUser) {

            $IsSync = $true;

            if (($CloudUser.Licenses.AccountSkuId -match "SPE_E3") -or ($CloudUser.Licenses.AccountSkuId -match "STANDARDPACK")) {
                $isLicensed = $true
            }            
            else {
                $isLicensed = $false
            }
        }
        else {
            $IsSync = $false;
            $isLicensed = $false;
        }


        if ($onPremMailbox) {

            $IsOnPremise = $true;

        }
        else {
            $IsOnPremise = $false;
        }

        if ($CloudMailbox) {

            $IsInCloud = $true 

        }
        else {

            $IsInCloud = $false

        }
    }

    $validMailboxes += [PSCustomObject]@{
        SamAccountName              = $(($user.SamAccountName).ToLower())
        EmailAddress                = $user.Mail
        MailBoxType                 = $($onPremMailbox.RecipientTypeDetails)
        IsOnPremise                 = $IsOnPremise
        Licensing                   = $isLicensed
        IsInCloud                   = $IsInCloud
        IsSync                      = $IsSync
        "U-APP-Cloud-O365-E3 Basic" = $isInE3Group
        "U-APP-Cloud-O365-E1 Basic" = $isInE1Group
        IsEnabled                   = $isEnabled

    }
}

$validMailboxes | Where-Object { $_.IsEnabled -eq $true } | `
    Select-Object SamAccountName, `
@{L = "Mailbox"; E = { $_.EmailAddress } }, `
@{L = "MailBoxType (user/shared/resource)"; E = { 
        $_.MailBoxType 
    } 
}, `
@{L = "Mailbox exists on-prem? (YES/NO)"; E = { 
        if ($_.IsOnPremise) { 
            "YES" 
        }
        else {
            "NO" 
        }
    } 
}, `
@{L = "User is synced? (YES/NO)"; E = { 
        if ($_.IsSync) { 
            "YES" 
        }
        else {
            "NO" 
        } 
    } 
}, `
@{L = "Proper 'Basic' Licensing? (OK/NOK)"; E = { 
        if ($_.Licensing) { 
            "YES" 
        }
        else { 
            "NOK" 
        } 
    } 
}, `
@{L = "Mailbox exists on-line (YES/NO)"; E = { 
        if ($_.IsInCloud) { 
            "YES" 
        }
        else {  
            "NO"  
        } 
    }
}, "U-APP-Cloud-O365-E3 Basic", "U-APP-Cloud-O365-E1 Basic", IsEnabled `
| export-csv -Path $validMailboxesPath -NoTypeInformation -Delimiter "|"


$validMailboxes | where { $_.IsEnabled -eq $false } |
Select-Object SamAccountName, `
@{L = "Mailbox"; E = { $_.EmailAddress } }, `
@{L = "MailBoxType (user/shared/resource)"; E = { 
        $_.MailBoxType 
    } 
}, `
@{L = "Mailbox exists on-prem? (YES/NO)"; E = { 
        if ($_.IsOnPremise) { 
            "YES" 
        }
        else {
            "NO" 
        }
    } 
}, `
@{L = "User is synced? (YES/NO)"; E = { 
        if ($_.IsSync) { 
            "YES" 
        }
        else {
            "NO" 
        } 
    } 
}, `
@{L = "Proper 'Basic' Licensing? (OK/NOK)"; E = { 
        if ($_.Licensing) { 
            "YES" 
        }
        else { 
            "NOK" 
        } 
    } 
}, `
@{L = "Mailbox exists on-line (YES/NO)"; E = { 
        if ($_.IsInCloud) { 
            "YES" 
        }
        else {  
            "NO"  
        } 
    }
}, "U-APP-Cloud-O365-E3 Basic", "U-APP-Cloud-O365-E1 Basic", IsEnabled `
| export-csv -Path $DisabledMailboxesPath -NoTypeInformation -Delimiter "|"



Get-Pssession | remove-pssession