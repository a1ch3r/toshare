$StartDTM = (get-date)
Write-Host $StartDTM -ForegroundColor Green

$type = "UserMailbox"

. "C:\_SUPPORT\Create-ReportPath.ps1" -ReportName "MAILBOXES_ACTIVE"

$MailboxesFilePath = Get-ChildItem -Path $global:ReportFullPath | `
    Where-Object { $_.Name -match "_active" } | `
    Sort-Object -Property CreationTime -Descending | `
    Select-Object -Property FullName | Select-Object -First 1

write-host "Mailboxes path: $($MailboxesFilePath.FullName)" -ForegroundColor Cyan

$Mailboxes = Import-Csv -Path $($MailboxesFilePath.FullName) -Delimiter "|" | Where-Object {($_.RecipientTypeDetails -eq $type)} #| Select-Object -First 100

write-host "Mailboxes imported: $($Mailboxes.Count)" -ForegroundColor Cyan


. "C:\_SUPPORT\Create-ReportPath.ps1" -ReportName "MAILBOXES_USERS_PERMISSIONS"

$MailBoxesPermissions = $global:ReportFullPath

$MailBoxesPermissionsTemp = $MailBoxesPermissions + "\" + "MAILBOXES_USERS_PERMISSIONS_TEMP"

if (!(Test-Path -Path $MailBoxesPermissionsTemp)) {
    new-item -Path $MailBoxesPermissionsTemp -ItemType Directory
}


$MailBoxesPermissionsTempData = $MailBoxesPermissions + "\" + "MAILBOXES_USERS_PERMISSIONS_TEMP_DATA"

if (!(Test-Path -Path $MailBoxesPermissionsTempData)) {
    new-item -Path $MailBoxesPermissionsTempData -ItemType Directory
}

$ReportDate = "$((get-date).Year)" + "-" + "$((Get-Culture).DateTimeFormat.GetMonthName((Get-Date).Month))"


$NumberofArrays = 20;
$NewArrays = @{ }
$i = 0;
$Mailboxes | ForEach-Object {
    $NewArrays[$i % $numberofArrays] += @($_);
    $i++
}

Foreach ($array in  $newArrays.Keys | Sort-Object $_) {
    $ArrayNumber = $null
    $ArrayCount = $null
    $values = $null;
    $val = $null;

    $ArrayNumber = $array
    $ArrayCount = $ArrayNumber + 1
    [System.Object]$value = $newArrays[$ArrayNumber]
    
    $value | Export-Csv -Path ("$MailBoxesPermissionsTemp\MAILBOXES_USERS_PERMISSIONS" + "-$ArrayCount.csv") -Delimiter "|" -NoTypeInformation
}

$values = Get-ChildItem -Path "$MailBoxesPermissionsTemp"

foreach ($v in $values | Sort-Object $_.BaseName ) {
    $val = $v.FullName 
    $ArrayCount = ($v.BaseName).Split("-")[1]
    Write-Host "Start file: $($val)"

    start-process -FilePath powershell.exe `
        -argument "C:\_GENERAL\Get-O365-MBX-Permissions-User-Session.ps1 -valuePath $val -Type $type -ArrayCount $ArrayCount -DataPath $MailBoxesPermissionsTempData" -Wait #-PassThru
    #Remove-Item -Path $($val) -Confirm:$false     
}


$getFirstLine = $true

get-childItem $($MailBoxesPermissionsTempData) | where {$_.Name -match "-temp.csv"} | ForEach-Object {
    
    $filePath = $_.FullName

    $lines = Get-Content $filePath  
    $linesToWrite = switch($getFirstLine) {
           $true  {$lines}
           $false {$lines | Select -Skip 1}

    }
    $getFirstLine = $false
    Add-Content  "$MailBoxesPermissions\MAILBOXES_USERS_PERMISSIONS_$ReportDate.csv" $linesToWrite
    }

$results = Import-Csv -Path "$MailBoxesPermissions\MAILBOXES_USERS_PERMISSIONS_$ReportDate.csv" -Delimiter "|"

foreach ($r in $results)  {
    foreach ($t in $r.Trustee.Split(",")){
        [PSCustomObject]@{
            Identity = $r.Identity
            I_PrimarySmtpAddress = $r.I_PrimarySmtpAddress
            I_UsageLocation = $r.I_UsageLocation
            Trustee = $t
            AccessRights = $r.AccessRights
        } | Export-Csv -Path "$MailBoxesPermissions\MAILBOXES_USERS_PERMISSIONS_UPD_$ReportDate.csv" -Delimiter "|" -NoTypeInformation -Append
    }
}
Remove-Item -Path "$MailBoxesPermissions\MAILBOXES_USERS_PERMISSIONS_$ReportDate.csv" -Confirm:$false     
Rename-Item -Path "$MailBoxesPermissions\MAILBOXES_USERS_PERMISSIONS_UPD_$ReportDate.csv" -NewName "$MailBoxesPermissions\MAILBOXES_USERS_PERMISSIONS_$ReportDate.csv" -Force


$EndtDTM = (get-date)
Write-Host $EndtDTM -ForegroundColor Green
Write-Host "Minutes taken: $([math]::Round(($EndtDTM - $StartDTM).TotalMinutes,2))" -ForegroundColor Green