$requestBody = Get-Content $Req -Raw | ConvertFrom-Json

Write-Output "PowerShell script processed queue message '$requestBody'"

import-module `
    "D:\home\site\wwwroot\SiteCreationWizard\myModules\Custom_2020-03-03\SharePointPnPPowerShellOnline.psd1" -Global

Write-Output "PnP Provisioning Started"

function Remove-Diacritics([string]$String) {
    $objD = $String.Normalize([Text.NormalizationForm]::FormD)
    $sb = New-Object Text.StringBuilder
 
    for ($i = 0; $i -lt $objD.Length; $i++) {
        $c = [Globalization.CharUnicodeInfo]::GetUnicodeCategory($objD[$i])
        if ($c -ne [Globalization.UnicodeCategory]::NonSpacingMark) {
            [void]$sb.Append($objD[$i])
        }
    }
 
    return("$sb".Normalize([Text.NormalizationForm]::FormC))
}

#region SharePoint variables (PROD)
$adminUrl = $env:adminUrl
$wizardUrl = $env:wizardUrl
$root = $env:root
$listName = "SiteCreationList"
#region SharePoint variables (PROD)

#region Credentials
$credUsername = $env:SiteCreation
$credPassword = $env:SiteCreationPassword | convertto-securestring -AsPlainText -Force
$creds = New-Object System.Management.Automation.PSCredential $credUsername, $credPassword
#region Credentials

#app settings
$tenant = "customergroup"
$graphClientId = "c05cef45-ca94-427b-a737-164b99e6e50e"
$graphClientSecret = "pHTMv7DfdY_6Oy7zI3U4d2PA-3C-3p5~QW"
#app settings

#test data
#$displayName = 'HTC Superfloorâ„¢.SÃ¶derkÃ¶ping, Sweden, KlevvÃ¤gen 7 (SESOG002)'
#$siteOwner = 'johanna.kotheus@customergroup.com'
#$sitePrivacy = 'Private'
#test data

#region SharePoint List Item Data
$SiteTitle = $requestBody.Title
#$SiteTitle = $displayName
$SiteTitle = ((Remove-Diacritics -String $($SiteTitle.Split(".")[0])) -replace '[^\x30-\x39\x41-\x5A\x61-\x7A]+', '')
$SiteDescription = $SiteTitle
$SiteAlias = $SiteTitle
$SiteBusinessOwner = $requestBody.BusinessOwner
#$SiteBusinessOwner = $siteOwner
$SiteConfidentialityLevel = $requestBody.ConfidentialityLevel
#$SiteConfidentialityLevel = $sitePrivacy
$isSiteCreated = $false

write-output $SiteTitle
write-output $SiteAlias

$SitePrivacy = "Public"

if ($SiteConfidentialityLevel -eq "Private") {

    $SitePrivacy = "Private"

}

#region SharePoint List Item Data
$ItemId = $requestBody.ItemId
$callbackUrl = $requestBody.callbackUrl;
#region SharePoint List Item Data

function Get-AuthenticationHeaders {
    [CmdletBinding()]
    param
    (
        [Parameter(Mandatory = $true)][string]$AccessToken,
        [Parameter(Mandatory = $false)][switch]$IncludeContentTypeJson,
        [Parameter(Mandatory = $false)][switch]$IncludeAcceptJson
    )

    begin {
    }
    process {
        $headers = @{ 'Authorization' = "Bearer $($AccessToken)" }
        
        if ( $IncludeContentTypeJson ) {
            $headers.Add( "Content-Type", "application/json" )
        }    

        if ( $IncludeAcceptJson ) {
            $headers.Add( "accept", "application/json" )
        }

        return $headers
    }
    end {
    }
}

function Get-AccessToken {
    [CmdletBinding()]
    param
    (
        [Parameter(Mandatory = $true)][string]$ClientId,
        [Parameter(Mandatory = $true)][string]$ClientSecret,
        [Parameter(Mandatory = $true)][string]$Tenant,
        [Parameter(Mandatory = $true)][string]$Resource
    )

    begin {
        $body = @{ 
            grant_type    = "client_credentials" 
            client_id     = $ClientId
            client_secret = $ClientSecret
            resource      = $Resource
        }
    }
    process {
        Invoke-RestMethod -Uri https://login.microsoftonline.com/$Tenant/oauth2/token -Method Post -Body $body | SELECT -ExpandProperty access_token
    }
    end {
    }
}

function New-MicrosoftTeam {
    [CmdletBinding()]
    param
    (
        [Parameter(Mandatory = $true)][string]$AccessToken,
        [Parameter(Mandatory = $true)][string]$MailNickname,
        [Parameter(Mandatory = $true)][string]$DisplayName,
        [Parameter(Mandatory = $true)][string]$Description,
        [Parameter(Mandatory = $true)][ValidateSet("Public", "Private")][string]$Visibility,
        [Parameter(Mandatory = $true)][string[]]$Owners
    )

    begin {
        $headers = Get-AuthenticationHeaders -AccessToken $AccessToken -IncludeContentTypeJson -IncludeAcceptJson
    }
    process {
        $Owners = $Owners | % { "https://graph.microsoft.com/v1.0/users/$_" }

        $body = @{
            displayName         = $DisplayName
            description         = $Description
            mailEnabled         = $true
            mailNickname        = $MailNickname
            securityEnabled     = $true
            "owners@odata.bind" = $Owners
            groupTypes          = @("Unified")
            visibility          = $Visibility
        } | ConvertTo-Json

        Write-output $body
        
        $group = Invoke-WebRequest -Uri "https://graph.microsoft.com/v1.0/groups" -Headers $headers -Body $body -Method Post -ContentType "application/json" | SELECT -ExpandProperty Content | ConvertFrom-Json | SELECT * -ExcludeProperty "@odata.context"
       
        if ( $group -ne $null ) {
            
            Start-Sleep 60
            Add-MicrosoftTeamToO365Group -AccessToken $AccessToken -GroupId $group.Id

        }
        else {
            throw "Failed to create new O365 group"
        }
    }
    end {
        return $group
    }    
}

function Add-MicrosoftTeamToO365Group {
    [CmdletBinding()]
    param
    (
        [Parameter(Mandatory = $true)][string]$AccessToken,
        [Parameter(Mandatory = $true)][string]$GroupId
    )

    begin {
        $headers = Get-AuthenticationHeaders -AccessToken $AccessToken -IncludeContentTypeJson -IncludeAcceptJson
    }
    process {
        Invoke-WebRequest -Uri "https://graph.microsoft.com/v1.0/groups/$GroupId/team" -Headers $headers -Method Put -Body "{}" | SELECT -ExpandProperty Content | ConvertFrom-Json | SELECT * -ExcludeProperty "@odata.context"
    }
    end {
    }  
}

function Remove-InvalidChar {
    [CmdletBinding()]
    param
    (
        [Parameter(Mandatory = $true)][string]$String
    )

    $String = $String.TrimStart("_") # cannot start with underscore

    $sb = New-Object System.Text.StringBuilder
    
    foreach ( $char in $String.ToCharArray() ) {
        if ( [System.Char]::IsLetterOrDigit($char) -or $char -eq "-" -or $char -eq "_" ) {
            $sb.Append($char) | Out-Null
        }
    }

    return $sb.ToString()
}

function Update-GroupObjectProperties {
    [CmdletBinding()]
    param
    (
        [Parameter(Mandatory = $true)][string]$DisplayName,
        [Parameter(Mandatory = $true)][string]$MailNickname,
        [Parameter(Mandatory = $true)][Guid]$OnBehalfOfUserId,
        [Parameter(Mandatory = $true)][string]$AccessToken
    )

    begin {
        $headers = Get-AuthenticationHeaders -AccessToken $AccessToken -IncludeContentTypeJson

        $uri = "https://graph.microsoft.com/v1.0/directoryObjects/validateProperties"
    }
    process {
        # remove any special chars from the email alias
        $MailNickname = Remove-InvalidChar -String $MailNickname

        # validation body
        $json = @{
            entityType       = "Group"
            displayName      = $DisplayName
            mailNickname     = $MailNickname
            onBehalfOfUserId = $OnBehalfOfUserId
        } | ConvertTo-Json
        
        try {
            # call graph to validate the alias and display name
            Invoke-RestMethod -Method Post -Uri $uri -Body $json -Headers $headers 

            # values are valid, return the updated alias and display name
            return [PSCustomObject] @{
                DisplayName  = $DisplayName
                MailNickname = $MailNickname
            }
        }
        catch {
            # values are not valid
            if ( $_.Exception.Response.StatusCode.value__ -eq 422 <# UnprocessableEntity #>  ) {
                # read the HTTP repsonse body
                $responseStream = $_.Exception.Response.GetResponseStream()
                $reader = New-Object System.IO.StreamReader($responseStream)
                $reader.BaseStream.Position = 0
                $reader.DiscardBufferedData()

                $responseBody = $reader.ReadToEnd()
                $reader.Close()
                $reader.Dispose()
                $responseStream = $null
                
                $response = $responseBody | ConvertFrom-Json
                $policyDetails = $response.Error.details
                
                Write-host $policyDetails -ForegroundColor Yellow

                # loop through each validation error error
                foreach ( $policyDetail in $policyDetails ) {
                    if ( $policyDetail.code -eq "MissingPrefixSuffix" ) {
                       Write-Output "$(Get-Date) - 0Adding missing suffix or prefix"

                        if ( $policyDetail.target -eq "displayName" ) {
                           <# if ( -not [string]::IsNullOrWhiteSpace($policyDetail.prefix) ) {
                                Write-Output "$(Get-Date) - Adding Prefix '$($policyDetail.prefix)' to DisplayName"
                                $DisplayName = "$($policyDetail.prefix)$DisplayName"
                            }#>

                            if ( -not [string]::IsNullOrWhiteSpace($policyDetail.suffix) ) {
                                Write-Output "$(Get-Date) - Adding Suffix '$($policyDetail.suffix)' to DisplayName"
                                $DisplayName = "$DisplayName$($policyDetail.suffix)"
                            }
                        }
                        elseif ( $policyDetail.target -eq "mailNickname" ) {
                            <#if ( -not [string]::IsNullOrWhiteSpace($policyDetail.prefix) ) {
                                Write-Output "$(Get-Date) - Adding Prefix '$($policyDetail.prefix)' to MailNickname"
                                $MailNickname = "$($policyDetail.prefix)$MailNickname"
                            }#>

                            if ( -not [string]::IsNullOrWhiteSpace($policyDetail.suffix) ) {
                                Write-Output "$(Get-Date) - Adding Suffix '$($policyDetail.suffix)' to MailNickname"
                                $MailNickname = "$MailNickname$($policyDetail.suffix)"
                            }
                        }
                    }
                    elseif ($policyDetail.code -eq "PropertyConflict") {
                        $random = Get-random -Minimum 1000 -Maximum 9999

                        if ( $policyDetail.target -eq "displayName" ) {
                            Write-Output "$(Get-Date) - Adding random number to display name"
                            $DisplayName = "$($DisplayName)_$($random)"
                        }
                        elseif ( $policyDetail.target -eq "mailNickname" ) {
                            Write-Output "$(Get-Date) - Adding random number to mail nickname"
                            $MailNickname = "$($MailNickname)_$($random)"
                        }
                    }
                    elseif ($policyDetail.code -eq "InvalidLength") {
                        # can't fix this scenario w/o human intervention
                        throw $policyDetail.message
                    }
                    elseif ($policyDetail.code -eq "ContainsBlockedWord") {
                        # can't fix this scenario w/o human intervention
                        throw $policyDetail.message
                    }
                }

                return [PSCustomObject] @{
                    DisplayName  = $DisplayName
                    MailNickname = $MailNickname
                }
            }
            else {
                # unexpected error
                throw $_
            }
        }
    }
    end {
    }
}

function Get-AADUser {
    [CmdletBinding()]
    param
    (
        [Parameter(Mandatory = $true)][string]$AccessToken,
        [Parameter(Mandatory = $true)][string]$UserPrincipalName,
        [Parameter(Mandatory = $false)][switch]$ThrowIfNotUserFound
    )

    begin {
        $headers = Get-AuthenticationHeaders -AccessToken $AccessToken -IncludeContentTypeJson -IncludeAcceptJson

        $uri = "https://graph.microsoft.com/v1.0/users/$UserPrincipalName"
    }
    process {
        try {
            Invoke-RestMethod -Method Get -Uri $uri -Headers $headers
        }
        catch {
            if ( $_.Exception.Response.StatusCode.value__ -eq 404 ) {
                if ( $ThrowIfNotUserFound.IsPresent ) {
                    # expected when the user doesn't exist
                    $responseStream = $_.Exception.Response.GetResponseStream()
                    $reader = New-Object System.IO.StreamReader($responseStream)
                    $reader.BaseStream.Position = 0
                    $reader.DiscardBufferedData()

                    $responseBody = $reader.ReadToEnd()
                    $reader.Close()
                    $reader.Dispose()
                    $responseStream = $null
                
                    $response = $responseBody | ConvertFrom-Json
                    throw $response.error.message
                }

                return $null
            }
            else {
                throw $_.Exception
            }
        }
    }
    end {
    }
}


$graphAccessToken = Get-AccessToken -Tenant "$tenant.onmicrosoft.com" -ClientID $graphClientId -ClientSecret $graphClientSecret -Resource "https://graph.microsoft.com"

$aadPrimaryOwner = Get-AADUser -AccessToken $GraphAccessToken -UserPrincipalName $SiteBusinessOwner

####  enforce group naming policies ####
try {
    
    $SiteAliasUpd = $null
    $SiteTitleUpd = $null
    
    Write-output  "$(Get-Date) - Validating alias and display name properties to comply with defined naming policies"

    $updatedProperties = Update-GroupObjectProperties -DisplayName $SiteTitle -MailNickname $SiteAlias -OnBehalfOfUserId $($aadPrimaryOwner.id) -AccessToken $GraphAccessToken

    $SiteAliasUpd = $updatedProperties.MailNickname
    $SiteTitleUpd = $updatedProperties.DisplayName

    Write-output "$(Get-Date) - Updated Alias: '$SiteAliasUpd', Display Name: '$SiteTitleUpd'"
}
catch {

    Write-Output -Message "Error applying naming policies and making site URL unique. Error: $($_.Exception)"
                        
    continue
}


try {

    Write-output  "$(Get-Date) - Creating new Microsoft Team"

    $team = New-MicrosoftTeam `
        -AccessToken  $GraphAccessToken `
        -MailNickname $SiteAliasUpd `
        -DisplayName  $SiteTitleUpd `
        -Description  $SiteTitleUpd `
        -Owners       $($aadPrimaryOwner.Id) `
        -Visibility   "$SitePrivacy"

    if ($team) {

        $webUrl = $team.webUrl
        $isSiteCreated = $true
    }   
}
catch {
    Write-Error -Message "Error creating new Microsoft Team. Error: $($_.Exception)"

    # update request to Creation Failed
                        
    continue
}

if ($isSiteCreated) {
    
    Connect-PnPOnline -url $WizardUrl -Credentials $creds
     
    try {
        Set-PnPListItem -List $listName -Identity $($ItemId) -Values @{"SiteUrl" = "$webUrl"; "CreationStatus" = "Success"; "IsSiteCreated" = "$true"; "BusinessOwner" = "$SiteBusinessOwner" }

    }  

    catch {

        Write-Output "Error in updation list"
        Write-Output "Exception was caught: $($_.Exception.Message)" 

    } 

    Disconnect-PnPOnline
}

else {

    Connect-PnPOnline -url $WizardUrl -Credentials $creds

    try {

        Set-PnPListItem -List $listName -Identity $($ItemId) -Values @{"CreationStatus" = "Failed"; "IsSiteCreated" = "$false" }
        write-output "List item $ItemId has been update. Site $SiteTemplate has not been created"

    }  
    catch {

        Write-Output "Error in updation list"
        Write-Output "Exception was caught: $($_.Exception.Message)" 

    }  

    Disconnect-PnPOnline

}

Write-Output "PnP Provisioning Completed"

try {

    $Body = @{name = 'PnP Provisioning Completed' } | ConvertTo-Json
    Invoke-RestMethod -Method Post -Body $Body -Uri $callbackUrl -ErrorAction SilentlyContinue
    
}
catch {
    
}