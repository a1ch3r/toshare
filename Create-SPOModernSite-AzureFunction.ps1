$requestBody = Get-Content $req -Raw | ConvertFrom-Json
Write-Output "PowerShell script processed queue message '$requestBody'"

import-module `
    "D:\home\site\wwwroot\ModernSharePointSitesCreation\myModules\Custom_2020-03-03\SharePointPnPPowerShellOnline.psd1" -Global -ErrorAction SilentlyContinue

Write-Output "PnP Provisioning Started"

#region SharePoint variables (PROD)
$adminUrl = $env:adminUrl
$wizardUrl = $env:wizardUrl
$root = $env:root
$listName = "SiteCreationList"
#region SharePoint variables

$org_template = "D:\home\site\wwwroot\ModernSharePointSitesCreation\SitesTemplates\org-new\customerOrgSite.xml"
$org_template_resourses = "D:\home\site\wwwroot\ModernSharePointSitesCreation\SitesTemplates\org-new"

$div_template = "D:\home\site\wwwroot\ModernSharePointSitesCreation\SitesTemplates\dep-new\customerDepSite.xml"
$div_template_resourses = "D:\home\site\wwwroot\ModernSharePointSitesCreation\SitesTemplates\dep-new"

$doc_template = "D:\home\site\wwwroot\ModernSharePointSitesCreation\SitesTemplates\doc\customerDocSite.xml"

#region Credentials
$credUsername = $env:SiteCreation
$credPassword = $env:SiteCreationPassword | convertto-securestring -AsPlainText -Force
$creds = New-Object System.Management.Automation.PSCredential $credUsername, $credPassword
#region Credentials

#region SharePoint List Item Data
$Title = $requestBody.Title
[string]$SiteTitle = $($requestBody.Title -replace '[^\x30-\x39\x41-\x5A\x61-\x7A]+', '').Trim()
$SiteBusinessOwner = $requestBody.BusinessOwner
$SiteSecondaryOwner = $requestBody.SecondaryOwner
$SiteTemplate = $requestBody.SiteTemplate
$SiteExternalAccess = $requestBody.ExternalAccess
$SiteConfidentialityLevel = $requestBody.ConfidentialityLevel
[string]$SiteOrganization = $requestBody.Organization
[string]$SiteCategory = $($requestBody.SiteCategory -replace '[^\x30-\x39\x41-\x5A\x61-\x7A]+', '').Trim()
$ItemId = $requestBody.ItemId
$SiteLanguage = "1033"
$callbackUrl = $requestBody.callbackUrl;
#region SharePoint List Item Data

$SiteUrl = $null;
$SiteUrl = "$root/sites/$SiteOrganization-$SiteCategory-$SiteTitle"

$isSiteCreated = $false

$ListSettings = @{
    ListName      = "PolicyList"
    Hidden        = 1
    OnQuickLaunch = $false
    ContentTypesEnabled = 1

    ListFields    = @{
        PolicyName            = @{
            DisplayName  = "PolicyName"
            InternalName = "PolicyName"
            Type         = "Text"
        }
        PeriodOfTime          = @{
            DisplayName  = "PeriodOfTime"
            InternalName = "PeriodOfTime"
            Type         = "Text"
        }
        EventType             = @{
            DisplayName  = "EventType"
            InternalName = "EventType"
            Type         = "Choice"
        }
        TimeMeasure           = @{
            DisplayName  = "TimeMeasure"
            InternalName = "TimeMeasure"
            Type         = "Choice"   
        }
        PolicyState           = @{
            DisplayName  = "PolicyState"
            InternalName = "PolicyState"
            Type         = "Choice"  
        }
        PostPoneDate          = @{
            DisplayName  = "PostPoneDate"
            InternalName = "PostPoneDate"
            Type         = "Choice"  
        }
        AccumPostPoneDate     = @{
            DisplayName  = "AccumPostPoneDate"
            InternalName = "AccumPostPoneDate"
            Type         = "Integer"  
        }
        ServiceModified       = @{
            DisplayName  = "ServiceModified"
            InternalName = "ServiceModified"
            Type         = "DateTime"
        }
        BeforeServiceModified = @{
            DisplayName  = "BeforeServiceModified"
            InternalName = "BeforeServiceModified"
            Type         = "DateTime"
        }
    }
}

$PolicySettings = @{
    PolicyName   = "TeamSitePolicy"
    PeriodOfTime = "2"
    EventType    = "Modified"
    TimeMeasure  = "Year"
    PolicyState  = "On"
    PostPoneDate  = "None"
}

Function New-List ($Settings) {
    
    $web = Get-PnPWeb -Includes WebTemplate -ErrorAction Continue
    $list = Get-PnPList -Identity $($Settings.ListName) -Web $web -Includes Fields -ErrorAction SilentlyContinue
    
    if (!$list) {
        try {
            write-host "Creating policy list"
            $list = New-PnPList -Title $($Settings.ListName) -Url $("lists/$($Settings.ListName)") -Template GenericList -Web $web -ErrorAction Continue
            write-host "List has been created"
        }
        catch {
        
            write-host -f red $_.Exception.ToString()   

        }
    }
    else {
        write-host "Policy settings list already exists"
    } 
    return $list
}

Function Set-ListSettings ($List, $Settings) {
    
    #$web = Get-PnPWeb -Includes WebTemplate -ErrorAction Continue
    #$list = Get-PnPList -Identity $($Settings.ListName) -Web $web -Includes Fields -ErrorAction Continue
    try {
        $List.Hidden = $Settings.Hidden
        $List.OnQuickLaunch = $Settings.OnQuickLaunch
        #$List.ContentTypesEnabled = $Settings.ContentTypesEnabled
        $List.Update();
    }
    Catch {
        write-host -f red $_.Exception.ToString()   
  
    }
}

Function Add-ListFields ($List, $Settings) {

    write-host "Adding fields to policy settings list"
    #$web = Get-PnPWeb -Includes WebTemplate -ErrorAction Continue
    #$list = Get-PnPList -Identity $($Settings.ListName) -Web $web -Includes Fields -ErrorAction Continue

    $null = Add-PnPField -ErrorAction SilentlyContinue `
        -List $List `
        -DisplayName $($Settings.ListFields.PolicyName.DisplayName) `
        -InternalName $($Settings.ListFields.PolicyName.InternalName) `
        -Type $($Settings.ListFields.PolicyName.Type) -AddToDefaultView
    
    $null = Add-PnPField -ErrorAction SilentlyContinue `
        -List $List `
        -DisplayName $($Settings.ListFields.PeriodOfTime.DisplayName) `
        -InternalName $($Settings.ListFields.PeriodOfTime.InternalName) `
        -Type $($Settings.ListFields.PeriodOfTime.Type) `
        -AddToDefaultView
    
    $null = Add-PnPField -ErrorAction SilentlyContinue `
        -List $List `
        -DisplayName $($Settings.ListFields.EventType.DisplayName) `
        -InternalName $($Settings.ListFields.EventType.InternalName) `
        -Type $($Settings.ListFields.EventType.Type) `
        -AddToDefaultView -Choices "Modified", "Created"

    $null = Add-PnPField -ErrorAction SilentlyContinue `
        -List $List `
        -DisplayName $($Settings.ListFields.TimeMeasure.DisplayName) `
        -InternalName $($Settings.ListFields.TimeMeasure.InternalName) `
        -Type $($Settings.ListFields.TimeMeasure.Type) `
        -AddToDefaultView -Choices "Day", "Month", "Year"

    $null = Add-PnPField -ErrorAction SilentlyContinue `
        -List $List `
        -DisplayName $($Settings.ListFields.PolicyState.DisplayName) `
        -InternalName $($Settings.ListFields.PolicyState.InternalName) `
        -Type $($Settings.ListFields.PolicyState.Type) `
        -AddToDefaultView -Choices "On", "Off"

    $null = Add-PnPField -ErrorAction SilentlyContinue `
        -List $List `
        -DisplayName $($Settings.ListFields.PostPoneDate.DisplayName) `
        -InternalName $($Settings.ListFields.PostPoneDate.InternalName) `
        -Type $($Settings.ListFields.PostPoneDate.Type) `
        -AddToDefaultView -Choices "None", "3 months", "6 months", "1 year" 

    $null = Add-PnPField -ErrorAction SilentlyContinue `
        -List $List `
        -DisplayName $($Settings.ListFields.AccumPostPoneDate.DisplayName) `
        -InternalName $($Settings.ListFields.AccumPostPoneDate.InternalName) `
        -Type $($Settings.ListFields.AccumPostPoneDate.Type) `
        -AddToDefaultView

    $null = Add-PnPField -ErrorAction SilentlyContinue `
        -List $List `
        -DisplayName $($Settings.ListFields.ServiceModified.DisplayName) `
        -InternalName $($Settings.ListFields.ServiceModified.InternalName) `
        -Type $($Settings.ListFields.ServiceModified.Type) `
        -AddToDefaultView

    $null = Add-PnPField -ErrorAction SilentlyContinue `
        -List $List `
        -DisplayName $($Settings.ListFields.BeforeServiceModified.DisplayName) `
        -InternalName $($Settings.ListFields.BeforeServiceModified.InternalName) `
        -Type $($Settings.ListFields.BeforeServiceModified.Type) `
        -AddToDefaultView
}

Function Set-ListFields ($List, $Settings) {

    $PeriodOfTime = Get-PnPField -List $List -Identity $($Settings.ListFields.PeriodOfTime.InternalName)
    $PeriodOfTime.SetShowInEditForm($false)
    $PeriodOfTime.Context.ExecuteQuery()

    $PolicyName = Get-PnPField -List $List -Identity $($Settings.ListFields.PolicyName.InternalName)
    $PolicyName.SetShowInEditForm($false)
    $PolicyName.Context.ExecuteQuery()

    $AccumPostPoneDate = Get-PnPField -List $List -Identity $($Settings.ListFields.AccumPostPoneDate.InternalName)
    $AccumPostPoneDate.SetShowInEditForm($false)
    $AccumPostPoneDate.Context.ExecuteQuery()

    $ServiceModified = Get-PnPField -List $List -Identity $($Settings.ListFields.ServiceModified.InternalName)
    $ServiceModified.SetShowInEditForm($false)
    $ServiceModified.Context.ExecuteQuery()

    $BeforeServiceModified = Get-PnPField -List $List -Identity $($Settings.ListFields.BeforeServiceModified.InternalName)
    $BeforeServiceModified.SetShowInEditForm($false)
    $BeforeServiceModified.Context.ExecuteQuery()

    $EventType = Get-PnPField -List $List  -Identity $($Settings.ListFields.EventType.InternalName)
    $EventType.DefaultValue = "Modified"
    $EventType.Context.ExecuteQuery()
    $EventType.SetShowInEditForm($false)
    $EventType.Context.ExecuteQuery()

    $TimeMeasure = Get-PnPField -List $List -Identity $($Settings.ListFields.TimeMeasure.InternalName)
    $TimeMeasure.DefaultValue = "Year"
    $TimeMeasure.Context.ExecuteQuery()
    $TimeMeasure.SetShowInEditForm($false)
    $TimeMeasure.Context.ExecuteQuery()

    $PolicyState = Get-PnPField -List $List -Identity $($Settings.ListFields.PolicyState.InternalName)
    $PolicyState.DefaultValue = "On"
    $PolicyState.Context.ExecuteQuery()
    $PolicyState.SetShowInEditForm($false)
    $PolicyState.Context.ExecuteQuery()

    $PostPoneDate = Get-PnPField -List $List -Identity  $($Settings.ListFields.PostPoneDate.InternalName)
    $PostPoneDate.DefaultValue = "None"
    $PostPoneDate.Context.ExecuteQuery()
}

Function New-ListItem ($List, $Settings) {
    
    $items = Get-PnPListItem -List $List -ErrorAction SilentlyContinue

    if ($items.Count -eq 0) {

        Try {

            write-host "Creating policy settings item"

            $null = Add-PnPListItem `
                -ErrorAction SilentlyContinue `
                -List $List `
                -Values @{"Title" = "SitePolicy"; 
                "PolicyName"      = $Settings."PolicyName"; 
                "PeriodOfTime"    = $Settings."PeriodOfTime";
                "EventType"       = $Settings."EventType";
                "TimeMeasure"     = $Settings."TimeMeasure";
                "PolicyState"     = $Settings."PolicyState";
                "PostPoneDate"    = $Settings."PostPoneDate";
            }
            write-host "Item has been created"
        }
        Catch {
            write-host -f red $_.Exception.ToString()   
        }
    }
    else {
        Write-Host "Policy Settings Item already exists"
    }  
}

#region Site Creation Block
Connect-PnPOnline -url $adminUrl -Credentials $creds

try {

    New-PnPSite `
        -Type CommunicationSite `
        -Title $Title `
        -Url $SiteUrl `
        -LCID $SiteLanguage #-Owner $credUsername

    write-output "Site with template $SiteTemplate has been created $SiteUrl"

    $isSiteCreated = $true

}

catch {

    Write-Output "Error in creation $SiteTemplate"
    Write-Output "Exception was caught: $($_.Exception.Message)" 
    $isSiteCreated = $false

}
Disconnect-PnPOnline
#region Site Creation Block

#region SitesConfiguration
if ($isSiteCreated) {

    Connect-PnPOnline -url $("$SiteUrl") -Credentials $creds
        
    try {
        
        Add-PnPSiteCollectionAdmin -Owners "$credUsername"
        Add-PnPSiteCollectionAdmin -Owners "$SiteBusinessOwner"
        Add-PnPSiteCollectionAdmin -Owners "$SiteSecondaryOwner"
        Add-PnPSiteCollectionAdmin -Owners "ediscovery.team@customergroup.onmicrosoft.com"

        write-output "$credUsername, $SiteBusinessOwner, $SiteSecondaryOwner has been added in site admin collection list"

        $group = Get-PnPGroup -AssociatedOwnerGroup

        Add-PnPUserToGroup -LoginName $SiteBusinessOwner -Identity $group
        write-output "$SiteBusinessOwner has been added in group $group"
        Add-PnPUserToGroup -LoginName $SiteSecondaryOwner -Identity $group
        write-output "$SiteSecondaryOwner has been added in group $group"
            
        Get-PnPNavigationNode | Remove-PnPNavigationNode -Force
            
    }
    catch {
        Write-Output "Error in configuring site settings" 
        Write-Output "Exception was caught: $($_.Exception.Message)" 

    }


    if ($SiteExternalAccess -eq $true) {

        try {

            Set-PnPSite -Sharing ExternalUserSharingOnly

        }
        catch {
                
            Write-Output "Error with setting external sharing settings on site $SiteUrl"
            Write-Output "Exception was caught: $($_.Exception.Message)"   
        }
    }

    switch ($SiteTemplate) {

        "Communication Site Organization" { 

            try {

                start-sleep 10

                Apply-PnPProvisioningTemplate `
                    -Path $org_template `
                    -ResourceFolder $org_template_resourses `
                    -ExcludeHandlers SiteSecurity, Lists `
                    -ErrorAction Continue
                
                write-output "Template $org_template has been applied on site $SiteUrl"
            }
            catch {
                Write-Output "Error in applying template $org_template on site $SiteUrl"
                Write-Output "Exception was caught: $($_.Exception.Message)"    
            }
  
        }

        "Communication Site Department" {
                    
            try {
                Set-PnPTraceLog -On -WriteToConsole -Level Debug
                start-sleep 10
                
                Apply-PnPProvisioningTemplate `
                    -Path $div_template `
                    -ResourceFolder $div_template_resourses `
                    -ExcludeHandlers SiteSecurity, Lists, Navigation -ClearNavigation `
                    -ErrorAction Continue

                write-output "Template $div_template has been applied on site $SiteUrl"

                Set-PnPTraceLog -off

            }
            catch {
                Write-Output "Error in applying template $div_template on site $SiteUrl"
                Write-Output "Exception was caught: $($_.Exception.Message)"
            }
        }

        "Document Center" {
                    
            try {
                Set-PnPTraceLog -On -WriteToConsole -Level Debug
                
                Apply-PnPProvisioningTemplate `
                    -Path $doc_template `
                    -ExcludeHandlers SiteSecurity, Lists, Navigation -ClearNavigation -ErrorAction Continue

                write-output "Template $doc_template has been applied on site $SiteUrl"
                
                Set-PnPTraceLog -off

            }
            catch {
                Write-Output "Error in applying template $doc_template on site $SiteUrl"
                Write-Output "Exception was caught: $($_.Exception.Message)"
            }
        }
    }    
  
    #region TemplateConfiguration
    switch ($SiteOrganization) {

        "customer" {

            Set-PnPWebTheme -Theme "customer Division" –WebUrl $("$SiteUrl")
            write-output "Theme customer Division has been applied on $SiteUrl"

        }

        "Gardena" {

            Set-PnPWebTheme -Theme "Gardena Division" –WebUrl $("$SiteUrl")
            write-output "Theme customer Division has been applied on $SiteUrl"

        }

        "Group" {

            Set-PnPWebTheme -Theme "Group Division" –WebUrl $("$SiteUrll")
            write-output "Theme Group Division has been applied on $SiteUrl" 

        }
    }
    #region TemplateConfiguration
  
    #region Sharing configuration
    switch ($SiteConfidentialityLevel) {

        "Open" { 
           
            try {

                $realm = Get-PnPAuthenticationRealm
                $loginName = "c:0-.f|rolemanager|spo-grid-all-users/$realm"
                $group = Get-PnPGroup -AssociatedVisitorGroup
                Add-PnPUserToGroup -LoginName $loginName -Identity $group

            }
            catch {
                    
            }
            
        }

        "Internal" { }
        
        "Private" { }

    } #region Sharing configuration

    $PolicyList = New-List -Settings $ListSettings

    if ($PolicyList) {

        Set-ListSettings -List $PolicyList -Settings $ListSettings
        Add-ListFields -List $PolicyList -Settings $ListSettings
        Set-ListFields -List $PolicyList -Settings $ListSettings
        New-ListItem -List $PolicyList -Settings $PolicySettings

    }

    Disconnect-PnPOnline
}

if ($isSiteCreated) {

    Connect-PnPOnline -url $WizardUrl -Credentials $creds

    try {

        Set-PnPListItem -List $listName -Identity $($ItemId) -Values @{"SiteUrl" = "$SiteUrl"; "CreationStatus" = "Success"; "IsSiteCreated" = "$true" }
        write-output "List item $ItemId has been update. Site $SiteTemplate has been created"

    }  
    catch {


    }  

    Disconnect-PnPOnline
}
else {

    Connect-PnPOnline -url $WizardUrl -Credentials $creds

    try {

        Set-PnPListItem -List $listName -Identity $($ItemId) -Values @{"CreationStatus" = "Failed"; "IsSiteCreated" = "$false" }
        write-output "List item $ItemId has been update. Site $SiteTemplate has not been created"

    }  
    catch {

        Write-Output "Error in updation list"
        Write-Output "Exception was caught: $($_.Exception.Message)" 

    }  

    Disconnect-PnPOnline

}

Write-Output "PnP Provisioning Completed"

try {

    $Body = @{name = 'PnP Provisioning Completed' } | ConvertTo-Json
    Invoke-RestMethod -Method Post -Body $Body -Uri $callbackUrl -ErrorAction SilentlyContinue
    
}
catch {
    
}
