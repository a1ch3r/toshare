Function Connect-Teams {

param (
    $creds,
    [ValidateSet("Connect","Disconnect")][string]$mode   
)

    switch ($mode) {

        "Connect" {

            try {
                Connect-MicrosoftTeams -Credential $creds
            }
            catch {
                $ErrorMessage = $_.Exception.Message
                $FailedItem = $_.Exception.ItemName
                write-host $ErrorMessage -ForegroundColor Red
				break;
            }
                   
        }
        "Disconnect" {
                try {
                    Disconnect-MicrosoftTeams
                }
                catch {
                }
        }
    }
}

Function Get-Teams {
    try {
        Get-Team
    }
    catch {   
        break;
    }    
}

Function Main {
    
     Connect-Teams -creds $parameters.Credentials -mode Connect

    $parameters = [PSCustomObject]@{
        Credentials = "" #type creds for account to connect MS Teams
    }

    $Teams = $(Get-Teams)
    
    If ($Teams) {     
        $Objects = @()      
        foreach ($Team in $(Get-Teams)) {
            $object = [pscustomobject]@{
                TeamGroupID = $($Team.GroupId)
                TeamDisplayName = $($Team.DisplayName)
                TeamDescription= $($Team.Description)
                TeamOwner = [psobject]$(Get-TeamUser -GroupId $team.GroupId -Role Owner)
                TeamMembers = [psobject]$(Get-TeamUser -GroupId $team.GroupId -Role Member)
            }
            $Objects += $object
        }
        $Objects

        Connect-Teams -creds $parameters.Credentials -mode Disconnect
    }
    else {
        write-host "Now groups have been found "
    }
}
Main