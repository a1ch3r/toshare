data "azurerm_resource_group" "rgd" {

  name = module.Azure_RG.resource_group_name
  
  depends_on = [
    module.Azure_RG
  ]
}

data "azurerm_storage_account" "std" {

  name                = module.Azure_SA.storage_account_name
  resource_group_name = module.Azure_RG.resource_group_name

  depends_on = [
    module.Azure_RG,
    module.Azure_SA
  ]
}


data "azurerm_function_app" "fad" {

  name                = module.Azure_APP-SERVICE.app_service_name
  resource_group_name = module.Azure_RG.resource_group_name


  depends_on = [
    module.Azure_RG,
    module.Azure_APP-SERVICE
  ]
}


