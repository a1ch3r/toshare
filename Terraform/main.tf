locals {

  rg_name      = lower(format("%s-%s-%s", "rg", "${var.account_name}", "${var.application_env}"))
  sa_name      = lower(format("%s%s", "${var.account_name}", "${var.application_env}"))
  appplan_name = lower(format("%s-%s", "${var.account_name}", "${var.application_env}"))
  appfunc_name = lower(format("%s-%s-%s", "func", "${var.account_name}", "${var.application_env}"))
  sql_name     = lower(format("%s-%s-%s", "sqls", "${var.account_name}", "${var.application_env}"))
  db_name      = lower(format("%s-%s-%s", "sqld", "${var.account_name}", "${var.application_env}"))
}

module "Azure_RG" {
  source              = "./Modules/Resource_Group"
  resource_group_name = local.rg_name
  location            = var.location
  tags = {
    environment = var.application_env
    Owner       = var.account_name
  }
}

module "Azure_SA" {

  source                   = "./Modules/Storage_Account"
  storage_account_name     = local.sa_name
  resource_group_name      = data.azurerm_resource_group.rgd.name
  location                 = data.azurerm_resource_group.rgd.location
  account_tier             = "Standard"
  account_replication_type = "LRS"
  tags = {
    environment = var.application_env
    Owner       = var.account_name
  }
}

module "Azure_APP-SERVICE" {

  source = "./Modules/Azure_WebApp"

  ##General
  resource_group_name = data.azurerm_resource_group.rgd.name
  location            = data.azurerm_resource_group.rgd.location

  ##App plan
  app_service_plan_name = local.appplan_name
  
  service_plan = {
    size = "S1"
    tier = "S1"
  }

  ##App function
  app_function_name = local.appfunc_name

  storage_account_name       = data.azurerm_storage_account.std.name
  storage_account_access_key = data.azurerm_storage_account.std.primary_access_key

}

module "Azure_SQL-SERVER" {

  source              = "./Modules/Azure_MSSql"
  resource_group_name = data.azurerm_resource_group.rgd.name
  location            = data.azurerm_resource_group.rgd.location

  sqlserver_name               = local.sql_name
  database_name                = local.db_name

  sql_database_edition         = "Standard"
  sqldb_service_objective_name = "S1"
  minimum_tls_version          = "1.2"

  license_type   = "LicenseIncluded"
  max_size_gb    = 4
  read_scale     = true
  sku_name       = "BC_Gen5_2"
  zone_redundant = true

  # Firewall Rules to allow azure and external clients and specific Ip address/ranges. 
  enable_firewall_rules = true
  firewall_rules = [
    {
      name             = "access-to-azure"
      start_ip_address = "0.0.0.0"
      end_ip_address   = "0.0.0.0"
    },
    {
      name             = "desktop-ip"
      start_ip_address = "0.0.0.0"
      end_ip_address   = "0.0.0.0"
    }
  ]
}

resource "null_resource" "PowerShellScriptRunFirstTimeOnly" {
  triggers = {
    trigger = "${uuid()}"
  }
  provisioner "local-exec" {
    command = "powershell -file './Modules/Helpers/Update-AzFunction.ps1' -name ${local.appfunc_name} -ResourceGroupName ${local.rg_name} -k 'Aleksei' -v 'Chernikov' -Subscription ${var.subscription_id}"
    interpreter = [
      "PowerShell",
      "-Command"
    ]
  }
}
