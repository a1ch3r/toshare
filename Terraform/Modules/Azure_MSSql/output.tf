#Resource group module ouput.tf file
output "sql_server_id" {
  description = "The primary Microsoft SQL Server ID"
  value       = azurerm_sql_server.sql.id
}
output "sql_server_admin_user" {
  description = "SQL database administrator login id"
  value       = azurerm_sql_server.sql.administrator_login
  sensitive   = true
}

output "sql_server_admin_password" {
  description = "SQL database administrator login password"
  value       = azurerm_sql_server.sql.administrator_login_password
  sensitive   = true
}

output "sql_database_id" {
  description = "The SQL Database ID"
  value       = azurerm_sql_database.db.id
}

output "sql_database_name" {
  description = "The SQL Database Name"
  value       = azurerm_sql_database.db.name
}