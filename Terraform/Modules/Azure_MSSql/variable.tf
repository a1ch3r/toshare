#Storage Account module variable.tf file
variable "sqlserver_name" {
  description = "SQL server Name"
  default     = ""
}

variable "database_name" {
  description = "The name of the database"
  default     = ""
}

variable "admin_username" {
  description = "The administrator login name for the new SQL Server"
  default     = ""
}

variable "admin_password" {
  description = "The password associated with the admin_username user"
  default     = ""
}

variable "resource_group_name" {
    description = ""
    type = string
}

variable "location" {
    description = ""
    type = string
}

variable "minimum_tls_version" {
    description = ""
    type = string
}

variable "license_type" {
    description = ""
    type = string
}

variable "max_size_gb" {
    description = ""
    type = number
}

variable "read_scale" {
    description = ""
    type = bool
}

variable "sku_name" {
    description = ""
    type = string
}

variable "zone_redundant" {
    description = ""
    type = bool
}


variable "sql_database_edition" {
  description = "The edition of the database to be created"
  default     = "Standard"
}

variable "sqldb_service_objective_name" {
  description = " The service objective name for the database"
  default     = "S1"
}

variable "enable_firewall_rules" {
  description = "Manage an Azure SQL Firewall Rule"
  default     = false
}

variable "firewall_rules" {
  description = "Range of IP addresses to allow firewall connections."
  type = list(object({
    name             = string
    start_ip_address = string
    end_ip_address   = string
  }))
  default = []
}

variable "tags" {
  description = "A map of tags to add to all resources"
  type        = map(string)
  default     = {}
}