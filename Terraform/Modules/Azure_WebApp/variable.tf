#Storage Account module variable.tf file
variable "app_service_plan_name" {
    description = "The name of the module demo resource group in which the resources will be created"
    type = string
}

variable "service_plan" {
  description = "Definition of the dedicated plan to use"
  type = object({
    size             = string
    tier             = string
  })
}

variable "tags" {
  description = "A map of tags to add to all resources"
  type        = map(string)
  default     = {}
}

variable "app_function_name" {
  description = "Specifies the name of the App Service."
  default     = ""
}

variable "resource_group_name" {
    description = "The name of the module demo resource group in which the resources will be created"
    type = string
}
variable "location" {
    description = "The name of the module demo resource group in which the resources will be created"
    type = string
}

variable "storage_account_name" {
    description = ""
    type = string
}

variable "storage_account_access_key" {
    description = ""
    type = string
}

