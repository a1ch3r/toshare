#App plan module main.tf file

resource "azurerm_app_service_plan" "asp" {
  name                = format("plan-%s", lower(var.app_service_plan_name))
  resource_group_name = var.resource_group_name
  location            = var.location
  tags                = merge({ "ResourceName" = format("%s", var.app_service_plan_name) }, var.tags, )

  sku {
    tier     = var.service_plan.tier
    size     = var.service_plan.size
  }
}

resource "azurerm_function_app" "fa" {
  name                       = var.app_function_name
  location                   = var.location
  resource_group_name        = var.resource_group_name
  app_service_plan_id        = azurerm_app_service_plan.asp.id
  storage_account_name       = var.storage_account_name
  storage_account_access_key = var.storage_account_access_key
  tags                    = merge({ "ResourceName" = lower(format("app-%s", var.app_function_name)) }, var.tags )
}