#Resource group module ouput.tf file
output "app_service_plan_id" {
  description = "The resource ID of the App Service Plan component"
  value       = azurerm_app_service_plan.asp.id
}

output "app_service_id" {
  description = "The resource ID of the App Service component"
  value       = azurerm_function_app.fa.id
}


output "app_service_name" {
  description = "The resource ID of the App Service component"
  value       = azurerm_function_app.fa.name
}