Function AW_Get-BasicUserForAuth {
    Param(
        [string]$func_username
    )

    $encoding = [System.Text.Encoding]::ASCII.GetBytes($func_username);
    $encodedString = [Convert]::ToBase64String($encoding);
    
    $fullencoded = "Basic " + $encodedString;

    Write-Verbose("");
    Write-Verbose("---------- Basic Auth ----------");
    Write-Verbose("Encoded String: " + $fullencoded);
    Write-Verbose("--------------------------------");
    Write-Verbose("");

    Return "Basic " + $encodedString;
}

Function AW_Build-Headers {
    Param(
        [string]$authorizationString,
        [string]$tenantCode,
        [string]$acceptType,
        [string]$contentType
    )
    
    $header = @{
        "Authorization" = $authorizationString;
        "aw-tenant-code" = $tenantCode;
        #"Accept" = $accept;
        "Content-Type" = $contentType
    }

    Write-Verbose("");
    Write-Verbose("---------- Headers ----------");
    Write-Verbose("Authorization: " + $authorizationString);
    Write-Verbose("aw-tenant-code: " + $tenantCode);
    Write-Verbose("Accept: " + $acceptType);
    Write-Verbose("Content-Type: " + $contentType);
    Write-Verbose("-----------------------------");
    Write-Verbose("");

    Return $header
}

Function Get-AW_Data  {
    
    Param(
        [string]$_aw_UserName,
        [string]$_aw_Password,
        [string]$_aw_EndpointURL,
        [string]$_aw_TenantAPIKey,
        [string]$_aw_jsonpath
    )

    $AW_baseURL = $_aw_EndpointURL + "/API/"  
    $AW_contentType = "application/json"     
    #$AW_userInfo = $_aw_UserName + ":" + $_aw_Password 
    $AW_userInfo = $_aw_UserName  + ":" + $_aw_Password
    $AW_restUser = AW_Get-BasicUserForAuth $AW_userInfo 
    $AW_headers = AW_Build-Headers $AW_restUser $_aw_TenantAPIKey $AW_contentType $AW_contentType
    $AW_changeURL = $AW_baseURL + "mdm/devices/search?pageSize=10000";
    try {
        $request = Invoke-RestMethod -Uri $AW_changeURL -Headers $AW_headers -OutFile $_aw_jsonpath
        Write-Log -Message "Data from AirWatch has been collected" -Path $global:aw_logpath -Level Info

    }
    catch {
        Write-Log -Message "$($_.Exception.Message)" -Path $global:aw_errorslogpath -Level Error
        break;
    }
   <# If ($Proxy) {
        
        If ($UserAgent) {
            $request = Invoke-RestMethod -Uri $AW_changeURL -Headers $AW_headers -Proxy $Proxy -UserAgent $UserAgent -OutFile $_aw_jsonpath
        } Else {
            $request = Invoke-RestMethod -Uri $AW_changeURL -Headers $AW_headers -Proxy $Proxy -OutFile $_aw_jsonpath
        }

    }
    Else {       
        If ($UserAgent) {
            $request = Invoke-RestMethod -Uri $AW_changeURL -Headers $AW_headers  -UserAgent $UserAgent -OutFile $_aw_jsonpath
        } Else {
            # Perform request
            $request = Invoke-RestMethod -Uri $AW_changeURL -Headers $AW_headers -OutFile $_aw_jsonpath
        }
    }   #>
        return $request
}

Function Create-XML {
    param (
        [System.Object]$_Device    
    )
    $SOAPRequest = $null;
   
    # Device.SerialNumber
    $Device_SerialNumber = $Device.SerialNumber
       if ($Device_SerialNumber -eq "") {
           $Device_SerialNumber = "-"
        }

    $Device_MacAddress = $Device.MacAddress
        if ($Device_MacAddress -eq "") {
            $Device_MacAddress = "-"
        }

        # Device.Imei
    $Device_Imei = $Device.Imei
        if ($Device_Imei -eq "") {
            $Device_Imei = "-"
        }

        # Device.AssetNumber
        $Device_AssetNumber = $Device.AssetNumber
        if ($Device_AssetNumber -eq "") {
            $Device_AssetNumber = "-"
        }

        # Device.DeviceFriendlyName
        $Device_DeviceFriendlyName = $Device.DeviceFriendlyName
        if ($Device_DeviceFriendlyName -eq "") {
            $Device_DeviceFriendlyName = "-"
        }
    
        # Device.UserName
        $Device_UserName = $Device.UserEmailAddress
        if ($Device_UserName -eq "") {
            $Device_UserName = "-"
        }

        # Device.Platform
        $Device_Platform = $Device.Platform
        if ($Device_Platform -eq "") {
            $Device_Platform = "-"
        }

        # Device.Model
        $Device_Model = $Device.Model
        if ($Device_Model -eq "") {
            $Device_Model = "-"
        }

        # Device.OperatingSystem
        $Device_OperatingSystem = $Device.OperatingSystem
        if ($Device_OperatingSystem -eq "") {
            $Device_OperatingSystem = "-"
        }

        # Device.PhoneNumber
        $Device_PhoneNumber = $Device.PhoneNumber
        if ($Device_PhoneNumber -eq "") {
            $Device_PhoneNumber = "-"
        }

        # Device.EnrollmentStatus
        $Device_EnrollmentStatus = $Device.EnrollmentStatus
        if ($Device_EnrollmentStatus -eq "") {
            $Device_EnrollmentStatus = "-"
        }

        # Device.ComplianceStatus
        $Device_ComplianceStatus = $Device.ComplianceStatus
        if ($Device_ComplianceStatus -eq "") {
            $Device_ComplianceStatus = "-"
        }

        # Device.AssetNumber
        $Device_AssetNumber = $Device.AssetNumber
        if ($Device_AssetNumber -eq "") {
            $Device_AssetNumber = "-"
        }

        $SOAPRequest = [xml]@"
        <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:u="http://www.service-now.com/u_domain_airwatch_mobile_devices">
            <soapenv:Header/>
				<soapenv:Body>
					<u:insert>
						<!--Optional:-->
						<u:import_set_run></u:import_set_run>
						<!--Optional:-->
						<u:template_import_log></u:template_import_log>
						<!--Optional:-->
						<u:u_compliancestatus>$Device_ComplianceStatus</u:u_compliancestatus>
						<!--Optional:-->
						<u:u_devicefriendlyname>$Device_DeviceFriendlyName</u:u_devicefriendlyname>
						<!--Optional:-->
						<u:u_enrollmentstatus>$Device_EnrollmentStatus</u:u_enrollmentstatus>
						<!--Optional:-->
						<u:u_imei>$Device_Imei</u:u_imei>
						<!--Optional:-->
						<u:u_macaddress>$Device_MacAddress</u:u_macaddress>
						<!--Optional:-->
						<u:u_modelid_name>$Device_Model</u:u_modelid_name>
						<!--Optional:-->
						<u:u_operatingsystem>$Device_OperatingSystem</u:u_operatingsystem>
						<!--Optional:-->
						<u:u_phonenumber>$Device_PhoneNumber</u:u_phonenumber>
						<!--Optional:-->
						<u:u_platform>$Device_Platform</u:u_platform>
						<!--Optional:-->
						<u:u_serialnumber>$Device_SerialNumber</u:u_serialnumber>
						<!--Optional:-->
						<u:u_username>$Device_UserName</u:u_username>
                        <!--Optional:-->
						<u:u_assetnumber>$Device_AssetNumber</u:u_assetnumber>
					  </u:insert>
				    </soapenv:Body>
            </soapenv:Envelope>
"@
    if ($SOAPRequest.Envelope.Body.insert.u_operatingsystem) {
        return $SOAPRequest
    } 
}

Function Open-Connection {
    param (
        [string]$SN_WSDLurl,
        [string]$LogPath
    )
    try {
        $wsproxy = New-WebServiceProxy -uri $SN_WSDLurl -Credential $SN_Creds    
        Write-Log -Message "Connection to $SN_WSDLurl has been opened" -Path $global:aw_logpath -Level Info
    }
    catch {
        Write-Log -Message "$($_.Exception.Message)" -Path $global:aw_errorslogpath -Level Error
        break;    
    }
    return $wsproxy
}

Function Send-toServiceNow {
    
    param (
        [string]$SN_WSDLurl,
        [string]$SN_SOAPurl,
        [System.Object]$SN_Creds,
        [xml]$SOAPRequest,
        [string]$LogPath
    )
    try {

        #Write-Host "Sending SOAP request to server: $SN_SOAPurl"
        $soapWebRequest = [System.Net.WebRequest]::Create($SN_SOAPurl)
        $soapWebRequest.Headers.Add("SOAPAction","`"`"")
        $soapWebRequest.ContentType = "text/xml;charset=`"utf-8`""
        $soapWebRequest.Accept = "text/xml"
        $soapWebRequest.Method = "POST"
        $soapWebRequest.Credentials = $SN_Creds

        #write-host "Initiating Send."
        $requestStream = $soapWebRequest.GetRequestStream()
        $SOAPRequest.Save($requestStream)
        $requestStream.Close()

        #write-host "Send Complete, Waiting For Response."
        $resp = $soapWebRequest.GetResponse()
        $responseStream = $resp.GetResponseStream()
        $soapReader = [System.IO.StreamReader]($responseStream)
        $ReturnXml = $soapReader.ReadToEnd()
        $responseStream.Close()

    }
    catch {
        Write-Log -Message "$($_.Exception.Message)" -Path $global:aw_errorslogpath -Level Error  
        continue;   
    }
    return $ReturnXml
}

Function Write-Log {
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory=$true,
                   ValueFromPipelineByPropertyName=$true)]
        [ValidateNotNullOrEmpty()]
        [Alias("LogContent")]
        [string]$Message,

        [Parameter(Mandatory=$false)]
        [Alias('LogPath')]
        [string]$Path='C:\Logs\PowerShellLog.log',
        
        [Parameter(Mandatory=$false)]
        [ValidateSet("Error","Info","Inserted", "Ignored", "Updated")]
        [string]$Level="Info",
        
        [Parameter(Mandatory=$false)]
        [switch]$NoClobber
    )

    Begin
    {
        # Set VerbosePreference to Continue so that verbose messages are displayed.
        $VerbosePreference = 'Continue'
    }
    Process
    {
        
        # If the file already exists and NoClobber was specified, do not write to the log.
        if ((Test-Path $Path) -AND $NoClobber) {
            Write-Error "Log file $Path already exists, and you specified NoClobber. Either delete the file or specify a different name."
            Return
            }

        # If attempting to write to a log file in a folder/path that doesn't exist create the file including the path.
        elseif (!(Test-Path $Path)) {
            Write-Verbose "Creating $Path."
            $NewLogFile = New-Item $Path -Force -ItemType File
            }

        else {
            # Nothing to see here yet.
            }

        # Format Date for our Log File
        $FormattedDate = Get-Date -Format "yyyy-MM-dd HH:mm:ss"

        # Write message to error, warning, or verbose pipeline and specify $LevelText
        switch ($Level) {
            'Error' {
                Write-Error $Message
                $LevelText = 'ERROR:'
                }
            'Info' {
                Write-Verbose $Message
                $LevelText = 'INFO:'
                }
            'Inserted' {
                #Write-Warning $Message
                $LevelText = 'INSERTED:'
                }
            'Ignored' {
                #Write-Verbose $Message
                $LevelText = 'IGNORED:'
                }
            'Updated' {
                #Write-Verbose $Message
                $LevelText = 'UPDATED:'
                }
            }
        
        # Write log entry to $Path
        "$FormattedDate $LevelText $Message" | Out-File -FilePath $Path -Append
    }
    End
    {
    }
}

Function Main {

    Write-Log -Message "Starting the process of device synchronization" -Path $global:aw_logpath -Level Info
    
    [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls -bor [Net.SecurityProtocolType]::Tls11 -bor [Net.SecurityProtocolType]::Tls12

    $settingsPath = Get-Content "C:\_config\domain-settings - prod.json" | ConvertFrom-Json
    
    $aw_UserName = $settingsPath.Integration_Settings.AirWatch.UserName
    $aw_Password = $settingsPath.Integration_Settings.AirWatch.Password
    $aw_EndpointURL = $settingsPath.Integration_Settings.AirWatch.EndpointURL
    $aw_TenantAPIKey = $settingsPath.Integration_Settings.AirWatch.TenantAPIKey

    $SN_UserName = $settingsPath.Integration_Settings.ServiceNow.UserName
    $SN_Password = $settingsPath.Integration_Settings.ServiceNow.Password
    $SN_Creds = New-Object System.Management.Automation.PSCredential $SN_UserName, (ConvertTo-SecureString $SN_Password -AsPlainText -Force)
    $SN_WSDLurl = $settingsPath.Integration_Settings.ServiceNow.WSDLurl
    $SN_SOAPurl = $settingsPath.Integration_Settings.ServiceNow.SOAPurl

    if (!(Test-Path -Path $settingsPath.Integration_Settings.'System Folders'.Data)) {
        New-Item $($settingsPath.Integration_Settings.'System Folders'.Data) -ItemType Directory -Force    
    }
    if (!(Test-Path -Path $settingsPath.Integration_Settings.'System Folders'.Devices)) {
        New-Item $($settingsPath.Integration_Settings.'System Folders'.Devices) -ItemType Directory -Force    
    }
     if (!(Test-Path -Path $settingsPath.Integration_Settings.'System Folders'.Logs)) {
        New-Item $($settingsPath.Integration_Settings.'System Folders'.Logs) -ItemType Directory -Force    
    }
    if (!(Test-Path -Path $settingsPath.Integration_Settings.'System Folders'.Errors)) {
        New-Item $($settingsPath.Integration_Settings.'System Folders'.Errors) -ItemType Directory -Force    
    }

    $aw_jsonpath = "$($settingsPath.Integration_Settings.'System Folders'.Data)\domain-devices-data-$((get-date).ToString("yyyy-MM-dd")).json" 
    $global:aw_logpath = "$($settingsPath.Integration_Settings.'System Folders'.Logs)\domain-devices-log-$((get-date).ToString("yyyy-MM-dd")).log" 
    $global:aw_errorslogpath = "$($settingsPath.Integration_Settings.'System Folders'.Errors)\domain-errors-log-$((get-date).ToString("yyyy-MM-dd")).log" 
    $aw_exportpath = "$($settingsPath.Integration_Settings.'System Folders'.Devices)\domain-devices-push-$((get-date).ToString("yyyy-MM-dd")).csv" 

    if(!(test-path -Path $aw_jsonpath)){
        Get-AW_Data -_aw_UserName $aw_UserName -_aw_Password $aw_Password -_aw_EndpointURL $aw_EndpointURL -_aw_TenantAPIKey $aw_TenantAPIKey -_aw_jsonpath $aw_jsonpath
    }
    else {
        Write-Log -Message "File $aw_jsonpath already exists" -Path $aw_logpath -Level Info    
    }
   
   if  (Get-Content $aw_jsonpath) {
        $data = Get-Content $aw_jsonpath | ConvertFrom-Json
   }
     
   if ($data.Devices) {

       Open-Connection -SN_WSDLurl $SN_WSDLurl -LogPath $aw_logpath | Out-Null
        
       Foreach ($Device in $data.Devices | select -First 5) {
    
            $Device | Select `
                DeviceFriendlyName, `
                @{L="PhoneNumber";E={[string]($_.PhoneNumber)}}, `
                MacAddress, `
                @{L="SerialNumber";E={[string]($_.SerialNumber)}}, `
                @{L="Imei";E={[string]($_.Imei)}}, `
                EnrollmentStatus, `
                Model,`
                UserEmailAddress,`
                AssetNumber,`
                Platform,`
                @{L="OperatingSystem"; E={" " + $_.OperatingSystem}},`
                ComplianceStatus, @{L="LastSeen"; E={(get-date(($_.LastSeen))).tostring("dd-MM-yyyy")}} , LocationGroupName | Export-Csv -Path $aw_exportpath -NoTypeInformation -Append

            [xml]$request = $null;
            [xml]$response = $null;
            [xml]$request = Create-XML -_Device $Device 
            [xml]$response = Send-toServiceNow -SN_Creds $SN_Creds -SN_WSDLurl $SN_WSDLurl -SN_SOAPurl $SN_SOAPurl -SOAPRequest $request -LogPath $aw_logpath
                             
            if (($response.Envelope.Body.insertResponse.status) -eq "inserted") {
                Write-Log -Message "SerialNumber:$($Device.SerialNumber);UserName:$($Device.UserEmailAddress);DeviceName:$($Device.DeviceFriendlyName)" -Path $global:aw_logpath -Level Inserted
            }
            elseif (($response.Envelope.Body.insertResponse.status) -eq "ignored") {
                Write-Log -Message "SerialNumber:$($Device.SerialNumber);UserName:$($Device.UserEmailAddress);DeviceName:$($Device.DeviceFriendlyName)" -Path $global:aw_logpath -Level Ignored
            }
            elseif (($response.Envelope.Body.insertResponse.status) -eq "updated") {
                Write-Log -Message "SerialNumber:$($Device.SerialNumber);UserName:$($Device.UserEmailAddress);DeviceName:$($Device.DeviceFriendlyName)" -Path $global:aw_logpath -Level Updated
            }
            else {
                Write-Log -Message "$($response.Envelope.Body.insertResponse.status)" -Path $aw_logpath -Level Info
                Write-Log -Message "SerialNumber:$($Device.SerialNumber);UserName:$($Device.UserEmailAddress);DeviceName:$($Device.DeviceFriendlyName)" -Path $global:aw_logpath -Level Info
            }
        }
        Write-Log -Message "Finishing the process of device synchronization" -Path $global:aw_logpath -Level Info
    }
    else {
        Write-Log -Message "Devices synchronization has not been finixhed" -Path $global:aw_errorslogpath -Level Error
    }
}
Main
