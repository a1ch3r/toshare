<#
.SYNOPSIS
    Correlate existing System Variables (including Environment Variables) with Template Parameters set in
    Template Spec of a project.
.DESCRIPTION
    Current script will attemp to create a Hashtable-object for a deployment of a Template Spec correllating 
    existing system Variables to Template Spec Parameters. Prefixed variables (like Infra.IRROPS.AppServicePlan.SKU)
    and non-prefixed variables (like Workload) will be correlated by name and their values will be passed to deployment.
.PARAMETER ResourceGroupName
    A name of the Resource Group the deployment will be applyed to.
.PARAMETER ProjectName
    A name of the Project to deploy.
.PARAMETER Env
    Current Environment name.
.PARAMETER date
    [optional]Postfix for a deployment. Defaults to (Get-Date -Format "dd-MM-yyyy-HH-mm-ss").
.PARAMETER ParentSpecVersion
    [optional] A specific version of a Template Spec can be deployed. If not set - the lates version is deployed.
.PARAMETER VariablesPrefix
    [optional] Variable name prefix used to correlate Environmental variables received from Variable Groups.
    Defaults to "infra_${ProjectName}_". Be cautios changing this value: keep in mind, that "dots" in Variable Names from
    Variable Groups are converted into lower-slashes ("_").
.PARAMETER Subscription
    [optional] The name of the subscription, where we want to work, eg.: DEV_.
.PARAMETER DeploymentNameFile
    [optional] The path of the file which will store the name of the actual deployment.
.NOTES
#>

[CmdletBinding()]
param (
    [Parameter(Mandatory)]
    [string]
    $ResourceGroupName,
    [Parameter(Mandatory)]
    [string]
    $ProjectName,
    [Parameter(Mandatory)]
    [string]
    $Env,
    [Parameter()]
    [string]
    $date = (Get-Date -Format "dd-MM-yyyy-HH-mm-ss"),
    [Parameter()]
    [string]
    $ParentSpecVersion = "",
    [Parameter()]
    [string]
    $VariablesPrefix = "infra_${ProjectName}_".ToUpper(),
    [Parameter(Mandatory = $false)]
    [string]
    $Subscription = "DEV_",
    [Parameter(Mandatory = $false)]
    [string]
    $DeploymentNameFile = "deployment-output-name.txt"
)
function Get-TemplateSpec {
    param (
        $ResourceGroupName,
        $ProjectName,
        $ParentSpecVersion = ""
    )
    # Get template Spec
    if ($ParentSpecVersion) {
        Write-Host "##[section]Template Spec version to locate is $ParentSpecVersion."
        try {
            $ParentTemplateSpec = (Get-AzTemplateSpec -ResourceGroupName $ResourceGroupName -Name $ProjectName -Version $ParentSpecVersion -ErrorAction Stop -Verbose).Versions
        }
        catch {
            Write-Error "##[error]Template Spec for Project $ProjectName of version $ParentSpecVersion not found in $ResourceGroupName..."
            exit 1
        }
    }
    else {
        Write-Host "##[section]No Template Spec Version set. Searching for a latest Template Spec."
        try {
            $ParentTemplateSpec = $(Get-AzTemplateSpec -ResourceGroupName $ResourceGroupName -Name $ProjectName -Verbose).Versions[-1]
        }
        catch {
            Write-Error "##[error]Template Spec for Project $ProjectName of version $ParentSpecVersion not found in $ResourceGroupName..."
            exit 1
        }
    }
    Write-Host "##[section]Found a template $ProjectName of version $($ParentTemplateSpec.Name)."
    Write-Host "##[command]Template last modified on $($ParentTemplateSpec.LastModifiedTime)."
    return $ParentTemplateSpec
}

function Update-AzureModules {
    Write-Host "##[group]Preparing Az Powershell:"
    Write-Host "Following modules present:"
    Get-Module
    if (-not (Get-Module -Name Az.Resources -ListAvailable)) {
        Write-Host "##[section]Az.Resources Module not found. Installing."
        Install-Module Az.Resources -Force -Scope CurrentUser
        Import-Module -Name Az.Resources
        Write-Host "##[section]Installation completed. Modules loaded:"
        Get-Module
    }
    else {
        Write-Host "##[endgroup]"
        Write-Host "Required modules found. Proceeding further."
    }
}

# Set the right Azure Subscription if multiple Subscriptions existing
# to avoid Get-TemplateSpec related error
Select-AzSubscription -Subscription $Subscription

# Prepare Environment
Update-AzureModules

# Placeholder for Deployment Parameters
$DeploymentParameterObject = @{
    "Name"              = "$ProjectName-$date";
    "ResourceGroupName" = $ResourceGroupName;
}

# Set Environment to Environmental variables
$ENV:Environment = $Env
Write-Host "##[section]Searching for a Template Spec..."
$ParentTemplateSpec = Get-TemplateSpec -ProjectName $ProjectName -ResourceGroupName $ResourceGroupName -ParentSpecVersion $ParentSpecVersion
# Update Deployment Parameters Object - add TemplateSpecId
Write-Host "##[section]Add Template Spec ID $($ParentTemplateSpec.Id) to Deployment parameters."
$DeploymentParameterObject.Add("TemplateSpecId", $ParentTemplateSpec.Id)

# Fetch list of Parameters
Write-Host "##[group]Fetching parameters from template:"
Write-Host "##[section]Found template parameters:"
$TemplateSpecParameters = ($ParentTemplateSpec.MainTemplate | ConvertFrom-Json).parameters
$TemplateSpecParameters.psobject.Properties.Name.ForEach({ Write-Host "##[command]$_" })
[array]$TemplateSpecParameters = $TemplateSpecParameters.psobject.Properties.Name
Write-Host "##[endgroup]"

# Get current ENV variables
$SystemVariables = [ordered] @{}
$SystemVariablesUnfiltered = (Get-Item -Path ENV:).GetEnumerator() | Sort-Object -Property Name

# Avoid 'Item has already been added. Key in dictionary'-related errors
# It can be for example:
# - because both of the 'KEYVAULTNAME' and the 'INFRA_COMMON_KEYVAULTNAME'
#   was set as an environment variable in the pipeline.
foreach ($var in $SystemVariablesUnfiltered) {
    $name = $var.Name
    $value = $var.Value
    # For instance if the 'INFRA_COMMON_KEYVAULTNAME' was set
    $VariablesPrefixAndName = (Get-ChildItem -Path Env:${VariablesPrefix}${name} -ErrorAction SilentlyContinue).Value

    # If a variable exists with VariablesPrefix 'INFRA_COMMON_',
    # then we should add the value of this variable
    if ($VariablesPrefixAndName) {
        $SystemVariables.Add($name, $VariablesPrefixAndName)
        # $SystemVariables.Add(KEYVAULTNAME, INFRA_COMMON_KEYVAULTNAME.value)
    }
    # If not (for example: 'KEYVAULTNAME'),
    # then we can use simply the value of the $name variable
    else {
        $SystemVariables.Add($name, $value)
        # $SystemVariables.Add(KEYVAULTNAME, KEYVAULTNAME.value)
    }
}
# Print out ordered system variables
Write-Host "##[group]Found Environmental Variables:"
$SystemVariables.Keys.GetEnumerator().ForEach({ Write-Host "##[command]Variable: $_" })
Write-Host "##[endgroup]"

# Compare ENV variables to existing Parameters
Write-Host "##[section]Comparing parameters to ENV variables:"
foreach ($item in $TemplateSpecParameters) {

    foreach ($VariablesKey in $SystemVariables.Keys) {
        # Saving Variable's value for further usage
        $VariablesValue = $SystemVariables.$VariablesKey
        # Format the Key Name to search, but no effect would be applied on Keys not matching 
        # the format (evaluated as-is)
        $VariablesKeyFormated = ($VariablesKey.Replace($VariablesPrefix, "")).Replace("_", "")
        if ($item -eq $VariablesKeyFormated ) {
            Write-Host "##[command]Match for $item found in Template Spec Parameters:"
            Write-Host "$item matches to $($VariablesKey) with value $($VariablesValue)."
            # Do not try to add the same key again into the DeploymentParameterObject
            $DeploymentParameterObject.Keys.GetEnumerator() | ForEach-Object {
                if ($item -eq $_) {
                    continue
                }
            }
            $DeploymentParameterObject.Add($item, $SystemVariables.$VariablesKey)
        }
    }
}

# Start Deployment with Parameters
Write-Host "##[section]Starting deployment with the following parameters:"
$DeploymentParameterObject

# Deploying resource
New-AzResourceGroupDeployment @DeploymentParameterObject

# Saving deployment name if a related file passed by the 'DeploymentNameFile' parameter
if ($DeploymentNameFile) {
    $DeploymentParameterObject.Name > deployment-output-name.txt
}
