<#
.SYNOPSIS
    Updates a specific Azure WebApp's Settings.
.DESCRIPTION
    Updates a specific Azure WebApp's Settings.
.PARAMETER Environment
    Optional [string] - Variable sets the environment for applying the configuration.
.PARAMETER AppServiceName
    Optional [string] - Name of the App Service service to apply config values.
.PARAMETER ResourceGroup
    Optional [string] - Name of the Resource Group where the WebApp placed.
.PARAMETER ComponentDirectoryPath
    Optional [string] - The path of the directory, where the component's config files are placed.
.PARAMETER KeyValuesShouldBeReplaced
    Optional [hashtable] - List of configuration keys which values should be replaced during the deployment.
    These things are usually those data, which will we know during the deployment.
.PARAMETER SecretsShouldBeFetched
    Optional [hashtable] - List of secret keys which values should be replaced during the deployment.
    The structure is: "KeyName" = "KeyVaultSecretURL",
    e.g.: "Azure_Client_ID" = "https://{AzureResourceGroupName}.vault.azure.net/secrets/lab1-client-id"
.PARAMETER KeySeparator
    The separator of the JSON keys.: 
    - if it was set to: ":", the keyname was set like that: 'Azure:EventHubs:ConsumerGroupName'.
    - if it was set to: "_", the keyname was set like that: 'Azure_EventHubs_ConsumerGroupName'.
.EXAMPLE
    .\deployment\infra.man.cd.iac\scripts\AppSettingsUpdate.ps1 `
        -Environment lab1 `
        -AppServiceName {name} `
        -ResourceGroup {name} `
        -ComponentDirectoryPath .\appConfig\Gateway `
        -KeyValuesShouldBeReplaced @{ 
            "Azure_KeyShouldBe_Replaced_During_The_Deployment1" = "Infra.COMMON.EventHubNameSpaceName"
            "Azure_KeyShouldBe_Replaced_During_The_Deployment2" = "ReplacedValue2"
        } `
        -SecretsShouldBeFetched @{
            "Serilog_WriteTo_1_Args_AuthenticationId" = "https://{azure-resource-group}.vault.azure.net/lab1-loganalytics-authenticationId"
            "AzureAD_ClientId" = "https://{azure-resource-group}.vault.azure.net/lab1-AzureAD-ClientId"
            "AzureAD_ClientSecret" = "https://{azure-resource-group}.vault.azure.net/lab1-AzureAD-ClientSecret"
        } `
        -KeySeparator _
#>

param(
    [Parameter(Mandatory = $false)] 
    [string]$Environment = "lab1",
    [Parameter(Mandatory = $false)]
    [string]$AppServiceName = "{AzureAppServiceName}",
    [Parameter(Mandatory = $false)] 
    [string]$ResourceGroup = "{AzureResouceGroupName}",
    [Parameter(Mandatory = $false)] 
    [string]$ComponentDirectoryPath = ".\appConfig\Gateway",
    [Parameter(Mandatory = $false)] 
    [Hashtable]$KeyValuesShouldBeReplaced = @{},
    [Parameter(Mandatory = $false)] 
    [Hashtable]$SecretsShouldBeFetched = @{},
    [Parameter(Mandatory = $false)]
    [string]$KeySeparator = "_"
)

#region INIT ----------------------------------------------

# clear the $Error variable
# that tracks errors in a PowerShell session.
$error.clear()

# Displays the error message and stops executing.
# In addition to the error generated, the Stop value generates
# an ActionPreferenceStopException object to the error stream.
# https://tinyurl.com/MsErrorActionPreference
$ErrorActionPreference = "Stop"

#endregion INIT -------------------------------------------

#region FUNCTIONS -----------------------------------------

# Create a hastable from an object.
function ConvertTo-Hashtable {
    [CmdletBinding()] 
    [OutputType('hashtable')]
    
    param (
        [Parameter(valueFromPipeline)]
        $inputObject
    )
    
    process {
        if ($null -eq $inputObject) {
            return $null
        }

        # Check if input is an array or collection.
        if ($inputObject -is [System.Collections.IEnumerable] -and $inputObject -isnot [string]) {
            $collection = @(
                foreach ($object in $inputObject) {
                    ConvertTo-Hashtable -inputObject $object
                }
            )
            # Return the array but do not enumerate it
            Write-Output -NoEnumerate $collection
        }
        elseif ($InputObject -is [psobject]) {
            # Convert it to its own has table and return it
            $hash = @{}
            foreach ($property in $inputObject.psobject.properties) {
                $hash[$property.Name] = ConvertTo-Hashtable -inputObject $property.value
            }
            return $hash
        }
        else {
            # If the object is not an array, collection or other object, it's already a hash table
            # Return object
            return $InputObject
        }
    }
}

# Updates key-values stored in a WebApp.
# Secrets should be set by a keyvault reference,
# so we should signal this by setting the KeyVaultLink field to: true.
#
# Configuration example:
#     {
#         "AppConfig": {
#             "Azure_Ai_InstrumentationKey": {
#                 "Content": "https://{AzureResourceName}.vault.azure.net/secrets/ai-instrumentation-key",
#                 "KeyVaultLink": true
#             },
#
# ... and the the secret-identifier will be:
#     -> @Microsoft.KeyVault(SecretUri=https://{AzureResourceName}.vault.azure.net/secrets/ai-instrumentation-key)
function Update-KeyValuesInWebApp {
    param (
        [System.Collections.DictionaryEntry]
        $AppConfigParameter,
        [string]
        $KeySeparator,
        [string]
        $ResourceGroup,
        [string]
        $AppServiceName
    )

    $key = $AppConfigParameter.Name
    $val = $AppConfigParameter.Value.Content
    $kvl = $AppConfigParameter.Value.KeyVaultLink

    Write-Host "##[section] Update-KeyValuesInWebApp"
    Write-Host "##[command] - Set Key => $key"
    Write-Host "##[command] - KeyVaultLink: $kvl"

    # If the KeyVaultLink: true
    # -> we have to provide a string which contains the referrence link to the secret in the KeyVault
    # -> @Microsoft.KeyVault(SecretUri=https://{AzureResouceName}.vault.azure.net/secrets/StorageConnectionString)
    $secretUri = ""

    if ($kvl) {
        # There is a problem with how Python is interpreting the parentheses and/or quotes.
        # That's why we need to escape the parentheses with: '^^^'.
        $secretUri = "@Microsoft.KeyVault^^^(SecretUri=${val}^^^)"
    }

    if ($secretUri) {
        $val = $secretUri
    }

    Write-Host "##[command] - Command: 'az webapp config appsettings set -g ${ResourceGroup} -n ${AppServiceName} --settings ${key}=${val}'"
    $out = az webapp config appsettings set `
        -g "${ResourceGroup}" `
        -n "${AppServiceName}" `
        --settings "${key}=${val}" 2>&1

    if (!$?) {
        Write-Host "##[error] Az CLI encountered and error trying to set '${key}' for '${AppServiceName}'."
        Write-Host "##[error] ${out}"
        exit 1
    }
}

# Replace the values of configuration fields.
# These values are usually those data, which will we know only during the deployment.
# We should pass the list of key/values by the 'KeyValuesShouldBeReplaced' parameter, 
# and a hashtable by the 'AppConfigParametersHash' parameter as well,
# in which we want to replace them.
# For example:
# $KeyValuesShouldBeReplaced = @{ 
#     "Azure_KeyShouldBe_Replaced:During_The_Deployment1" = "ReplacedValue1"
#     "Azure_KeyShouldBe_Replaced:During_The_Deployment2" = "ReplacedValue2"
# }
function Set-KeyValues {
    param (
        [Hashtable]
        $AppConfigParametersHash,
        [Hashtable]
        $KeyValuesShouldBeReplaced
    )

    Write-Host "##[section] Set-KeyValues"
    $appConfigParameters = @{}

    if ($AppConfigParametersHash.Count -ne 0) {
        foreach ($e in $KeyValuesShouldBeReplaced.GetEnumerator()) {
            $key = $e.Name
            $val = $e.Value
            
            if ($AppConfigParametersHash."$key".Content) {
                Write-Host "##[command] - Updating: $key"
                $AppConfigParametersHash."$key".Content = "$val"
            } 
            else {
                Write-Host "##[warning] - Cannot find field: 'Content' for key: $key"
            }
        }
        $appConfigParameters += $AppConfigParametersHash
    }

    $appConfigParameters
}

# Replace the values of configuration fields.
#   -> The structure is: "KeyName" = "KeyVaultSecretURL",
#   -> e.g.: "Azure_Client_ID" = "https://{AzureResouceName}.vault.azure.net/secrets/lab1-client-id"
# The 'AppConfigParametersHash' parameter should have all the needed configurations.
# We should pass the list of key/values by the 'SecretsShouldBeFetched' parameter
# which should be replaced in the 'AppConfigParametersHash'.
# For example:
# -AppConfigParametersHash @{
#     "KeyVault_SecretName_Which_ShouldBe_Fetched_During_The_Deployment1" = "CHANGE_ME"
#     "KeyVault_SecretName_Which_ShouldBe_Fetched_During_The_Deployment2" = "CHANGE_ME"
#     "KeyVault_SecretName_Which_ShouldBe_Fetched_During_The_Deployment3" = "CHANGE_ME"
# }
# For example:
# -SecretsShouldBeFetched @{
#     "KeyVault_SecretName_Which_ShouldBe_Fetched_During_The_Deployment1" = "https://{AzureResouceName}.vault.azure.net/secrets/lab1-client-id"
#     "KeyVault_SecretName_Which_ShouldBe_Fetched_During_The_Deployment2" = "https://{AzureResouceName}.vault.azure.net/secrets/lab1-eh-ods-connection-string"
#     "KeyVault_SecretName_Which_ShouldBe_Fetched_During_The_Deployment3" = "https://{AzureResouceName}.vault.azure.net/secrets/lab1-fr24-historical-data-password"
# }
function Update-Secrets {
    param (
        [Hashtable]
        $AppConfigParametersHash,
        [Hashtable]
        $SecretsShouldBeFetched
    )

    Write-Host "##[section] Update-Secrets"
    $appConfigParameters = @{}

    if ($AppConfigParametersHash.Count -ne 0) {
        foreach ($e in $SecretsShouldBeFetched.GetEnumerator()) {
            $key = $e.Name
            $kv = $e.Value

            if ($AppConfigParametersHash."$key".Content) {
                Write-Host "##[command] - Updating: $key"
                $AppConfigParametersHash."$key".KeyVaultLink = $true
                $AppConfigParametersHash."$key".Content = "$kv"
            }
            else {
                Write-Host "##[warning] - Cannot find key: 'Content' for secret key: $key"
            }
        }
        $appConfigParameters += $AppConfigParametersHash
    }

    $appConfigParameters
}

# Returns with a hashtable of the core configurations
# which stored under the ComponentDirectoryPath directly,
# For example:
# appConfig
# └─── coreConfig.json
function Get-CoreAppConfigParameters {
    param (
        [string]
        $ComponentDirectoryPath
    )

    Write-Host "##[section] New-CoreHashTable"
    $appConfigParametersCoreHash = @{}

    # Because we passing the path of the component, 
    # and the "core" configurations are placed right in the folder where the component folders are placed
    # we need to have this "root folder", to define the core configuration files.
    # For example: 
    # - if thw $ComponentDirectoryPath = .\appConfig\Gateway
    # - then the $rootFolder = appConfig
    $rootFolder = (($ComponentDirectoryPath | Convert-Path) -replace [Regex]::Escape($(Get-Location)), "").Split("\")[1]

    Write-Host "##[command] - Root folder: $rootFolder"
    $fileCore = Get-ChildItem -Path $rootFolder | Where-Object { $_.Extension -like ".json" }

    ## Check if files with core parameters exists
    if ($null -ne $fileCore) {
        Write-Host "##[command] - $($fileCore.FullName)"

        $fileCoreContent = $fileCore | Get-Content 
        $appConfigParametersCoreJson = $fileCoreContent | ConvertFrom-Json 
        $appConfigParametersCoreHash = $appConfigParametersCoreJson.AppConfig | ConvertTo-Hashtable
    }
    else {
        Write-Host "##[command] - There is nothing under the root folder."
    }
    
    Write-Host "##[command] - Core hash is created"
    $appConfigParametersCoreHash
}

# Returns with the environment-related list of files for a component.
# If the ComponentDirectory = 'appConfig\Gateway'
# &  the Environment = 'lab1':
# appConfig
# ├─── Gateway
# │    └─── environment.lab1
# │         └─── Gateway.json
function Get-ConfigFilesFromComponentByEnvironment {
    param (
        [string]
        $ComponentDirectory,
        [string]
        $Environment
    )

    Write-Host "##[section] Get-ConfigFilesFromComponentByEnvironment"
    $files = Get-ChildItem -Path $ComponentDirectory -Recurse | Where-Object { $_.Extension -like ".json" }
    $files = $files | Where-Object { $_.Directory -match $( -join ("environment.", $Environment)) }
    
    Write-Host "##[command] - Files: $($files -join ", ")"
    $files
}

# Returns with the component-based AppConfig hashtable.
function Get-ComponentAppConfigParameters {
    param (
        [System.Object[]]
        $FilesComponentsFiltered
    )

    Write-Host "##[section] Get-ComponentAppConfigParameters"

    $componentAppConfigParameters = @{}
  
    if ($null -ne $FilesComponentsFiltered) {
        foreach ($fileComponent in $FilesComponentsFiltered) {
            $json = $fileComponent | Get-Content
            $obj = $json | ConvertFrom-Json
            $hash = $obj.AppConfig | ConvertTo-Hashtable
           
            if ($hash.Count -ne 0) {
                $componentAppConfigParameters += $hash
            }
        }
    }

    Write-Host "##[command] - ComponentAppConfigParameters: $($componentAppConfigParameters | ConvertTo-Json -Compress)"

    $componentAppConfigParameters
}

# Replace the '{Environment}' string 
# to the environment name passed by the 'Environment' parameter.
function Update-HashByEnvironment {
    param (
        [Hashtable]
        $AppConfigParameters,
        [string]
        $Environment
    )

    Write-Host "##[section] Update-HashByEnvironment"

    if ($AppConfigParameters) {
        $update = $AppConfigParameters.GetEnumerator() | Where-Object { $_.Value.Content -match "{Environment}" }
        $update | ForEach-Object { $AppConfigParameters[$_.Key].content = $AppConfigParameters[$_.Key].Content -replace ('{Environment}', $Environment) }
    }

    Write-Host "##[command] - AppConfigParameters: $($AppConfigParameters | ConvertTo-Json -Compress)" 
    $AppConfigParameters
}

# Iterates over 'AppConfigParameters'
# and updates each of them by the 'Update-KeyValuesInWebApp' function.
function Update-APPConfig {
    param (
        [Hashtable]
        $AppConfigParameters,
        [string]
        $KeySeparator,
        [string]
        $ResourceGroup,
        [string]
        $AppServiceName
    )

    Write-Host "##[section] Update-APPConfig - Started"

    if ($AppConfigParameters.Count -ne 0) {
        $AppConfigParameters.GetEnumerator().foreach(
            { 
                Update-KeyValuesInWebApp `
                    -AppConfigParameter $_ `
                    -KeySeparator $KeySeparator `
                    -ResourceGroup $ResourceGroup `
                    -AppServiceName $AppServiceName
            }
        )
    }
 
    Write-Host "##[section] Update-APPConfig - Finished"
}

# The main function.
# Puts all things together.
function Invoke-Main {
    Write-Host "##[section] Starting Update of Azure App Configuration KeyValues"

    # Create a hashtable from the core configuration.
    $coreAppConfigParameters = Get-CoreAppConfigParameters `
        -ComponentDirectoryPath $ComponentDirectoryPath

    # Create an environment-related list of files of all components.
    $filesComponentsFiltered = Get-ConfigFilesFromComponentByEnvironment `
        -ComponentDirectory $ComponentDirectoryPath `
        -Environment $Environment

    # Create a hashtable from the component-specific configuration, filtered by environment.
    $componentAppConfigParameters = Get-ComponentAppConfigParameters `
        -FilesComponentsFiltered $filesComponentsFiltered

    # Merging core and component-specific hashtables.
    $appConfigParameters = $coreAppConfigParameters + $componentAppConfigParameters

    # Replace the values of configuration fields
    # passed by the 'KeyValuesShouldBeReplaced' parameter
    $appConfigParameters = Set-KeyValues `
        -AppConfigParametersHash $appConfigParameters `
        -KeyValuesShouldBeReplaced $KeyValuesShouldBeReplaced

    # Replace the values of configuration fields
    # passed by the 'SecretsShouldBeFetched' parameter
    $appConfigParameters = Update-Secrets `
        -AppConfigParametersHash $appConfigParameters `
        -SecretsShouldBeFetched $SecretsShouldBeFetched

    # Replace the '{Environment}' string in the hashtable
    # to the environment name passed by the 'Environment' parameter.
    $appConfigParameters = Update-HashByEnvironment `
        -AppConfigParameters $appConfigParameters `
        -Environment $Environment

    # Upload / update all key/values in the Azure AppConfig.
    Update-APPConfig `
        -AppConfigParameters $appConfigParameters `
        -ResourceGroup $ResourceGroup `
        -AppServiceName $AppServiceName `
        -KeySeparator $KeySeparator

    Write-Host "##[section] Finished Update of Azure App Configuration KeyValues"
}

#endregion FUNCTIONS --------------------------------------

# Executing main.
Invoke-Main
