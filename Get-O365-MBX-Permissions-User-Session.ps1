param (

    [string]$valuePath,
    [string]$Type,
    [int]$ArrayCount,
    [string]$DataPath

)

Write-Host $valuePath
Write-Host $Type
Write-Host $ArrayCount
Write-Host $DataPath

$StartDTM = (get-date)

Write-Host $StartDTM -ForegroundColor Green
$mailboxes = Import-Csv -Path $valuePath -Delimiter "|"

$MailBoxesPermissionsPath = $DataPath + "\" + "MAILBOXES_PERMISSIONS_$(($type).ToUpper())_" + $((get-date).ToString("yyyy-MM-dd")) + "-$ArrayCount-TEMP.csv"

# Define Credentials
[string]$userName = 'svc-reporting-365@domain.com'
[string]$passwordText = Get-Content 'D:\Scripts-V2\O365_Reports\_GENERAL\_SUPPORT\Secure.txt'

# Convert to secure string
[SecureString]$securePwd = $passwordText  | ConvertTo-SecureString 

# Create credential object
[PSCredential]$cred = New-Object System.Management.Automation.PSCredential -ArgumentList $userName, $securePwd

Connect-ExchangeOnline -Credential $cred -PageSize 5000

function Foreach-ObjectFast {
    param
    (
        [ScriptBlock]
        $Process,
    
        [ScriptBlock]
        $Begin,
    
        [ScriptBlock]
        $End
    )
  
    begin {
        # construct a hard-coded anonymous simple function from
        # the submitted scriptblocks:
        $code = @"
& {
  begin
  {
    $Begin
  }
  process
  {
    $Process
  }
  end
  {
    $End
  }
}
"@
        # turn code into a scriptblock and invoke it
        # via a steppable pipeline so we can feed in data
        # as it comes in via the pipeline:
        $pip = [ScriptBlock]::Create($code).GetSteppablePipeline()
        $pip.Begin($true)
    }
    process {
        # forward incoming pipeline data to the custom scriptblock:
        $pip.Process($_)
    }
    end {
        $pip.End()
    }
}

$scriptblock = {

    param($mailbox)

    $UserPrincipalName = $mailbox.UserPrincipalName
    $PrimarySmtpAddress = $mailbox.PrimarySmtpAddress
    $UsageLocation = $mailbox.UsageLocation
    
    $FullAccessPermissions = (Get-EXOMailboxPermission -Identity $UserPrincipalName | `
            Where-Object { ($_.AccessRights -contains "FullAccess") `
                -and ($_.IsInherited -eq $False) `
                -and -not ($_.User -match "NT AUTHORITY" -or $_.User -match "S-1-5-21" -or $_.User -match "NAMPR" -or $_.User -match "EURPR") }).User

    if ([string]$FullAccessPermissions -ne "") {
        
        $UserWithAccess = ""
        $AccessType = "FullAccess"
                        
        foreach ($FullAccessPermission in $FullAccessPermissions) {
            $UserWithAccess = $UserWithAccess + $FullAccessPermission
            if ($FullAccessPermissions.indexof($FullAccessPermission) -lt (($FullAccessPermissions.Count) - 1)) {
                $UserWithAccess = $UserWithAccess + ","
            }
        }
        $FA = @{"Identity" = $mailbox.DisplayName; "I_PrimarySmtpAddress" = $PrimarySmtpAddress; "I_UsageLocation" = $UsageLocation; "MailBoxType" = $MBType; "AccessRights" = $AccessType; "Trustee" = $UserWithAccess }
        $FA_Result = New-Object psobject -Property $FA 
        $FA_Result | Select-Object Identity, I_PrimarySmtpAddress, I_UsageLocation, Trustee, AccessRights 
        
        $FA = $null;
        $FA_Result = $null;
        [System.GC]::Collect();
    }

    #Getting SendAs permissions for mailbox
    $SendAsPermissions = (Get-EXORecipientPermission -Identity $UserPrincipalName | `
            Where-Object { `
                -not ($_.Trustee -match "NT AUTHORITY" -or $_.Trustee -match "S-1-5-21" -or $_.Trustee -match "NAMPR" -or $_.User -match "EURPR") }).Trustee

    if ([string]$SendAsPermissions -ne "") {
    
        $UserWithAccess = ""
        $AccessType = "SendAs"
    
        foreach ($SendAsPermission in $SendAsPermissions) {
            $UserWithAccess = $UserWithAccess + $SendAsPermission
            if ($SendAsPermissions.indexof($SendAsPermission) -lt (($SendAsPermissions.Count) - 1)) {
                $UserWithAccess = $UserWithAccess + ","
            }
        }
        $SA = @{"Identity" = $mailbox.DisplayName; "I_PrimarySmtpAddress" = $PrimarySmtpAddress; "I_UsageLocation" = $UsageLocation; "MailBoxType" = $MBType; "AccessRights" = $AccessType; "Trustee" = $UserWithAccess }
        $SA_Result = New-Object psobject -Property $SA 
        $SA_Result | Select-Object Identity, I_PrimarySmtpAddress, I_UsageLocation, Trustee, AccessRights 
    }
}           

$InitialSessionState = [system.management.automation.runspaces.initialsessionstate]::CreateDefault()
$RunspacePool = [runspacefactory]::CreateRunspacePool(1, 5, $InitialSessionState, $Host)
$RunspacePool.Open()
$Jobs = New-Object System.Collections.Generic.List[System.Object]

$mailboxes | Foreach-ObjectFast -Process {

    $PowershellThread = [PowerShell]::Create().AddScript($ScriptBlock).AddArgument($_)

    $PowershellThread.RunspacePool = $RunspacePool
    $Handle = $PowershellThread.BeginInvoke()
    $Job = "" | Select-Object Handle, Thread, object
    $Job.Handle = $Handle
    $Job.Thread = $PowershellThread
    $Job.Object = $m.UserPrincipalName
    $Jobs.Add($Job)
    [System.GC]::Collect();

}

$running = $true
While ($running) {
    $Running = $false
    $Jobs | ForEach-object { if ($_.Handle.IsCompleted -eq $True) {
            $_.Thread.EndInvoke($_.Handle) | Export-Csv -Path "$MailBoxesPermissionsPath" -Append -NoTypeInformation -Delimiter "|"
            $_.Thread.Dispose()
            $_.Thread = $Null
            $_.Handle = $Null
            [System.GC]::Collect();
        }
        elseif ((-not $Running) -and ($_.Handle -ne $Null)) {
            $Running = $true
        }
    }
}
$null = $RunspacePool.Close()
$null = $RunspacePool.Dispose()

Get-PSSession | Remove-PSSession

$EndtDTM = (get-date)
Write-Host $EndtDTM -ForegroundColor Green
Write-Host "Minutes taken: $([math]::Round(($EndtDTM - $StartDTM).TotalMinutes,2))" -ForegroundColor Green