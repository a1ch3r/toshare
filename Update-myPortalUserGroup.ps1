[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
Import-Module "Microsoft.Graph.Intune"

## Auth settings
$global:clientId = "***"
$global:tenantId = "***"
$global:clientSecret = "***"
$global:graphUrl = "https://graph.microsoft.com"

## Group
$userGroup = "AG-myPortalDependencies-XL"
$LogFile = "C:\_LOGS\AG-myPortalDependencies-XL-logs-$((get-date).ToString("yyyy-MM-dd")).txt"

## Mail Settings	
$MailFrom = 'office365_noreply@domain.com'
$MailSubject = 'Intune applications installation'
$MailServer = 'relay.domain.com'
$MessageBody = Get-Content -Path "C:\Notification.html" -Raw

Function LogMessage {

    param([string]$Message, [string]$LogFile)

    ((get-date).ToString() + " - " + $Message) | Out-File -FilePath $LogFile -Append
}

Function WriteLog {

    param([string]$Message)

    LogMessage -Message $Message -LogFile $LogFile

}


####################################################

try {

    Update-MSGraphEnvironment -AppId $global:clientId -Quiet
    Update-MSGraphEnvironment -AuthUrl "https://login.windows.net/domain.onmicrosoft.com"
    
    WriteLog -Message "MSGraphEnvironment has been updated"

}
catch {
}

try {

    Connect-MSGraph -ClientSecret $global:clientSecret -Quiet
    WriteLog -Message "Connection to MSGraph has been opened"

}
catch {
    break;
}
####################################################

Function IntuneMobileApps {
    
    try {

        Get-IntuneMobileApp | Get-MSGraphAllPages | Where-Object { $_.displayName -match "myportal" }
        WriteLog -Message "Intune applications has been collected"

    }
    catch {
        
        break;
    }

}

Function IntuneDevicesApplications ($_apps) {

    $Devices = @()

    foreach ($app in $Apps) {

        Start-Sleep 10
        $appId = $app.id
        $uri = "$global:graphUrl/beta/deviceAppManagement/mobileApps/$appId/deviceStatuses"
    
        $DetectedDevices = (Invoke-MSGraphRequest -Url $uri -HttpMethod Get).value #| select deviceName, deviceId, installState, osDescription, userName, userPrincipalName

        foreach ($DetectedDevice  in $DetectedDevices) {

            if ($DetectedDevice.installState -eq "installed") {
                $Devices += [PSCustomObject]@{
                    DeviceName                  = $($DetectedDevice.deviceName)
                    DeviceID                    = $($DetectedDevice.deviceId)
                    OS                          = $($DetectedDevice.osDescription)
                    InstallState                = $($DetectedDevice.installState)
                    MobileAppInstallStatusValue = $($DetectedDevice.mobileAppInstallStatusValue)
                    UserName                    = $($DetectedDevice.UserName)
                    UserPrincipalName           = $($DetectedDevice.UserPrincipalName)
                    AppId                       = $($app.id)
                    AppName                     = $($app.displayName)
                }         
            }
        }
    }
    return $Devices
}

Function Get-UserGroups {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory = $true, ValueFromPipeline = $true)]
        [alias("SamAccountName", "Name")]
        [String]$user,
        [switch]$sam,
        [switch]$adsiInterface = $false
    ) # param
 
    Begin { 
        Try {
            Import-Module ActiveDirectory -ErrorAction Stop
        }
        Catch {
            $adsiInterface = $true
        }
    }# Begin
 
    Process {
        If ($adsiInterface) {

            $objDomain = New-Object System.DirectoryServices.DirectoryEntry
            $domainDN = $objDomain.distinguishedName
            $searchRoot = "LDAP://$domainDN"
           
            $root = [ADSI]''			
            $Searcher = New-Object DirectoryServices.DirectorySearcher($root)
            $Searcher.Filter = "(&(objectCategory=person)(userprincipalname=$user))"
            $retObject = $Searcher.FindAll()
           
            $userObj = [ADSI]$retObject.Path
            $Groups = $userObj.MemberOf | ForEach-Object { [ADSI]"LDAP://$_" }
        }
        Else {
            #Get user Data
            Try {
                $Groups = (Get-ADUser $user -properties MemberOf -ErrorAction Stop).MemberOf | Get-ADGroup
            }
            Catch {
                Write-Warning "$user not found`n"
                $error.Clear()
                Break
            }
        }
    }# Process
 
    End {
        if ($Groups) {
            return $Groups.Name
        }
    } # End
}# function

$apps = IntuneMobileApps
$devices = IntuneDevicesApplications -_apps $apps

if ($Devices.Count -gt 0) {
    
    $Devices = $Devices | Select-Object UserName, UserPrincipalName, DeviceName, DeviceID, OS, AppId, AppName, InstallState 
    $Devices = $Devices | where {($_.OS -match "iOS") -or ($_.OS -match "Android")}
    $Devices.count
    WriteLog -Message "Devices: Number of devices with myportal application - $($Devices.count)"
    $UsersInTheGroup = Get-ADGroupMember -Identity $userGroup
    WriteLog -Message "Number of users in the group before - $($UsersInTheGroup.count)"
    
    Foreach ($user in $Devices) {

        $UserGroups = Get-UserGroups -user $user.UserPrincipalName  -adsiInterface

        if (($UserGroups.Count -gt 0) -and ($UserGroups.IndexOf($userGroup)) -eq "-1") {

            try {
                $UID = $null;
                $UID = Get-ADUser -Filter "UserPrincipalName -eq '$($user.UserPrincipalName)'" | where {$_.DistinguishedName -match "DC=Global,DC=domain,DC=com"}
                Write-Host "$($UID.SamAccountName)"
                Add-ADGroupMember -Identity $userGroup -Members $($UID.SamAccountName)
                WriteLog -Message "User: $($user.UserPrincipalName) has been added to the group $userGroup"

                
                Send-MailMessage `
                    -From $MailFrom `
                    -To $($user.UserPrincipalName) `
                    -Subject $MailSubject `
                    -Body $MessageBody `
                    -BodyAsHtml `
                    -DeliveryNotificationOption onFailure `
                    -SmtpServer $MailServer

                WriteLog -Message "Email Has been sent to $($user.UserPrincipalName)"


            }
            catch {
                WriteLog -Message "User: $($user.UserPrincipalName) has not been added to the group $userGroup"
                WriteLog -Message "Error: $($Error[0])"


            }

        }

    }
        WriteLog -Message "Number of users in the group after - $($UsersInTheGroup.count)"

}