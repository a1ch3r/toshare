param (
    [string]$azure_sec_new_group_name,  
    [string]$azure_sec_new_owner,
    [string]$azure_sec_new_description,
    [string[]]$azure_sec_group_add_members,
    [string[]]$azure_sec_group_remove_members,
    [string]$azure_sec_exist_group_name,
    [ValidateSet("Create","Modify","Delete")][string]$azure_sec_group_what_to_do
)

###-------------------------------------###

function Provide-Credentials() { #Function to get credentials from the credential manager
    Param(
        $CredManagerTarget
    )
                ###Some Variables

                ###A bit of code
$PsCredmanUtils = @"
                using System;
                using System.Runtime.InteropServices;

                namespace PsUtils
                {
                               public class CredMan
                               {
                                               #region Imports
                                               [DllImport("Advapi32.dll", SetLastError = true, EntryPoint = "CredFree")]
                                               private static extern void CredFree([In] IntPtr cred);

                                               [DllImport("Advapi32.dll", SetLastError = true, EntryPoint = "CredReadW", CharSet = CharSet.Unicode)]
                                               private static extern bool CredReadW([In] string target, [In] CRED_TYPE type, [In] int reservedFlag, out IntPtr CredentialPtr);
                                               #endregion

                                               #region Fields
                                               public enum CRED_FLAGS : uint
                                               {
                                                               NONE = 0x0,
                                                               PROMPT_NOW = 0x2,
                                                               USERNAME_TARGET = 0x4
                                               }

                                               public enum CRED_ERRORS : uint
                                               {
                                                               ERROR_SUCCESS = 0x0,
                                                               ERROR_INVALID_PARAMETER = 0x80070057,
                                                               ERROR_INVALID_FLAGS = 0x800703EC,
                                                               ERROR_NOT_FOUND = 0x80070490,
                                                               ERROR_NO_SUCH_LOGON_SESSION = 0x80070520,
                                                               ERROR_BAD_USERNAME = 0x8007089A
                                               }

                                               public enum CRED_PERSIST : uint
                                               {
                                                               SESSION = 1,
                                                               LOCAL_MACHINE = 2,
                                                               ENTERPRISE = 3
                                               }

                                               public enum CRED_TYPE : uint
                                               {
                                                               GENERIC = 1,
                                                               DOMAIN_PASSWORD = 2,
                                                               DOMAIN_CERTIFICATE = 3,
                                                               DOMAIN_VISIBLE_PASSWORD = 4,
                                                               GENERIC_CERTIFICATE = 5,
                                                               DOMAIN_EXTENDED = 6,
                                                               MAXIMUM = 7,      // Maximum supported cred type
                                                               MAXIMUM_EX = (MAXIMUM + 1000),  // Allow new applications to run on old OSes
                                               }

                                               [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
                                               public struct Credential
                                               {
                                                               public CRED_FLAGS Flags;
                                                               public CRED_TYPE Type;
                                                               public string TargetName;
                                                               public string Comment;
                                                               public DateTime LastWritten;
                                                               public UInt32 CredentialBlobSize;
                                                               public string CredentialBlob;
                                                               public CRED_PERSIST Persist;
                                                               public UInt32 AttributeCount;
                                                               public IntPtr Attributes;
                                                               public string TargetAlias;
                                                               public string UserName;
                                               }

                                               [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
                                               private struct NativeCredential
                                               {
                                                               public CRED_FLAGS Flags;
                                                               public CRED_TYPE Type;
                                                               public IntPtr TargetName;
                                                               public IntPtr Comment;
                                                               public System.Runtime.InteropServices.ComTypes.FILETIME LastWritten;
                                                               public UInt32 CredentialBlobSize;
                                                               public IntPtr CredentialBlob;
                                                               public UInt32 Persist;
                                                               public UInt32 AttributeCount;
                                                               public IntPtr Attributes;
                                                               public IntPtr TargetAlias;
                                                               public IntPtr UserName;
                                               }
                                               #endregion

                                               #region Child Class
                                               private class CriticalCredentialHandle : Microsoft.Win32.SafeHandles.CriticalHandleZeroOrMinusOneIsInvalid
                                               {
                                                               public CriticalCredentialHandle(IntPtr preexistingHandle)
                                                               {
                                                                              SetHandle(preexistingHandle);
                                                               }

                                                               private Credential XlateNativeCred(IntPtr pCred)
                                                               {
                                                                               NativeCredential ncred = (NativeCredential)Marshal.PtrToStructure(pCred, typeof(NativeCredential));
                                                                               Credential cred = new Credential();
                                                                               cred.Type = ncred.Type;
                                                                               cred.Flags = ncred.Flags;
                                                                               cred.Persist = (CRED_PERSIST)ncred.Persist;

                                                                               long LastWritten = ncred.LastWritten.dwHighDateTime;
                                                                               LastWritten = (LastWritten << 32) + ncred.LastWritten.dwLowDateTime;
                                                                               cred.LastWritten = DateTime.FromFileTime(LastWritten);

                                                                               cred.UserName = Marshal.PtrToStringUni(ncred.UserName);
                                                                               cred.TargetName = Marshal.PtrToStringUni(ncred.TargetName);
                                                                               cred.TargetAlias = Marshal.PtrToStringUni(ncred.TargetAlias);
                                                                               cred.Comment = Marshal.PtrToStringUni(ncred.Comment);
                                                                               cred.CredentialBlobSize = ncred.CredentialBlobSize;
                                                                               if (0 < ncred.CredentialBlobSize)
                                                                               {
                                                                                              cred.CredentialBlob = Marshal.PtrToStringUni(ncred.CredentialBlob, (int)ncred.CredentialBlobSize / 2);
                                                                               }
                                                                               return cred;
                                                               }

                                                               public Credential GetCredential()
                                                               {
                                                                               if (IsInvalid)
                                                                               {
                                                                                              throw new InvalidOperationException("Invalid CriticalHandle!");
                                                                               }
                                                                               Credential cred = XlateNativeCred(handle);
                                                                               return cred;
                                                               }

                                                               public Credential[] GetCredentials(int count)
                                                               {
                                                                               if (IsInvalid)
                                                                               {
                                                                                              throw new InvalidOperationException("Invalid CriticalHandle!");
                                                                               }
                                                                               Credential[] Credentials = new Credential[count];
                                                                               IntPtr pTemp = IntPtr.Zero;
                                                                               for (int inx = 0; inx < count; inx++)
                                                                               {
                                                                                              pTemp = Marshal.ReadIntPtr(handle, inx * IntPtr.Size);
                                                                                              Credential cred = XlateNativeCred(pTemp);
                                                                                              Credentials[inx] = cred;
                                                                               }
                                                                               return Credentials;
                                                               }

                                                               override protected bool ReleaseHandle()
                                                               {
                                                                               if (IsInvalid)
                                                                               {
                                                                                              return false;
                                                                               }
                                                                               CredFree(handle);
                                                                               SetHandleAsInvalid();
                                                                               return true;
                                                               }
                                               }
                                               #endregion


                                               public static int CredRead(string target, CRED_TYPE type, out Credential Credential)

                                               {
                                                               IntPtr pCredential = IntPtr.Zero;
                                                               Credential = new Credential();
                                                               if (!CredReadW(target, type, 0, out pCredential))
                                                               {
                                                                               return Marshal.GetHRForLastWin32Error();

                                                               }
                                                               CriticalCredentialHandle CredHandle = new CriticalCredentialHandle(pCredential);
                                                               Credential = CredHandle.GetCredential();
                                                               return 0;

                                               }



                                               private static bool CheckError(string TestName, CRED_ERRORS Rtn)
                                               {
                                                               switch(Rtn)
                                                               {
                                                                               case CRED_ERRORS.ERROR_SUCCESS:
                                                                                              Console.WriteLine(string.Format("'{0}' worked", TestName));
                                                                                              return true;
                                                                               case CRED_ERRORS.ERROR_INVALID_FLAGS:
                                                                               case CRED_ERRORS.ERROR_INVALID_PARAMETER:
                                                                               case CRED_ERRORS.ERROR_NO_SUCH_LOGON_SESSION:
                                                                               case CRED_ERRORS.ERROR_NOT_FOUND:
                                                                               case CRED_ERRORS.ERROR_BAD_USERNAME:
                                                                                               Console.WriteLine(string.Format("'{0}' failed; {1}.", TestName, Rtn));
                                                                                              break;
                                                                               default:
                                                                                              Console.WriteLine(string.Format("'{0}' failed; 0x{1}.", TestName, Rtn.ToString("X")));
                                                                                              break;
                                                               }
                                                               return false;
                                               }
                               }
                }
"@
                $PsCredMan = $null
                try {
                               $PsCredMan = [PsUtils.CredMan]
                }
                catch {
                               #only remove the error we generate
                               $Error.RemoveAt($Error.Count-1)
                }
                if($null -eq $PsCredMan){
                               Add-Type $PsCredmanUtils
                }
                #endregion
                #Region Internal Tools
                [HashTable] $ErrorCategory = @{
                               0x80070057 = "InvalidArgument";
                               0x800703EC = "InvalidData";
                               0x80070490 = "ObjectNotFound";
                               0x80070520 = "SecurityError";
                               0x8007089A = "SecurityError"
                }
                function GetCredentialType{
                               Param(
                                               [Parameter(Mandatory=$true)][ValidateSet(
                                                               "GENERIC",
                                                               "DOMAIN_PASSWORD",
                                                               "DOMAIN_CERTIFICATE",
                                                               "DOMAIN_VISIBLE_PASSWORD",
                                                               "GENERIC_CERTIFICATE",
                                                               "DOMAIN_EXTENDED",
                                                               "MAXIMUM",
                                                               "MAXIMUM_EX")][String] $CredType
                               )
                               switch($CredType){
                                               "GENERIC" {return [PsUtils.CredMan+CRED_TYPE]::GENERIC}
                                               "DOMAIN_PASSWORD" {return [PsUtils.CredMan+CRED_TYPE]::DOMAIN_PASSWORD}
                                               "DOMAIN_CERTIFICATE" {return [PsUtils.CredMan+CRED_TYPE]::DOMAIN_CERTIFICATE}
                                               "DOMAIN_VISIBLE_PASSWORD" {return [PsUtils.CredMan+CRED_TYPE]::DOMAIN_VISIBLE_PASSWORD}
                                               "GENERIC_CERTIFICATE" {return [PsUtils.CredMan+CRED_TYPE]::GENERIC_CERTIFICATE}
                                               "DOMAIN_EXTENDED" {return [PsUtils.CredMan+CRED_TYPE]::DOMAIN_EXTENDED}
                                               "MAXIMUM" {return [PsUtils.CredMan+CRED_TYPE]::MAXIMUM}
                                               "MAXIMUM_EX" {return [PsUtils.CredMan+CRED_TYPE]::MAXIMUM_EX}
                               }
                }
                #EndRegion
                #Region Password Reader
                function ReadCredentials{
                               Param(
                                               [Parameter(Mandatory=$true)][ValidateLength(1,32767)][String] $Target,
                                               [Parameter(Mandatory=$false)][ValidateSet(
                                                               "GENERIC",
                                                               "DOMAIN_PASSWORD",
                                                               "DOMAIN_CERTIFICATE",
                                                               "DOMAIN_VISIBLE_PASSWORD",
                                                               "GENERIC_CERTIFICATE",
                                                               "DOMAIN_EXTENDED",
                                                               "MAXIMUM",
                                                               "MAXIMUM_EX")][String] $CredType = "GENERIC"
                               )
                               if("GENERIC" -ne $CredType -and 337 -lt $Target.Length) {#CRED_MAX_DOMAIN_TARGET_NAME_LENGTH
                                               [String] $Msg = "Target field is longer ($($Target.Length)) than allowed (max 337 characters)"
                                               [Management.ManagementException] $MgmtException = New-Object Management.ManagementException($Msg)
                                               [Management.Automation.ErrorRecord] $ErrRcd = New-Object Management.Automation.ErrorRecord($MgmtException, 666, 'LimitsExceeded', $null)
                                               return $ErrRcd
                               }
                               [PsUtils.CredMan+Credential] $Cred = New-Object PsUtils.CredMan+Credential
                               [Int] $Results = 0
                               try          {
                                               $Results = [PsUtils.CredMan]::CredRead($Target, $(GetCredentialType $CredType), [Ref]$Cred)
                               }
                               catch {
                                               return $_
                               }
                               
                               switch($Results){
                                               0 {break}
                                               0x80070490 {return $null} #ERROR_NOT_FOUND
                                               default{
                                                               [String] $Msg = "Error reading credentials for target '$Target' from '$Env:UserName' credentials store"
                                                               [Management.ManagementException] $MgmtException = New-Object Management.ManagementException($Msg)
                                                               [Management.Automation.ErrorRecord] $ErrRcd = New-Object Management.Automation.ErrorRecord($MgmtException, $Results.ToString("X"), $ErrorCategory[$Results], $null)
                                                               return $ErrRcd
                                               }
                               }
                               return $Cred
                }
                #EndRegion
                $MsolCred = ReadCredentials $CredManagerTarget "GENERIC"
                $SecurePassword = ConvertTo-SecureString $MsolCred.CredentialBlob -AsPlainText -Force
                $MSOnlineCredentials = New-Object System.Management.Automation.PSCredential ($MsolCred.UserName, $SecurePassword)
                return $MSOnlineCredentials
}

Function Connect-Teams {

param (
    $creds,
    [ValidateSet("Connect","Disconnect")][string]$mode   
)

    switch ($mode) {

        "Connect" {

            try {
                Connect-MicrosoftTeams -Credential $creds
            }
            catch {
                $ErrorMessage = $_.Exception.Message
                $FailedItem = $_.Exception.ItemName
                write-host $ErrorMessage -ForegroundColor Red
				break;
            }
                   
        }
        "Disconnect" {
                try {
                    Disconnect-MicrosoftTeams
                }
                catch {
                }
        }
    }
}

###-------------------------------------###

##Update-Description for existing team
Function Update-Description {
    param (
        $TeamID,
        $_NewDescription
    )
    if ($_NewDescription) {
        try {
            Set-Team -GroupId $TeamID -Description $_NewDescription
            return $true
        }
        catch {
            return $false
        }
    }
}

##Update-DisplayName for existing team
Function Update-DisplayName {
    param (
        $TeamID,
        $_NewDisplayName
    )
    if ($_NewDisplayName) {
        try {
            Set-Team -GroupId $TeamID -DisplayName  $_NewDisplayName
            return $true
        }
        catch {
            return $false
        }
    }
}

##Add-TeamMember in existing Team
Function Add-TeamMember {
    param (
         $TeamID,
         [string[]]$_AddMembers
    )
    if (($_AddMembers).Count -gt 0) {
         foreach ($member in $_AddMembers -split ",") {

            if (!([string]::IsNullOrEmpty($member))) {
                try {  
                    Add-TeamUser -GroupId $TeamID -User $member -Role Member
                }
                catch {
                }
            }
        }
    }
}

##Remove-TeamMember from existing team
Function Remove-TeamMember {
    param (
         $TeamID,
         [string[]]$_RemoveMembers
    )
    if (($_RemoveMembers).Count -gt 0) {
         foreach ($member in $_RemoveMembers -split ",") {

            if (!([string]::IsNullOrEmpty($member))) {
                try {  
                    Remove-TeamUser -GroupId $TeamID -User $member
                }
                catch {
                }
            }
        }
    }
}

##Update-TeamOwner in existing team
Function Update-TeamOwner {
    param (
         $TeamID,
         $_NewOwner
    )
    if ($_NewOwner) {
        try {
            $CurrentOwner = Get-TeamUser -GroupId $TeamID -Role Owner
            Add-TeamUser -GroupId $TeamID -User $_NewOwner -Role Owner
            Remove-TeamUser -GroupId $TeamID -User $CurrentOwner.User -Role Owner
            
            return $true
        }
        catch {

            return $false
        }

    }
}

###-------------------------------------###

##Create-Team
Function Create-Team  {
	param (
		[psobject]$parameters   
	)
	
    $TeamsAccessType = "Private"

    try {
        $NewTeam = New-Team `
            -alias $parameters.TeamAlias `
            -displayname $parameters.TeamDisplayName `
            -Description $parameters.TeamDescription `
            -Owner $parameters.TeamOwner `
            -AccessType $TeamsAccessType
    }
    catch {
        $ErrorMessage = $_.Exception.Message
        $FailedItem = $_.Exception.ItemName
        write-host $ErrorMessage -ForegroundColor Red
        Break;
    }
    if ($NewTeam) {
        if ($parameters.TeamAddMembers) {
            Add-TeamMember -TeamID $NewTeam.GroupId -_AddMembers $parameters.TeamAddMembers
        }
        return $NewTeam
    }
}

##Update-Team
Function Update-Team  {
	param (
		[psobject]$parameters   
	)
    if (Get-Team | where {$_.GroupId -eq $parameters.TeamExistingObject}) {
        Update-DisplayName -TeamID $parameters.TeamExistingObject -_NewDisplayName $parameters.TeamDisplayName
        Update-Description -TeamID $parameters.TeamExistingObject -_NewDescription $parameters.TeamDescription
        Add-TeamMember -TeamID $parameters.TeamExistingObject -_AddMembers $parameters.TeamAddMembers
        Remove-TeamMember -TeamID $parameters.TeamExistingObject -_RemoveMembers $parameters.TeamRemoveMembers
        Update-TeamOwner -TeamID $parameters.TeamExistingObject -_NewOwner $parameters.TeamOwner
    }
}

##Delete-Team
Function Delete-Team  {
	param (
		[psobject]$parameters   
	)   
        try {
            Remove-Team -GroupId $parameters.TeamExistingObject
            return $true
        }
        catch {
            return $false
        }
}

###-------------------------------------###

Function Main {
    
    $parameters = [PSCustomObject]@{
        Credentials = $(Provide-Credentials -CredManagerTarget "admin_contoso2019")
        TeamDisplayName = $azure_sec_new_group_name
        TeamAlias = $azure_sec_new_group_name
        TeamDescription = $azure_sec_new_description
        TeamOwner = $azure_sec_new_owner
        TeamAddMembers = $azure_sec_group_add_members
        TeamRemoveMembers = $azure_sec_group_remove_members
        TeamExistingObject = $azure_sec_exist_group_name

    }

    Switch ($parameters.SiteAction)  {
    
        "Create" {
            Connect-Teams -creds $parameters.Credentials -mode Connect
            
            $newSite = Create-Team -parameters $parameters
            
            if ($NewSite) {
                write-output "Status: success"
            }

            Connect-Teams -creds $parameters.Credentials -mode Disconnect
        }
        "Modify" {
            Connect-Teams -creds $parameters.Credentials -mode Connect
            
            Update-Team -parameters $parameters

            Connect-Teams -creds $parameters.Credentials -mode Disconnect
        }
        "Delete" {

            Connect-Teams -creds $parameters.Credentials -mode Connect

             $DeleteSite = Delete-Team -parameters $parameters
             
             if  ($DeleteSite) {
                write-output "Status: success"  
             }
             Connect-Teams -creds $parameters.Credentials -mode Disconnect

        }
    }
}
Main
