param (
    [string]$azure_sec_new_group_name, 
    [string]$azure_sec_new_description,  
    [string]$azure_sec_new_owner,
    [string[]]$azure_sec_group_add_members,
    [string[]]$azure_sec_group_remove_members,
    [ValidateSet("Yes","No")][string][string]$new_send_copies_of_group_conversations,
    [ValidateSet("Yes","No")][string]$new_allow_external_senders,
    [string]$azure_sec_exist_group_name,
    [ValidateSet("Create","Modify","Delete")][string]$azure_sec_group_what_to_do
)


Function Provide-Credentials {
    try { 
        $user = ''
        $password = '' | ConvertTo-SecureString -AsPlainText -Force
        New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $user, $password
    }
    catch {
        $ErrorMessage = $_.Exception.Message
        $FailedItem = $_.Exception.ItemName
        write-host $ErrorMessage -ForegroundColor Red
		break;
    }
}

##Function to connect exchange online
Function Connect-EXO {

param (
    $cred,
    [ValidateSet("Connect","Disconnect")][string]$mode   
)
    $SessionName = "ExchangeOnline"

    switch ($mode) {

        "Connect" {
            If (Get-PSSession -Name $SessionName -ErrorAction SilentlyContinue) {
                Remove-PSSession -Name $SessionName
            }
			else {
                ##nothing to do here
			}

            try {
                #$sessionOption = New-PSSessionOption -SkipRevocationCheck
                #$proxysettings = New-PSSessionOption -ProxyAccessType IEConfig
                $Session = New-PSSession -Name $SessionName `
			        -ConfigurationName Microsoft.Exchange `
			        -ConnectionUri https://outlook.office365.com/powershell-liveid/ `
                    -Credential $cred  `
			        -Authentication Basic `
			        -AllowRedirection `
			        -ThrottleLimit 50 #-SessionOption $proxysettings                   
		        Import-PSSession $session -DisableNameChecking -Verbose -AllowClobber | out-null
            }
            catch {
                $ErrorMessage = $_.Exception.Message
                $FailedItem = $_.Exception.ItemName
                write-host $ErrorMessage -ForegroundColor Red
				break;
            }
                   
        }
        "Disconnect" {
            If (Get-PSSession -Name $SessionName -ErrorAction SilentlyContinue) {
                Remove-PSSession -Name $SessionName
            }
			else {
				##nothing to do here
			}
        }
    }
}

##Set Office 365 group name
Function Set-GroupName {

	param (
		[string]$_Identity,
		[string]$_NewDisplayName
	)    
    if (!([string]::IsNullOrEmpty($_NewDisplayName))) {
        
        try {
            Set-UnifiedGroup -Identity $_Identity -DisplayName $_NewDisplayName -WarningAction SilentlyContinue | Out-Null
        }
        catch {
            $ErrorMessage = $_.Exception.Message
            $FailedItem = $_.Exception.ItemName
            write-host $ErrorMessage -ForegroundColor Red
            continue;
        }   
    } 
}

##Function to update o365 group de
Function Set-GroupDescription {
	param (
		[string]$_Identity, 
		[string]$_NewGroupDescription	
	)
	
    if (!([string]::IsNullOrEmpty($_NewGroupDescription))) {
        
        try {
            Set-UnifiedGroup -Identity $_Identity -Notes $_NewGroupDescription -WarningAction SilentlyContinue | Out-Null
        }
        catch {
            $ErrorMessage = $_.Exception.Message
            $FailedItem = $_.Exception.ItemName
            write-host $ErrorMessage -ForegroundColor Red
        }  
    }  
}

Function Set-SendCopies {

	param (
		[string]$_Identity,
		[string]$_SendCopies    
	) 
        
    try {
        if ($_SendCopies -eq "Yes") {
            Set-UnifiedGroup -Identity $_Identity -AutoSubscribeNewMembers -WarningAction SilentlyContinue | Out-Null
        }
        else {
            Set-UnifiedGroup -Identity $_Identity -AutoSubscribeNewMembers:$false -WarningAction SilentlyContinue | Out-Null
        }
    }
	catch {
	    $ErrorMessage = $_.Exception.Message
		$FailedItem = $_.Exception.ItemName
		write-host $ErrorMessage -ForegroundColor Red
		Continue;
    }       
}            

##Configure group external communication
Function Set-ExternalCommunication {

	param (
		[string]$_Identity, 
		[string]$_AllowExternal    
	)     
	
    if ($_AllowExternal -eq "Yes") {
        $external = $false #Messages are accepted from authenticated (internal) and unauthenticated (external) senders.
    }
    else {
        $external = $true #Messages are accepted only from authenticated (internal) senders. Messages from unauthenticated (external) senders are rejected.
    }

    try {
        Set-UnifiedGroup -Identity $_Identity -RequireSenderAuthenticationEnabled $external -WarningAction SilentlyContinue | Out-Null
    }
    catch {
        $ErrorMessage = $_.Exception.Message
        $FailedItem = $_.Exception.ItemName
        write-host $ErrorMessage -ForegroundColor Red
        Continue;
    }   
}

##Set O365 group owner
Function Set-Owner  {

	param (
		[string]$_Identity,
		[string]$_NewOwner    
	)    
	
    if (!([string]::IsNullOrEmpty($_NewOwner))) {

        try {
            $CurrentOwner = Get-UnifiedGroupLinks -Identity $_Identity -LinkType Owners
            
			if ($CurrentOwner[0].ExternalDirectoryObjectId -ne $_NewOwner) {
                Add-UnifiedGroupLinks -Identity $_Identity -LinkType Members -Links $_NewOwner | Out-Null
                Add-UnifiedGroupLinks -Identity $_Identity -LinkType Owners -Links $_NewOwner  | Out-Null
                Remove-UnifiedGroupLinks -Identity $_Identity -LinkType Owners -Links $($CurrentOwner[0].PrimarySmtpAddress) -Confirm:$false | Out-Null
                Remove-UnifiedGroupLinks -Identity $_Identity -LinkType Members -Links $($CurrentOwner[0].PrimarySmtpAddress) -Confirm:$false | Out-Null
            }
        }
        catch {
            $ErrorMessage = $_.Exception.Message
            $FailedItem = $_.Exception.ItemName
            write-host $ErrorMessage -ForegroundColor Red
            COntinue;
        }    
    }
}

#Function to add member from O365Group
Function Add-Members  {

	param (
		[string]$_Identity,
		[string[]]$_AddMembers    
	)    
	
    if (($_AddMembers).Count -gt 0) {

        foreach ($member in $_AddMembers -split ",") {

            if (!([string]::IsNullOrEmpty($member))) {
                try {    
                    Add-UnifiedGroupLinks -Identity $_Identity -LinkType Members -Links $member | Out-Null    
                }  
                catch {
                    $ErrorMessage = $_.Exception.Message
                    $FailedItem = $_.Exception.ItemName
                    write-host $ErrorMessage -ForegroundColor Red
                    Continue;
                }
            }  
        }
    }
}

#Function to remove member from O365Group
Function Remove-Members  {

	param (
		[string]$_Identity,
		[string[]]$_RemoveMembers    
	)
	
    if (($_RemoveMembers).Count -gt 0) {

        foreach ($Member in $_RemoveMembers -split ",") {

            if (!([string]::IsNullOrEmpty($member))) {

                try {
                    Remove-UnifiedGroupLinks -Identity $_Identity -LinkType Members -Links $($Member.Trim()) -Confirm:$false
                }
                catch {
                    $ErrorMessage = $_.Exception.Message
                    $FailedItem = $_.Exception.ItemName
                    write-host $ErrorMessage -ForegroundColor Red
                    Continue;
                }
            }
        }
    }
}

#Functions for group managements options Create/Update/Delete

Function Create-Group  {
	param ( 
        [psobject]$parameters
	)
	
    #$GroupAlias = $_DisplayName.Replace(" ", "")
    #$GroupPrimarySMTP = $GroupAlias + "@" + "atriummortis.work"
    $GroupAccessType = "Private"
 
    try {
        $NewGroup = New-UnifiedGroup -DisplayName $($parameters.ObjectName) -AccessType $GroupAccessType  
    }
    catch {
        $ErrorMessage = $_.Exception.Message
        $FailedItem = $_.Exception.ItemName
        write-host $ErrorMessage -ForegroundColor Red
        Break;
    }
    if ($NewGroup.Id) {
        Set-GroupDescription -_Identity $($NewGroup.Id) -_NewGroupDescription $parameters.ObjectDescription
        Set-Owner -_Identity $($NewGroup.Id) -_NewOwner $parameters.ObjectOwner
        Add-Members -_Identity $($NewGroup.Id) -_AddMembers $parameters.ObjectAddMembers
        Set-SendCopies -_Identity $($NewGroup.Id) -_SendCopies $parameters.ObjectSendCopies
        Set-ExternalCommunication -_Identity $($NewGroup.Id) -_AllowExternal $parameters.ObjectExternal
    }
    return $NewGroup
}

Function Update-Group  {

	param (
        [psobject]$parameters
	)    
	
    Set-GroupName -_Identity $($parameters.ObjectExistingObject) -_NewDisplayName $parameters.ObjectName
    Set-GroupDescription -_Identity $($parameters.ObjectExistingObject) -_NewGroupDescription $parameters.ObjectDescription
    Set-Owner -_Identity $($parameters.ObjectExistingObject) -_NewOwner $parameters.ObjectOwner      
    Add-Members -_Identity $($parameters.ObjectExistingObject) -_AddMembers $parameters.ObjectAddMembers
    Remove-Members -_Identity $($parameters.ObjectExistingObject) -_RemoveMembers $parameters.objectRemoveMembers
    Set-SendCopies -_Identity $($parameters.ObjectExistingObject) -_SendCopies $($parameters.ObjectSendCopies)
    Set-ExternalCommunication -_Identity $($parameters.ObjectExistingObject) -_AllowExternal $parameters.ObjectExternal 
}

Function Remove-Group {

	param (
		[psobject]$parameters  
	)
	
    try {
        Remove-UnifiedGroup -Identity $($parameters.ObjectExistingObject) -Confirm:$false
    }
    catch {
        $ErrorMessage = $_.Exception.Message
        $FailedItem = $_.Exception.ItemName
        Write-host $ErrorMessage -ForegroundColor Red
        Continue;
    }    
    if ((get-unifiedGroup -Identity $($parameters.ObjectExistingObject) -ErrorAction SilentlyContinue)) {
        return $false
    }
    else {
        Return $true
    }
}

#Main script
Function Main {


    $parameters = [PSCustomObject]@{
        ObjectCredentials = $(Provide-Credentials)
        ObjectName = $("EXO_"+$azure_sec_new_group_name)
        ObjectAlias = $("EXO_"+$azure_sec_new_group_name)
        ObjectDescription = $azure_sec_new_description
        ObjectOwner = $azure_sec_new_owner
        ObjectAddMembers = $azure_sec_group_add_members
        ObjectSendCopies = $azure_sec_group_remove_members
        ObjectExternal = $new_allow_external_senders
        ObjectExistingObject = $azure_sec_exist_group_name
        ObjectAction = $azure_sec_group_what_to_do
    }

    Switch ($parameters.ObjectAction)  {
    
        "Create" {
		    Connect-EXO -cred $parameters.ObjectCredentials -mode "Connect"		
			$NewGroup = Create-Group -parameters $parameters
            
            if ($NewGroup) {
                    write-output "Status: success" 
            }
            Connect-EXO -cred $parameters.ObjectCredentials -mode "Disconnect"
        }
        "Modify" {
            Connect-EXO -cred $parameters.ObjectCredentials -mode "Connect"	
            $UpdateGroup = Update-Group -parameters $parameters
            if ($UpdateGroup) {
                write-output "Status: success"    
            }
            Connect-EXO -cred $parameters.ObjectCredentials -mode "Disconnect" 
        }
        "Delete" {
            Connect-EXO -cred $parameters.ObjectCredentials -mode "Connect"
            $DeleteGroup = Remove-Group -parameters $parameters
            if ($DeleteGroup) {
                write-output "Status: success"			
            }
            Connect-EXO -cred $parameters.ObjectCredentials -mode "Disconnect"               
        }
    }
}
Main