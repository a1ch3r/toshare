param (

    [ValidateSet("SharedMailbox", "RoomMailbox")][string]$type

)
$StartDTM = (get-date)
Write-Host $StartDTM -ForegroundColor Green
write-host "$type"
$global:ReportDate = "$((get-date).Year)"  + "-" + "$((Get-Culture).DateTimeFormat.GetMonthName((Get-Date).Month-1))"


. "C:\Create-ReportPath.ps1" -ReportName "MAILBOXES_ACTIVE"

$MailboxesFilePath = Get-ChildItem -Path $global:ReportFullPath | `
    Where-Object { $_.Name -match "_active" } | `
    Sort-Object -Property CreationTime -Descending | `
    Select-Object -Property FullName | Select-Object -First 1

write-host "Mailboxes path: $($MailboxesFilePath.FullName)" -ForegroundColor Cyan

$Mailboxes = Import-Csv -Path $($MailboxesFilePath.FullName) -Delimiter "|" | Where-Object { $_.RecipientTypeDetails -eq "$Type" } #| Select-Object -First 500

. "C:\Create-ReportPath.ps1" -ReportName "MAILBOXES_PERMISSIONS"

$MailBoxesPermissions = $global:ReportFullPath + "\" + "MAILBOXES_PERMISSIONS_$(($type).ToUpper())_" + $global:ReportDate + ".csv"
$MailBoxesPermissionsTemp = $global:ReportFullPath + "\" + "MAILBOXES_PERMISSIONS_$(($type).ToUpper())_TEMP_" + $global:ReportDate + ".csv"

write-host "Permissions path path: $MailBoxesPermissions" -ForegroundColor Cyan
write-host "Permissions temp path path: $MailBoxesPermissionsTemp" -ForegroundColor Cyan

# Define Credentials
[string]$userName = 'svc-reporting-365@domain.com'
[string]$passwordText = Get-Content 'C:\_SUPPORT\Secure.txt'

# Convert to secure string
[SecureString]$securePwd = $passwordText  | ConvertTo-SecureString 

# Create credential object
[PSCredential]$cred = New-Object System.Management.Automation.PSCredential -ArgumentList $userName, $securePwd

Connect-ExchangeOnline -Credential $cred -PageSize 5000

function Foreach-ObjectFast {
    param
    (
        [ScriptBlock]
        $Process,
    
        [ScriptBlock]
        $Begin,
    
        [ScriptBlock]
        $End
    )
  
    begin {
        # construct a hard-coded anonymous simple function from
        # the submitted scriptblocks:
        $code = @"
& {
  begin
  {
    $Begin
  }
  process
  {
    $Process
  }
  end
  {
    $End
  }
}
"@
        # turn code into a scriptblock and invoke it
        # via a steppable pipeline so we can feed in data
        # as it comes in via the pipeline:
        $pip = [ScriptBlock]::Create($code).GetSteppablePipeline()
        $pip.Begin($true)
    }
    process {
        # forward incoming pipeline data to the custom scriptblock:
        $pip.Process($_)
    }
    end {
        $pip.End()
    }
}

$scriptblock = {

    param($mailbox)

    $UserPrincipalName = $mailbox.UserPrincipalName
    $PrimarySmtpAddress = $mailbox.PrimarySmtpAddress
    $UsageLocation = $mailbox.UsageLocation
    
    $FullAccessPermissions = (Get-EXOMailboxPermission -Identity $UserPrincipalName | `
            Where-Object { ($_.AccessRights -contains "FullAccess") `
                -and ($_.IsInherited -eq $False) `
                -and -not ($_.User -match "NT AUTHORITY" -or $_.User -match "S-1-5-21" -or $_.User -match "NAMPR" -or $_.User -match "EURPR") }).User

    if ([string]$FullAccessPermissions -ne "") {
        
        $UserWithAccess = ""
        $AccessType = "FullAccess"
                        
        foreach ($FullAccessPermission in $FullAccessPermissions) {
            $UserWithAccess = $UserWithAccess + $FullAccessPermission
            if ($FullAccessPermissions.indexof($FullAccessPermission) -lt (($FullAccessPermissions.Count) - 1)) {
                $UserWithAccess = $UserWithAccess + ","
            }
        }
        $FA = @{"Identity" = $mailbox.DisplayName; "I_PrimarySmtpAddress" = $PrimarySmtpAddress; "I_UsageLocation" = $UsageLocation; "MailBoxType" = $MBType; "AccessRights" = $AccessType; "Trustee" = $UserWithAccess }
        $FA_Result = New-Object psobject -Property $FA 
        $FA_Result | Select-Object Identity, I_PrimarySmtpAddress, I_UsageLocation, Trustee, AccessRights 
        
        $FA = $null;
        $FA_Result = $null;
        [System.GC]::Collect();
    }

    #Getting SendAs permissions for mailbox
    $SendAsPermissions = (Get-EXORecipientPermission -Identity $UserPrincipalName | `
            Where-Object { `
                -not ($_.Trustee -match "NT AUTHORITY" -or $_.Trustee -match "S-1-5-21" -or $_.Trustee -match "NAMPR" -or $_.User -match "EURPR") }).Trustee

    if ([string]$SendAsPermissions -ne "") {
    
        $UserWithAccess = ""
        $AccessType = "SendAs"
    
        foreach ($SendAsPermission in $SendAsPermissions) {
            $UserWithAccess = $UserWithAccess + $SendAsPermission
            if ($SendAsPermissions.indexof($SendAsPermission) -lt (($SendAsPermissions.Count) - 1)) {
                $UserWithAccess = $UserWithAccess + ","
            }
        }
        $SA = @{"Identity" = $mailbox.DisplayName; "I_PrimarySmtpAddress" = $PrimarySmtpAddress; "I_UsageLocation" = $UsageLocation; "MailBoxType" = $MBType; "AccessRights" = $AccessType; "Trustee" = $UserWithAccess }
        $SA_Result = New-Object psobject -Property $SA 
        $SA_Result | Select-Object Identity, I_PrimarySmtpAddress, I_UsageLocation, Trustee, AccessRights 
    }
}           

write-host "Mailboxes imported: $($Mailboxes.Count) - type: $type" -ForegroundColor Cyan

$InitialSessionState = [system.management.automation.runspaces.initialsessionstate]::CreateDefault()
$RunspacePool = [runspacefactory]::CreateRunspacePool(1, 5, $InitialSessionState, $Host)
$RunspacePool.Open()
$Jobs = New-Object System.Collections.Generic.List[System.Object]

$mailboxes | Foreach-ObjectFast -Process {

    $PowershellThread = [PowerShell]::Create().AddScript($ScriptBlock).AddArgument($_)

    $PowershellThread.RunspacePool = $RunspacePool
    $Handle = $PowershellThread.BeginInvoke()
    $Job = "" | Select-Object Handle, Thread, object
    $Job.Handle = $Handle
    $Job.Thread = $PowershellThread
    $Job.Object = $m.UserPrincipalName
    $Jobs.Add($Job)
    [System.GC]::Collect();

}

$running = $true

While ($running) {
    $Running = $false
    $Jobs | ForEach-object { if ($_.Handle.IsCompleted -eq $True) {
            $_.Thread.EndInvoke($_.Handle) | Export-Csv -Path $MailBoxesPermissionsTemp  -Append -NoTypeInformation -Delimiter "|"
            $_.Thread.Dispose()
            $_.Thread = $Null
            $_.Handle = $Null
            [System.GC]::Collect();
        }
        elseif ((-not $Running) -and ($_.Handle -ne $Null)) {
            $Running = $true
        }
    }
}
$null = $RunspacePool.Close()
$null = $RunspacePool.Dispose()

Get-PSSession | Remove-PSSession

$result= Import-Csv -Path $MailBoxesPermissionsTemp  -Delimiter "|"

foreach ($r in $result)  {
    foreach ($t in $r.Trustee.Split(",")){
        [PSCustomObject]@{
            Identity = $r.Identity
            I_PrimarySmtpAddress = $r.I_PrimarySmtpAddress
            I_UsageLocation = $r.I_UsageLocation
            Trustee = $t
            AccessRights = $r.AccessRights
        } | Export-Csv -Path $MailBoxesPermissions -Delimiter "|" -NoTypeInformation -Append
    }
}

Remove-Item -Path $MailBoxesPermissionsTemp -Confirm:$false

$EndtDTM = (get-date)
Write-Host $EndtDTM -ForegroundColor Green
Write-Host "Minutes taken: $([math]::Round(($EndtDTM - $StartDTM).TotalMinutes,2))" -ForegroundColor Green
