[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls -bor [Net.SecurityProtocolType]::Tls11 -bor [Net.SecurityProtocolType]::Tls12

. "C:\_SUPPORT\Create-GlobalVariable.ps1"
. "C:\_SUPPORT\Create-ReportPath.ps1" -ReportName "OFFICE_USAGE"

$global:graphUrlReport = "$global:graphUrl/v1.0/reports/{0}({1})"

$usage_path_Exchange = $global:ReportFullPath + '\Exchange'
$usage_path_Groups = $global:ReportFullPath + '\Office 365 Groups'
$usage_path_OneDrive = $global:ReportFullPath + '\OneDrive'
$usage_path_Sharepoint = $global:ReportFullPath + '\Sharepoint'
$usage_path_Skype = $global:ReportFullPath + '\Skype'
$usage_path_Teams = $global:ReportFullPath + '\Teams'

if ((Test-Path $usage_path_Exchange) -eq $false) { New-Item -ItemType Directory $usage_path_Exchange | Out-Null }
if ((Test-Path $usage_path_Groups) -eq $false) { New-Item -ItemType Directory $usage_path_Groups | Out-Null }
if ((Test-Path $usage_path_OneDrive) -eq $false) { New-Item -ItemType Directory $usage_path_OneDrive | Out-Null }
if ((Test-Path $usage_path_Sharepoint) -eq $false) { New-Item -ItemType Directory $usage_path_Sharepoint | Out-Null }
if ((Test-Path $usage_path_Skype) -eq $false) { New-Item -ItemType Directory $usage_path_Skype | Out-Null }
if ((Test-Path $usage_path_Teams) -eq $false) { New-Item -ItemType Directory $usage_path_Teams | Out-Null }
#-------------------------------------

$reports = @(
    "getMailboxUsageUserDetail",
    "getOffice365GroupsActivityUserDetail",
    "getOneDriveUsageUserDetail",
    "getOneDriveActivityUserDetail",
    "getSharePointSiteUsageUserDetail",
    "getSkypeForBusinessActivityUserDetail",
    "GetTeamsDeviceUsageUserDetail",
    "GetTeamsDeviceUsageUserCounts",
    "GetTeamsDeviceUsagedistributionUserCounts",
    "GetTeamsUserActivityUserDetail",
    "GetTeamsUserActivityCounts",
    "GetTeamsUserActivityUserCounts"
)

Function Get-AuthToken ($clientId, $tenantId, $clientSecret) {

    # Construct URI
    $uri = "https://login.microsoftonline.com/$tenantId/oauth2/v2.0/token"

    # Construct Body
    $body = @{
        client_id     = $clientId
        scope         = "https://graph.microsoft.com/.default"
        client_secret = $clientSecret
        grant_type    = "client_credentials"
    }

    # Get OAuth 2.0 Token
    $tokenRequest = Invoke-WebRequest -Method Post -Uri $uri -ContentType "application/x-www-form-urlencoded" -Body $body -UseBasicParsing

    # Access Token
    $token = ($tokenRequest.Content | ConvertFrom-Json).access_token

    return $token
}

Function Get-MailboxUsageUserDetail($ReportingDays, $savefolder) {

    $reportname = 'getMailboxUsageDetail'
    $period = "period=`'D" + $ReportingDays + "`'"
    $URi = "$global:graphUrlReport" -f $reportname, $period
    $SavePath = $SaveFolder + '\EmailStorageMailbox_' + $ReportingDays + 'd.csv'
    $reportdata = Invoke-RestMethod -Uri "$URi" -Method Get -ContentType "application/json" -Headers @{Authorization = "Bearer $Token" }
    #Set up headers as per customer requirements
    $reportdata = $reportdata.Replace("ï»¿Report Refresh Date", "Data as of")
    $reportdata = $reportdata.Replace("User Principal Name", "User principal name")
    $reportdata = $reportdata.Replace("Display Name", "DisplayName")
    $reportdata = $reportdata.Replace("Is Deleted", "Deleted")
    $reportdata = $reportdata.Replace("Deleted Date", "Deleted date")
    $reportdata = $reportdata.Replace("Created Date", "CreatedDate")
    $reportdata = $reportdata.Replace("Last Activity Date", "Last activity date (UTC)")
    $reportdata = $reportdata.Replace("Item Count", "Item count")
    $reportdata = $reportdata.Replace("Storage Used (Byte)", "Storage used (B)")
    $reportdata = $reportdata.Replace("Issue Warning Quota (Byte)", "Issue warning quota (B)")
    $reportdata = $reportdata.Replace("Prohibit Send Quota (Byte)", "Prohibit send quota (B)")
    $reportdata = $reportdata.Replace("Prohibit Send/Receive Quota (Byte)", "Prohibit send/receive quota (B)")
    $reportdata = $reportdata.Replace("Report Period", "Reporting period in days")
    $reportdata = $reportdata.Replace(",", ";")


    $reportdata > $SavePath
    Write-Host "Mailbox Usage for $ReportingDays days was collected and saved to `n $SavePath"
}

Function Get-Office365GroupsActivityUserDetail($ReportingDays, $savefolder) {

    $reportname = 'getOffice365GroupsActivityDetail'
    $period = "period=`'D" + $ReportingDays + "`'"
    $URi = "$global:graphUrlReport" -f $reportname, $period
    $SavePath = $SaveFolder + '\GroupActivityReport_' + $ReportingDays + 'd.csv'
    $reportdata = Invoke-RestMethod -Uri "$URi" -Method Get -ContentType "application/json" -Headers @{Authorization = "Bearer $Token" }
    #Set up headers as per customer requirements
    $reportdata = $reportdata.Replace("ï»¿Report Refresh Date", "ContentDate")
    $reportdata = $reportdata.Replace("Group Display Name", "Group name")
    $reportdata = $reportdata.Replace("Is Deleted", "Deleted")
    $reportdata = $reportdata.Replace("Owner Principal Name", "Group owner")
    $reportdata = $reportdata.Replace("Last Activity Date", "Last activity date (UTC)")
    $reportdata = $reportdata.Replace("Group Type", "Type")
    $reportdata = $reportdata.Replace("Member Count", "Members")
    $reportdata = $reportdata.Replace("Guest Count", "Guests")
    $reportdata = $reportdata.Replace("Exchange Received Email Count", "Exchange emails received")
    $reportdata = $reportdata.Replace("SharePoint Active File Count", "SharePoint active files")
    $reportdata = $reportdata.Replace("Yammer Posted Message Count", "Yammer messages posted")
    $reportdata = $reportdata.Replace("Yammer Read Message Count", "Yammer messages read")
    $reportdata = $reportdata.Replace("Yammer Liked Message Count", "Yammer messages liked")
    $reportdata = $reportdata.Replace("Exchange Mailbox Total Item Count", "Exchange mailbox total items")
    $reportdata = $reportdata.Replace("Exchange Mailbox Storage Used (Byte)", "Exchange mailbox storage used (B)")
    $reportdata = $reportdata.Replace("SharePoint Total File Count", "SharePoint total files")
    $reportdata = $reportdata.Replace("SharePoint Site Storage Used (Byte)", "SharePoint site storage used (B)")
    $reportdata = $reportdata.Replace("Report Period", "Reporting period in days")
    $reportdata = $reportdata.Replace(",", ";")

    $reportdata > $SavePath
    Write-Host "Office 365 Groups usage for $ReportingDays days was collected and saved to `n $SavePath"
}

Function Get-OneDriveUsageUserDetail($ReportingDays, $savefolder) {

    $reportname = 'getOneDriveUsageAccountDetail'
    $period = "period=`'D" + $ReportingDays + "`'"
    $URi = "$global:graphUrlReport" -f $reportname, $period
    $SavePath = $SaveFolder + '\OneDriveSiteUsage_' + $ReportingDays + 'd.csv'
    $reportdata = Invoke-RestMethod -Uri "$URi" -Method Get -ContentType "application/json" -Headers @{Authorization = "Bearer $Token" }
    $reportdata = $reportdata.Replace("ï»¿Report Refresh Date", "Data as of")
    $reportdata = $reportdata.Replace("Owner Display Name", "Site owner")
    $reportdata = $reportdata.Replace("Is Deleted", "Deleted")
    $reportdata = $reportdata.Replace("Last Activity Date", "Last activity date (UTC)")
    $reportdata = $reportdata.Replace("File Count", "Files")
    $reportdata = $reportdata.Replace("Active File Count", "Files viewed or edited")
    $reportdata = $reportdata.Replace("Storage Used (Byte)", "Storage used (B)")
    $reportdata = $reportdata.Replace("Storage Allocated (Byte)", "Storage allocated (B)")
    $reportdata = $reportdata.Replace("Report Period", "Reporting period in days")
    $reportdata = $reportdata.Replace(",", ";")
    
    $reportdata > $SavePath
    Write-Host "OneDrive Site Usage for $ReportingDays days was collected and saved to `n $SavePath"      
}

Function Get-OneDriveActivityUserDetail($ReportingDays, $savefolder) {

    $reportname = 'getOneDriveActivityUserDetail'
    $period = "period=`'D" + $ReportingDays + "`'"
    $URi = "$global:graphUrlReport" -f $reportname, $period
    $SavePath = $SaveFolder + '\OneDriveUserActivity_' + $ReportingDays + 'd.csv'
    $reportdata = Invoke-RestMethod -Uri "$URi" -Method Get -ContentType "application/json" -Headers @{Authorization = "Bearer $Token" }
    $reportdata = $reportdata.Replace("ï»¿Report Refresh Date", "Data as of")
    $reportdata = $reportdata.Replace("User Principal Name", "User principal name")
    $reportdata = $reportdata.Replace("Is Deleted", "Deleted")
    $reportdata = $reportdata.Replace("Deleted Date", "Deleted date")
    $reportdata = $reportdata.Replace("Last Activity Date", "Last activity date (UTC)")
    $reportdata = $reportdata.Replace("Viewed Or Edited File Count", "Files viewed or edited")
    $reportdata = $reportdata.Replace("Synced File Count", "Files synced")
    $reportdata = $reportdata.Replace("Shared Internally File Count", "Files shared internally")
    $reportdata = $reportdata.Replace("Shared Externally File Count", "Files shared externally")
    $reportdata = $reportdata.Replace("Assigned Products", "Products assigned")
    $reportdata = $reportdata.Replace("Report Period", "Reporting period in days")
    $reportdata = $reportdata.Replace(",", ";")

    $reportdata > $SavePath
    Write-Host "OneDrive User Activity for $ReportingDays days was collected and saved to `n $SavePath"
}

Function Get-SharePointSiteUsageUserDetail($ReportingDays, $savefolder) {

    $reportname = 'getSharePointSiteUsageDetail'
    $period = "period=`'D" + $ReportingDays + "`'"
    $URi = "$global:graphUrlReport" -f $reportname, $period
    $SavePath = $SaveFolder + '\SharepointSiteUsage_' + $ReportingDays + 'd.csv'
    $reportdata = Invoke-RestMethod -Uri "$URi" -Method Get -ContentType "application/json" -Headers @{Authorization = "Bearer $Token" }
    $reportdata = $reportdata.Replace("ï»¿Report Refresh Date", "Data as of")
    $reportdata = $reportdata.Replace("Owner Display Name", "Site owner")
    $reportdata = $reportdata.Replace("Is Deleted", "Deleted")
    $reportdata = $reportdata.Replace("Last Activity Date", "Last activity date (UTC)")
    $reportdata = $reportdata.Replace("File Count", "Files")
    $reportdata = $reportdata.Replace("Active File Count", "Files viewed or edited")
    $reportdata = $reportdata.Replace("Page View Count", "Page views")
    $reportdata = $reportdata.Replace("Visited Page Count", "Pages visited")
    $reportdata = $reportdata.Replace("Storage Used (Byte)", "Storage used (B)")
    $reportdata = $reportdata.Replace("Storage Allocated (Byte)", "Storage allocated (B)")
    $reportdata = $reportdata.Replace("Root Web Template", "Root Web Template")
    $reportdata = $reportdata.Replace("Report Period", "Reporting period in days")
    $reportdata = $reportdata.Replace(",", ";")

    $reportdata > $SavePath
    Write-Host "Sharepoint Site usage for $ReportingDays days was collected and saved to `n $SavePath"
}

Function Get-SkypeForBusinessActivityUserDetail($ReportingDays, $savefolder) {

    $reportname = 'getSkypeForBusinessActivityUserDetail'
    $period = "period=`'D" + $ReportingDays + "`'"
    $URi = "$global:graphUrlReport" -f $reportname, $period
    $SavePath = $SaveFolder + '\SkypeUserActivitySummary_' + $ReportingDays + 'd.csv'
    $reportdata = Invoke-RestMethod -Uri "$URi" -Method Get -ContentType "application/json" -Headers @{Authorization = "Bearer $Token" }
    $reportdata = $reportdata.Replace("ï»¿Report Refresh Date", "Data as of")
    $reportdata = $reportdata.Replace("User Principal Name", "User principal name")
    $reportdata = $reportdata.Replace("Is Deleted", "Deleted")
    $reportdata = $reportdata.Replace("Deleted Date", "Deleted date")
    $reportdata = $reportdata.Replace(",Last Activity Date,", ",Last activity date (UTC),")
    $reportdata = $reportdata.Replace("Total Peer-to-peer Session Count", "P2P - total sessions")
    $reportdata = $reportdata.Replace("Total Organized Conference Count", "Conference organized - total sessions")
    $reportdata = $reportdata.Replace("Total Participated Conference Count", "Conference participated - total sessions")
    $reportdata = $reportdata.Replace("Peer-to-peer Last Activity Date", "P2P - last activity date")
    $reportdata = $reportdata.Replace("Organized Conference Last Activity Date", "Conference organized - last activity date")
    $reportdata = $reportdata.Replace("Participated Conference Last Activity Date", "Conference participated - Last activity date")
    $reportdata = $reportdata.Replace("Peer-to-peer IM Count", "P2P - IM")
    $reportdata = $reportdata.Replace("Peer-to-peer Audio Count", "P2P - audio")
    $reportdata = $reportdata.Replace("Peer-to-peer Audio Minutes", "P2P - audio minutes")
    $reportdata = $reportdata.Replace("Peer-to-peer Video Count", "P2P - video")
    $reportdata = $reportdata.Replace("Peer-to-peer Video Minutes", "P2P - video minutes")
    $reportdata = $reportdata.Replace("Peer-to-peer App Sharing Count", "P2P - app sharing")
    $reportdata = $reportdata.Replace("Peer-to-peer File Transfer Count", "P2P - file transfers")
    $reportdata = $reportdata.Replace("Organized Conference IM Count", "Conference organized - IM")
    $reportdata = $reportdata.Replace("Organized Conference Audio/Video Count", "Conference organized - audio/video")
    $reportdata = $reportdata.Replace("Organized Conference Audio/Video Minutes", "Conference organized - audio/video minutes")
    $reportdata = $reportdata.Replace("Organized Conference App Sharing Count", "Conference organized - app sharing")
    $reportdata = $reportdata.Replace("Organized Conference Web Count", "Conference organized - web")
    $reportdata = $reportdata.Replace("Organized Conference Dial-in/out 3rd Party Count", "Conference organized dial-in/out 3rd party")
    $reportdata = $reportdata.Replace("Organized Conference Dial-in/out Microsoft Count", "Conference organized dial-in/out Microsoft")
    $reportdata = $reportdata.Replace("Organized Conference Dial-in Microsoft Minutes", "Conference organized dial-in Microsoft minutes")
    $reportdata = $reportdata.Replace("Organized Conference Dial-out Microsoft Minutes", "Conference organized dial-out Microsoft minutes")
    $reportdata = $reportdata.Replace("Participated Conference IM Count", "Conference participated - IM")
    $reportdata = $reportdata.Replace("Participated Conference Audio/Video Count", "Conference participated - audio/video")
    $reportdata = $reportdata.Replace("Participated Conference Audio/Video Minutes", "Conference participated - audio/video minutes")
    $reportdata = $reportdata.Replace("Participated Conference App Sharing Count", "Conference participated - app sharing")
    $reportdata = $reportdata.Replace("Participated Conference Web Count", "Conference participated - web")
    $reportdata = $reportdata.Replace("Participated Conference Dial-in/out 3rd Party Count", "Conference participated - dial-in")
    $reportdata = $reportdata.Replace("Assigned Products", "Products assigned")
    $reportdata = $reportdata.Replace("Report Period", "Reporting period in days")
    $reportdata = $reportdata.Replace(",", ";")

    $reportdata > $SavePath
    Write-Host "Skype usage for $ReportingDays days was collected and saved to `n $SavePath"
}

Function Get-TeamsDeviceUsageUserDetail($ReportingDays, $savefolder) {

    $reportname = 'getTeamsDeviceUsageUserDetail'
    $period = "period=`'D" + $ReportingDays + "`'"
    $URi = "$global:graphUrlReport" -f $reportname, $period
    $SavePath = $SaveFolder + '\TeamsDeviceUsageUserDetail_' + $ReportingDays + 'd.csv'
    $reportdata = Invoke-RestMethod -Uri "$URi" -Method Get -ContentType "application/json" -Headers @{Authorization = "Bearer $Token" }
    $reportdata = $reportdata.Replace("ï»¿Report Refresh Date", "Data as of")
    $reportdata = $reportdata.Replace("User Principal Name", "User principal name")
    $reportdata = $reportdata.Replace(",Last Activity Date,", ",Last activity date (UTC),")
    $reportdata = $reportdata.Replace("Is Deleted", "Deleted")
    $reportdata = $reportdata.Replace("Deleted Date", "Deleted date")
    $reportdata = $reportdata.Replace("Used Web", "Used Web")
    $reportdata = $reportdata.Replace("Used Windows Phone", "Used Windows Phone")
    $reportdata = $reportdata.Replace("Used iOS", "Used iOS")
    $reportdata = $reportdata.Replace("Used Mac", "Used Mac")
    $reportdata = $reportdata.Replace("Used Android Phone", "Used Android Phone")
    $reportdata = $reportdata.Replace("Used Windows", "Used Windows")
    $reportdata = $reportdata.Replace("Report Period", "Reporting period in days")
    
    $reportdata > $SavePath
    Write-Host "Teams Device Usage User Detail usage for $ReportingDays days was collected and saved to `n $SavePath"
}

Function Get-TeamsDeviceUsageUserCounts($ReportingDays, $savefolder) {

    $reportname = 'getTeamsDeviceUsageUserCounts'
    $period = "period=`'D" + $ReportingDays + "`'"
    $URi = "$global:graphUrlReport" -f $reportname, $period
    $SavePath = $SaveFolder + '\TeamsDeviceUsageUserCounts_' + $ReportingDays + 'd.csv'
    $reportdata = Invoke-RestMethod -Uri "$URi" -Method Get -ContentType "application/json" -Headers @{Authorization = "Bearer $Token" }
    $reportdata = $reportdata.Replace("ï»¿Report Refresh Date", "Data as of")
    $reportdata = $reportdata.Replace("Web", "Web")
    $reportdata = $reportdata.Replace("Windows Phone", "Windows Phone")
    $reportdata = $reportdata.Replace("Android Phone", "Android Phone")
    $reportdata = $reportdata.Replace("iOS", "iOS")
    $reportdata = $reportdata.Replace("Mac", "Mac")
    $reportdata = $reportdata.Replace("Windows", "Windows")
    $reportdata = $reportdata.Replace("Report Date", "Report Date")
    $reportdata = $reportdata.Replace("Report Period", "Reporting period in days")
    
    $reportdata > $SavePath
    Write-Host "Teams Device Usage User Counts usage for $ReportingDays days was collected and saved to `n $SavePath"
}

Function Get-TeamsDeviceUsageDistributionUserCounts($ReportingDays, $savefolder) {

    $reportname = 'getTeamsDeviceUsageDistributionUserCounts'
    $period = "period=`'D" + $ReportingDays + "`'"
    $URi = "$global:graphUrlReport" -f $reportname, $period
    $SavePath = $SaveFolder + '\TeamsDeviceUsageDistributionUserCounts_' + $ReportingDays + 'd.csv'
    $reportdata = Invoke-RestMethod -Uri "$URi" -Method Get -ContentType "application/json" -Headers @{Authorization = "Bearer $Token" }
    $reportdata = $reportdata.Replace("ï»¿Report Refresh Date", "Data as of")
    $reportdata = $reportdata.Replace("Web", "Web")
    $reportdata = $reportdata.Replace("Windows Phone", "Windows Phone")
    $reportdata = $reportdata.Replace("Android Phone", "Android Phone")
    $reportdata = $reportdata.Replace("iOS", "iOS")
    $reportdata = $reportdata.Replace("Mac", "Mac")
    $reportdata = $reportdata.Replace("Windows", "Windows")
    $reportdata = $reportdata.Replace("Report Period", "Reporting period in days")
    
    $reportdata > $SavePath
    Write-Host "Teams Device Usage Distribution Use rCounts usage for $ReportingDays days was collected and saved to `n $SavePath"
}

Function Get-TeamsUserActivityUserDetail($ReportingDays, $savefolder) {

    $reportname = 'getTeamsUserActivityUserDetail'
    $period = "period=`'D" + $ReportingDays + "`'"
    $URi = "$global:graphUrlReport" -f $reportname, $period
    $SavePath = $SaveFolder + '\TeamsUserActivityUserDetail_' + $ReportingDays + 'd.csv'
    $reportdata = Invoke-RestMethod -Uri "$URi" -Method Get -ContentType "application/json" -Headers @{Authorization = "Bearer $Token" }
    $reportdata = $reportdata.Replace("ï»¿Report Refresh Date", "Data as of")
    $reportdata = $reportdata.Replace("User Principal Name", "User principal name")
    $reportdata = $reportdata.Replace(",Last Activity Date,", ",Last activity date (UTC),")
    $reportdata = $reportdata.Replace("Is Deleted", "Deleted")
    $reportdata = $reportdata.Replace("Deleted Date", "Deleted date")
    $reportdata = $reportdata.Replace("Assigned Products", "Assigned Products")
    $reportdata = $reportdata.Replace("Private Chat Message Count", "Private Chat Message Count")
    $reportdata = $reportdata.Replace("Call Count", "Call Count")
    $reportdata = $reportdata.Replace("Meeting Count", "Meeting Count")
    $reportdata = $reportdata.Replace("Has Other Action", "Has Other Action")
    $reportdata = $reportdata.Replace("Report Period", "Reporting period in days")

    $reportdata > $SavePath
    Write-Host "Teams User Activity User Detail usage for $ReportingDays days was collected and saved to `n $SavePath"
}

Function Get-TeamsUserActivityCounts($ReportingDays, $savefolder) {

    $reportname = 'getTeamsUserActivityCounts'
    $period = "period=`'D" + $ReportingDays + "`'"
    $URi = "$global:graphUrlReport" -f $reportname, $period
    $SavePath = $SaveFolder + '\TeamsUserActivityCounts_' + $ReportingDays + 'd.csv'
    $reportdata = Invoke-RestMethod -Uri "$URi" -Method Get -ContentType "application/json" -Headers @{Authorization = "Bearer $Token" }
    $reportdata = $reportdata.Replace("ï»¿Report Refresh Date", "Data as of")
    $reportdata = $reportdata.Replace("Report Date", "Report Date")
    $reportdata = $reportdata.Replace("Team Chat Messages", "Team Chat Messages")
    $reportdata = $reportdata.Replace("Private Chat Messages", "Private Chat Messages")
    $reportdata = $reportdata.Replace("Calls", "Calls")
    $reportdata = $reportdata.Replace("Meetings", "Meetings")
    $reportdata = $reportdata.Replace("Report Period", "Reporting period in days")
    
    $reportdata > $SavePath
    Write-Host "Teams User Activity Counts usage for $ReportingDays days was collected and saved to `n $SavePath"
}

Function Get-TeamsUserActivityUserCounts($ReportingDays, $savefolder) {

    $reportname = 'getTeamsUserActivityUserCounts'
    $period = "period=`'D" + $ReportingDays + "`'"
    $URi = "$global:graphUrlReport" -f $reportname, $period
    $SavePath = $SaveFolder + '\TeamsUserActivityUserCounts_' + $ReportingDays + 'd.csv'
    $reportdata = Invoke-RestMethod -Uri "$URi" -Method Get -ContentType "application/json" -Headers @{Authorization = "Bearer $Token" }
    $reportdata = $reportdata.Replace("ï»¿Report Refresh Date", "Data as of")
    $reportdata = $reportdata.Replace("Report Date", "Report Date")
    $reportdata = $reportdata.Replace("Team Chat Messages", "Team Chat Messages")
    $reportdata = $reportdata.Replace("Private Chat Messages", "Private Chat Messages")
    $reportdata = $reportdata.Replace("Calls", "Calls")
    $reportdata = $reportdata.Replace("Meetings", "Meetings")
    $reportdata = $reportdata.Replace("Other Actions", "Other Actions")
    $reportdata = $reportdata.Replace("Report Period", "Reporting period in days") 
    
    $reportdata > $SavePath
    Write-Host "Teams User Activity User Counts usage for $ReportingDays days was collected and saved to `n $SavePath"
}

$Token = Get-AuthToken -clientId $global:clientId -tenantId $global:tenantId -clientSecret $global:clientSecret

ForEach ($Name in $Reports) { 
    switch ($Name) {
        "getMailboxUsageUserDetail" {
            $SaveFolder = $usage_path_Exchange
            get-MailboxUsageUserDetail -savefolder $SaveFolder -ReportingDays "30"
            Start-Sleep -Seconds 15
            get-MailboxUsageUserDetail -savefolder $SaveFolder -ReportingDays "180"
            Start-Sleep -Seconds 30
        }
        "getOffice365GroupsActivityUserDetail" {
            $SaveFolder = $usage_path_Groups
            get-Office365GroupsActivityUserDetail -savefolder $SaveFolder -ReportingDays "30"
            Start-Sleep -Seconds 15
            get-Office365GroupsActivityUserDetail -savefolder $SaveFolder -ReportingDays "180"
            Start-Sleep -Seconds 30
        }
        "getOneDriveUsageUserDetail" {
            $SaveFolder = $usage_path_OneDrive
            get-OneDriveUsageUserDetail -savefolder $SaveFolder -ReportingDays "30"
            Start-Sleep -Seconds 15
            get-OneDriveUsageUserDetail -savefolder $SaveFolder -ReportingDays "180"
            Start-Sleep -Seconds 30
        }
        "getOneDriveActivityUserDetail" {
            $SaveFolder = $usage_path_OneDrive
            get-OneDriveActivityUserDetail -savefolder $SaveFolder -ReportingDays "30"
            Start-Sleep -Seconds 15
            get-OneDriveActivityUserDetail -savefolder $SaveFolder -ReportingDays "180"
            Start-Sleep -Seconds 30
        }
        "getSharePointSiteUsageUserDetail" {
            $SaveFolder = $usage_path_Sharepoint
            get-SharePointSiteUsageUserDetail -savefolder $SaveFolder -ReportingDays "30"
            Start-Sleep -Seconds 15
            get-SharePointSiteUsageUserDetail -savefolder $SaveFolder -ReportingDays "180"
            Start-Sleep -Seconds 30
        }
        "getSkypeForBusinessActivityUserDetail" {
            $SaveFolder = $usage_path_Skype
            get-SkypeForBusinessActivityUserDetail -savefolder $SaveFolder -ReportingDays "30"
            Start-Sleep -Seconds 15
            get-SkypeForBusinessActivityUserDetail -savefolder $SaveFolder -ReportingDays "180"
            Start-Sleep -Seconds 30
        }
        "GetTeamsDeviceUsageUserDetail" {
            $SaveFolder = $usage_path_Teams
            Get-TeamsDeviceUsageUserDetail -savefolder $SaveFolder -ReportingDays "30"
            Start-Sleep -Seconds 15
            Get-TeamsDeviceUsageUserDetail -savefolder $SaveFolder -ReportingDays "180"
            Start-Sleep -Seconds 30
        }
        "GetTeamsDeviceUsageUserCounts" {
            $SaveFolder = $usage_path_Teams
            Get-TeamsDeviceUsageUserCounts -savefolder $SaveFolder -ReportingDays "30"
            Start-Sleep -Seconds 15
            Get-TeamsDeviceUsageUserCounts -savefolder $SaveFolder -ReportingDays "180"
            Start-Sleep -Seconds 30
        }
        "GetTeamsDeviceUsageDistributionUserCounts" {
            $SaveFolder = $usage_path_Teams
            Get-TeamsDeviceUsageDistributionUserCounts -savefolder $SaveFolder -ReportingDays "30"
            Start-Sleep -Seconds 15
            Get-TeamsDeviceUsageDistributionUserCounts -savefolder $SaveFolder -ReportingDays "180"
            Start-Sleep -Seconds 30
        }
        "GetTeamsUserActivityUserDetail" {
            $SaveFolder = $usage_path_Teams
            Get-TeamsUserActivityUserDetail -savefolder $SaveFolder -ReportingDays "30"
            Start-Sleep -Seconds 15
            Get-TeamsUserActivityUserDetail -savefolder $SaveFolder -ReportingDays "180"
            Start-Sleep -Seconds 30
        }
        "GetTeamsUserActivityCounts" {
            $SaveFolder = $usage_path_Teams
            Get-TeamsUserActivityCounts -savefolder $SaveFolder -ReportingDays "30"
            Start-Sleep -Seconds 15
            Get-TeamsUserActivityCounts -savefolder $SaveFolder -ReportingDays "180"
            Start-Sleep -Seconds 30
        }
        "getTeamsUserActivityUserCounts" {
            $SaveFolder = $usage_path_Teams
            Get-TeamsUserActivityUserCounts -savefolder $SaveFolder -ReportingDays "30"
            Start-Sleep -Seconds 15
            Get-TeamsUserActivityUserCounts -savefolder $SaveFolder -ReportingDays "180"
        }
        "default" { break }
    }
}   