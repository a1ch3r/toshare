$global:Credentials = Get-Credential;
$global:Tenant = "domain";
$global:Domain = "domain.com"

###----Connect Functions---###
Function Connect-OFFICE
{
    [CmdletBinding()]
    [Alias()]
    [OutputType([int])]
    Param
    (
        [Parameter(Mandatory=$false,
                   ValueFromPipelineByPropertyName=$true,
                   Position=0)]
        $CloudCred
    )

    Begin
    {
    }
    Process
    {
        try {
            Write-Host "Trying to connect to Azure AD" -ForegroundColor Yellow
            Connect-MsolService -ErrorAction SilentlyContinue #-Credential $CloudCred
            Write-Host "Successfully connecte to Azure AD" -ForegroundColor Green
        }        
        catch {
            $ErrorMessage = $_.Exception.Message
            $FailedItem = $_.Exception.ItemName
            $ErrorMessage
            break; 
        }
    }
    End
    {
    }
}
Function Connect-EXO
{
    [CmdletBinding()]
    [Alias()]
    [OutputType([int])]
    Param
    (
        # Param1 help description
        [Parameter(Mandatory=$false,
                   ValueFromPipelineByPropertyName=$true,
                   Position=0)]
        $CloudCred
    )

    Begin
    {
        $sessionOption = New-PSSessionOption -SkipRevocationCheck
        $ModuleName = (Get-ChildItem -Path $($env:LOCALAPPDATA+"\Apps\2.0\") -Filter Microsoft.Exchange.Management.ExoPowershellModule.dll -Recurse ).FullName|?{$_ -notmatch "_none_"} | select -First 1
    }
    Process
    {
        if ($ModuleName) {
            Import-Module $ModuleName

            try {
                Write-Host "Trying to connect to Exchange Online" -ForegroundColor Yellow
                Import-Module $ModuleName
                $EXOSession = New-ExoPSSession -PSSessionOption $proxysettings  #-UserPrincipalName $CloudCred.UserName
                Import-PSSession $EXOSession –AllowClobber 
            }
            catch {
                $ErrorMessage = $_.Exception.Message
                $FailedItem = $_.Exception.ItemName
                write-host $ErrorMessage -ForegroundColor Red
                break;
            }
        }
        else {
            break;
        }
    }
    End
    {
    }
}
Function Connect-SPO
{
    [CmdletBinding()]
    [Alias()]
    [OutputType([int])]
    Param
    (
        [Parameter(Mandatory=$false,
                   ValueFromPipelineByPropertyName=$true,
                   Position=0)]
        $CloudCred,
        
        [Parameter(Mandatory=$true,
                   ValueFromPipelineByPropertyName=$true,
                   Position=0)]
        $Tenant
    )

    Begin
    {
        #$Tenant = [regex]::matches($CloudCred.UserName,'(?<=\@).+?(?=\.)').value
        $AdminURL = "https://" + $Tenant +"-admin.sharepoint.com"
        #$Personal="https://" + $Tenant +"-my.sharepoint.com/personal"
    }
    Process
    {
        try {
            Write-Host "Trying to connect to SharePoint Online" -ForegroundColor Yellow
            Connect-SPOService -Url $AdminURL #-Credential $CloudCred
            Write-Host "Successfully connecte to SharePoint Online" -ForegroundColor Green
        }
        catch {
            $ErrorMessage = $_.Exception.Message
            $FailedItem = $_.Exception.ItemName
            write-host $ErrorMessage -ForegroundColor Red
            break;            
        }
    }
    End
    {
    }
}
###---Connect Functions---###

###---Get Data Functions---###
Function Get-Users
{
[CmdletBinding()]
[Alias()]
[OutputType([int])]
    Param
    (
    )

    Begin
    {
        write-host "Start collecting users: $([system.datetime]::Now)"
    }
    Process
    {
        try {
            write-host "Trying to get users" -ForegroundColor Yellow

            Get-MsolUser -EnabledFilter EnabledOnly -All |  `
                where {
                    $_.UserPrincipalName.ToLower().EndsWith($global:Domain)
                } | `
	            select DisplayName, UserPrincipalName, SignInName, UsageLocation, isLicensed

            write-host "Users has been collected" -ForegroundColor Green
        }
        catch {
            $ErrorMessage = $_.Exception.Message
            $FailedItem = $_.Exception.ItemName
            $ErrorMessage
            break; 
        }
    }
    End
    {
        write-host "Finish collecting users: $([system.datetime]::Now)"
    }
}
Function Get-Recipients
{
[CmdletBinding()]
[Alias()]
[OutputType([int])]
    Param
    (
    )

    Begin
    {
         write-host "Start collecting recipients: $([system.datetime]::Now)"
    }
    Process
    {
        try {
            write-host "Trying to get recipients" -ForegroundColor Yellow

            Get-Recipient -ResultSize unlimited  | `
                where {
                    ($_.CustomAttribute3 -eq "CT") -or ($_.CustomAttribute3 -eq "EMPL")
                } | `
                Select DisplayName, PrimarySmtpAddress, WindowsLiveID, CustomAttribute3

                write-host "Recipients has been collected" -ForegroundColor Green
        } 
        catch {
            $ErrorMessage = $_.Exception.Message
            $FailedItem = $_.Exception.ItemName
            break;     
        }
    }
    End
    {
        write-host "Finish collecting recipients: $([system.datetime]::Now)"
    }
}
Function Get-Sites
{
[CmdletBinding()]
[Alias()]
[OutputType([int])]
    Param
    (
    )

    Begin
    {
         write-host "Start collecting sites: $([system.datetime]::Now)"
    }
    Process
    {
        try {

            write-host "Trying to get sites" -ForegroundColor Green

            Get-SPOSite -Template "SPSPERS#10" -limit ALL -includepersonalsite $True  | `
                where {
                    $_.url.ToLower().EndsWith($global:Domain.Replace(".","_"))
                } |`
                Select URL

            write-host "Sites has been collected" -ForegroundColor Green
        }
        catch {
            $ErrorMessage = $_.Exception.Message
            $FailedItem = $_.Exception.ItemName
            break;     
        }
    }
    End
    {
        write-host "Finish collecting sites: $([system.datetime]::Now)"
    }
}
###---Get Data Functions---###



Function Main
{
    [CmdletBinding()]
    [Alias()]
    [OutputType([int])]
    Param
    (
        
    )

    Begin
    {
        $usersArray = @();
        $recipientsArray = @();
        $ownersArray = @();

        Connect-OFFICE #-CloudCred $global:Credentials
        Connect-EXO #-CloudCred $global:Credentials
        Connect-SPO -Tenant $global:Tenant #-CloudCred $global:Credentials

    }
    Process
    {
        $AllUsers = Get-Users #| export-csv "C:\users-final12.csv" -NoTypeInformation
        $AllRecipients =  Get-Recipients #| export-csv "C:\recipients-final12.csv" -NoTypeInformation
        $AllSites = Get-Sites #| export-csv "C:\sites-final12.csv" -NoTypeInformation

        #$AllUsers = Import-Csv -Path "C:\users-final12.csv" 
        #$AllRecipients = Import-Csv -Path "C:\recipients-final12.csv"
        #$AllSites =  Import-Csv -Path "C:\sites-final12.csv"

        write-host "All RAI users: $($AllUsers.Count)";
        write-host "All recipient with CT and EMPL: $($AllRecipients.Count)";
        write-host "All RAI OneDrives: $($AllSites.Count)";

        $start = [system.datetime]::Now

        foreach ($user in $AllUsers) {
            $usersArray +=  $user.SignInName.ToLower()  
        }
        foreach ($site in $AllSites) {
            $ownersArray += $site.url.replace("https://$global:Tenant-my.sharepoint.com/personal/","").Replace("_eldiablo_xyz","@eldiablo.xyz")
        }


      foreach ($recipient in $AllRecipients) {   
            if($usersArray.indexof($recipient.WindowsLiveID.ToLower()) -ne "-1") {
                if ($ownersArray.IndexOf($recipient.WindowsLiveID.ToLower()) -ne "-1") {
                }
                else {
                    $recipient.WindowsLiveID
                    #Request-SPOPersonalSite -UserEmails $($recipient.WindowsLiveID) -Verbose -NoWait
                    #$SiteURL = "https://$global:Tenant-my.sharepoint.com/personal/" + ($recipient.WindowsLiveID).Replace("@eldiablo.xyz", "_eldiablo_xyz")
                    Set-SPOSite $SiteURL -SharingCapability Disabled
                    #Set-SPOSite “url for user’s OneDrive” -LoginName “c:0t.c|tenant|6f22ad45-181e-4efa-b3a0-e8cd3e842d46” -IsSiteCollectionAdmin $true
                }
            }
            else {  
            }   
        
    }
    End
    {
        $end = [system.datetime]::Now
        $result = ($end  - $start).TotalSeconds
    }
}
}
Main


#$AllUsers  = Import-Csv -Path "C:\users3.csv" #| select -First 500
#$AllRecipients = Import-Csv -Path "C:\recipient-final.csv" #|  select -First 500
#$AllSites = Import-Csv -Path "C:\sites3.csv"