[CmdletBinding()]
param (
    [Parameter()]
    [string]
    $MIM_UserPrincipalName = "Alexey_Chernikov@domainqa.onmicrosoft.com"
)

Function Get-AADUser() {

    <#
    .SYNOPSIS
    This function is used to get AAD Users from the Graph API REST interface
    .DESCRIPTION
    The function connects to the Graph API Interface and gets any users registered with AAD
    .EXAMPLE
    Get-AADUser
    Returns all users registered with Azure AD
    .EXAMPLE
    Get-AADUser -userPrincipleName user@domain.com
    Returns specific user by UserPrincipalName registered with Azure AD
    .NOTES
    NAME: Get-AADUser
    #>
    
    [cmdletbinding()]
    
    param
    (
        [Parameter(Mandatory = $true)]    
        $UserPrincipalName,
        [Parameter(Mandatory = $true)] 
        $Token
    )
    
    # Defining Variables
    $graphApiVersion = "v1.0"
    $User_resource = "users"
    $uri = "$global:graphUrl/$graphApiVersion/$($User_resource)/$($UserPrincipalName)"
    $Method = "GET"

    if ($userPrincipalName) {
        try {
            Get-GraphResult -Url $uri -Token $Token -Method $Method
        }
        catch {
            $ex = $_.Exception
            $errorResponse = $ex.Response.GetResponseStream()
            $reader = New-Object System.IO.StreamReader($errorResponse)
            $reader.BaseStream.Position = 0
            $reader.DiscardBufferedData()
            $responseBody = $reader.ReadToEnd();
            Write-Host "Response content:`n$responseBody" -f Red
            Write-Error "Request to $Uri failed with HTTP Status $($ex.Response.StatusCode) $($ex.Response.StatusDescription)"
            write-host
            break
        }
    }
}

Function Get-AADUserDevices() {

    <#
    .SYNOPSIS
    This function is used to get an AAD User Devices from the Graph API REST interface
    .DESCRIPTION
    The function connects to the Graph API Interface and gets a users devices registered with Intune MDM
    .EXAMPLE
    Get-AADUserDevices -UserID $UserID
    Returns all user devices registered in Intune MDM
    .NOTES
    NAME: Get-AADUserDevices
    #>
    
    [cmdletbinding()]
    
    param
    (
        [Parameter(Mandatory = $true, HelpMessage = "UserID (guid) for the user you want to take action on must be specified:")]
        $UserID,
        [Parameter(Mandatory = $true)]
        $Token
    )
    
    # Defining Variables
    $graphApiVersion = "v1.0"
    $Resource = "users/$UserID/managedDevices"

    $uri = "$global:graphUrl/$graphApiVersion/$($Resource)"
    $Method = "GET"
   
    Get-GraphResult -Url $uri -Token $Token -Method $Method

}

Function Invoke-DeviceAction() {

    <#
    .SYNOPSIS
    This function is used to set a generic intune resources from the Graph API REST interface
    .DESCRIPTION
    The function connects to the Graph API Interface and sets a generic Intune Resource. IN this example retire procedure will be executed.
    .EXAMPLE
    Invoke-DeviceAction -DeviceID $DeviceID
    Resets a managed device passcode
    .NOTES
    NAME: Invoke-DeviceAction
    #>
    
    [cmdletbinding()]
    
    param
    (
        [Parameter(Mandatory = $true, HelpMessage = "DeviceId (guid) for the Device you want to take action on must be specified:")]
        $DeviceID,
        [Parameter(Mandatory = $true)]
        $Token,
        [Parameter(Mandatory = $false)]
        $Action = "retire"
    )
      
    try {

        # Defining Variables
        $graphApiVersion = "v1.0"
        $Resource = "deviceManagement/managedDevices/$DeviceID/$Action"
        $uri = "$global:graphUrl/$graphApiVersion/$($resource)"
        $Method = "POST"
        Get-GraphResult -Url $uri -Token $Token -Method $Method
    
    }
        
    catch {
    
        $ex = $_.Exception
        $errorResponse = $ex.Response.GetResponseStream()
        $reader = New-Object System.IO.StreamReader($errorResponse)
        $reader.BaseStream.Position = 0
        $reader.DiscardBufferedData()
        $responseBody = $reader.ReadToEnd();
        Write-Host "Response content:`n$responseBody" -f Red
        Write-Error "Request to $Uri failed with HTTP Status $($ex.Response.StatusCode) $($ex.Response.StatusDescription)"
        write-host
        break
    
    }
}
   
Function Main() {
    
    <#
    .SYNOPSIS
    This is main function of the script
    .DESCRIPTION
    .EXAMPLE
    .NOTES
    #>

    . "C:\_REMOTE_WIPE\Get-GraphCredential.ps1" #separate script to get MS Graph credential, i.e. application ID
    . "C:\_REMOTE_WIPE\Get-GraphToken.ps1" #separate script to get MS Graph token
    . "C:\_REMOTE_WIPE\Get-GraphResult.ps1" #separate script to get MS Graph results for each actions


    $Token = Get-GraphToken -AppId $global:clientId -AppSecret $global:clientSecret -TenantID $global:tenantId 

    if ($Token.access_token) {       
        $UserInfo = Get-AADUser -userPrincipalName $MIM_UserPrincipalName -token $Token #get user from AAD  
    }

    if ($UserInfo.Id) {
        #If user is found, get his devices
        $DeviceInfo = Get-AADUserDevices -UserID $UserInfo.id -Token $Token
    }
    if ($DeviceInfo.value.id) {
        #If devices are found, execute retire procedure
        foreach ($DeviceID in $($DeviceInfo.value.id)) {
            Invoke-DeviceAction -DeviceID $DeviceID -Token $Token
        }
    }
}