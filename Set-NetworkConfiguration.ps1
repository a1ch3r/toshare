<#
.SYNOPSIS
.DESCRIPTION
    This function is used for writing logs on every operation
.PARAMETER Path
    Parameters examples:

.PARAMETER LiteralPath
.EXAMPLE
.NOTES
    Author: Alexey Chernikov
    Date:   August, 2017    
#>
 
function Write-Log { 
    [CmdletBinding()] 
    Param 
    ( 
        [Parameter(Mandatory=$true, 
                   ValueFromPipelineByPropertyName=$true)] 
        [ValidateNotNullOrEmpty()] 
        [Alias("LogContent")] 
        [string]$Message, 
 
        [Parameter(Mandatory=$false)] 
        [Alias('LogPath')] 
        [string]$Path='C:\Logs\PowerShellLog.log', 
         
        [Parameter(Mandatory=$false)] 
        [ValidateSet("Error","Warn","Info")] 
        [string]$Level="Info", 
         
        [Parameter(Mandatory=$false)] 
        [switch]$NoClobber 
    ) 
    Begin 
    { 
        # Set VerbosePreference to Continue so that verbose messages are displayed. 
        $VerbosePreference = 'Continue' 
    } 
    Process 
    { 
         
        # If the file already exists and NoClobber was specified, do not write to the log. 
        if ((Test-Path $Path) -AND $NoClobber) { 
            #Write-Error "Log file $Path already exists, and you specified NoClobber. Either delete the file or specify a different name." 
            Return 
            } 
 
        # If attempting to write to a log file in a folder/path that doesn't exist create the file including the path. 
        elseif (!(Test-Path $Path)) { 
            #Write-Verbose "Creating $Path." 
            $NewLogFile = New-Item $Path -Force -ItemType File 
            } 
 
        else { 
            # Nothing to see here yet. 
            } 
 
        # Format Date for our Log File 
        $FormattedDate = Get-Date -Format "yyyy-MM-dd HH:mm:ss" 
        # Write message to error, warning, or verbose pipeline and specify $LevelText 
        switch ($Level) { 
            'Error' { 
                #Write-Error $Message 
                $LevelText = 'ERROR:' 
                } 
            'Warn' { 
                #Write-Warning $Message 
                $LevelText = 'WARNING:' 
                } 
            'Info' { 
                #Write-Verbose $Message 
                $LevelText = 'INFO:' 
                } 
            } 
         
        # Write log entry to $Path 
        "$FormattedDate $LevelText $Message" | Out-File -FilePath $Path -Append 
    } 
    End 
    { 
    } 
}

<#
.SYNOPSIS
.DESCRIPTION
    This function is used to create static IP settings on remote computer.
.PARAMETER Path
    Parameters examples:

.PARAMETER LiteralPath
.EXAMPLE


.NOTES
    Author: Alexey Chernikov
    Date:   August, 2017    
#>
Function Change-RouterIP {
[CmdletBinding()]
    Param(
        [Parameter(Mandatory=$true, 
            ValueFromPipeline=$true,
            ValueFromPipelineByPropertyName=$true, 
            ValueFromRemainingArguments=$false, 
            Position=0)]
        [ValidateNotNull()]
        [Alias("pName")]
        [string]$Computername, 

        [Parameter(Mandatory=$true, 
            ValueFromPipeline=$true,
            ValueFromPipelineByPropertyName=$true, 
            ValueFromRemainingArguments=$false, 
            Position=1)]
        [ValidateNotNull()]
        [ValidateSet("StoreAAA", "StoreBBB", "StoreCCC")]
        [Alias("pInterface")]
        [string]$interface,

        [Parameter(Mandatory=$true, 
            ValueFromPipeline=$true,
            ValueFromPipelineByPropertyName=$true, 
            ValueFromRemainingArguments=$false, 
            Position=2)]
        [ValidateNotNull()]
        [Alias("pIP")]
        [string]$IP,

        [Parameter(Mandatory=$true, 
            ValueFromPipeline=$true,
            ValueFromPipelineByPropertyName=$true, 
            ValueFromRemainingArguments=$false, 
            Position=3)]
        [ValidateNotNull()]
        [Alias("pSubnet")]
        [string]$Subnet           
)
    $sb = {
        param($i,$int,$mask)
            # Retrieve the network adapter that should be configured
            $adapter = Get-NetAdapter -Name $int
            # Remove any existing IP, gateway from ipv4 adapter
            If (($adapter | Get-NetIPConfiguration).IPv4Address.IPAddress) {
                $adapter | Remove-NetIPAddress -AddressFamily "IPv4" -Confirm:$false
				Write-Log -message "Existing IP, gateway from ipv4 was removed" -level Warn
            }
            # Configure the IP address
            $adapter | New-NetIPAddress `
                -AddressFamily "IPv4" `
                -IPAddress $i `
                -PrefixLength $mask | Out-Null
			Write-Log -message "New IP setting has been added" -level Info	
    }
    if (Test-connection -ComputerName $Computername -Count 1) {
        write-host "Connection is established to computer:" $Computername -ForegroundColor Green
        try {
            invoke-command -ComputerName $Computername -ScriptBlock $sb -ArgumentList $IP, $interface, $Subnet
            write-host "Network settings has been configured!" -ForegroundColor Cyan
			Write-Log -message "Network settings has been configured" -level Info
        }
        catch {
            Write-host $error.Exception -ForegroundColor Red;
			Write-Log -message $error.Exception -level Error
        }
     }
     else {
        write-host "Can't connect to computer:" $Computername -ForegroundColor Red
		Write-Log -message "Can't connect to computer: "+$Computername -level Warn
     }
}

<#
.SYNOPSIS
.DESCRIPTION
    This function is used to change DHCP server settings
.PARAMETER Path
    Parameters examples:

.PARAMETER LiteralPath
.EXAMPLE

    .\
.NOTES
    Author: Alexey Chernikov
    Date:   August, 2017     
#>
Function Change-StoreDHCP {
[CmdletBinding()]
    Param(
        [Parameter (Mandatory = $true)]
        [ValidateNotNull()]
        [Alias("pName")]
        [string]$Computername, 

        [Parameter (Mandatory = $true)]
        [ValidateNotNull()]
        [Alias("pScope")]
        [string]$Scope, 
        
        [Parameter (Mandatory = $true)]
        [ValidateNotNull()]
        [Alias("pStartIP")]
        [string]$StartIP,
        
        [Parameter (Mandatory = $true)]
        [ValidateNotNull()]
        [Alias("pEndIP")]
        [string]$EndIP, 
        
        [Parameter (Mandatory = $true)]
        [ValidateNotNull()]
        [Alias("pSubnet")]
        [string]$Subnet
    )
    
	#Checking if the scope already exists
    $ex_scope=Get-DhcpServerv4Scope| ?{$_.Name -like "*$Scope*"}
    
	if($ex_Scope) {
		Remove-DhcpServerv4Scope -ScopeId $ex_scope.ScopeId
		Write-Log -message ("Scope with ScopeID "+$ex_scope.ScopeId+" was removed") -level Warn
    }
        
	#Creating new scope
    $ex_scope=Add-DhcpServerv4Scope -Name $Scope -StartRange $StartIP -EndRange $EndIP -SubnetMask $Subnet -State Active  -LeaseDuration 0.08:00:00 -PassThru
	Write-Log -message ("Scope with name "+$ex_scope.Name+" was created") -level Info
    
	ADD-DHCPserverv4ExclusionRange -ScopeId $Network -StartRange ((($StartIP -split "\.")[0..2] -join ".")+".100") -EndRange $EndIP
    ADD-DHCPserverv4ExclusionRange -ScopeID $Network -StartRange $StartIP -EndRange $StartIP	
	Write-Log -message ("Exclusion range configured") -level Info
	
    Set-DhcpServerv4OptionValue -Router $StartIP -ScopeId $ex_scope.ScopeId
	Write-Log -message ("Router ip "+$StartIP+" added.") -level Info
}

<#
.SYNOPSIS
.DESCRIPTION
    This function is used to install DHCP server on remote computer
.PARAMETER Path
    Parameters examples:

.PARAMETER LiteralPath
.EXAMPLE

.NOTES
    Author: Alexey Chernikov
    Date:   August, 2017    
#>
Function Install-DHCP {
[CmdletBinding()]
    Param(
		[Parameter(Mandatory=$true, 
			ValueFromPipeline=$true,
			ValueFromPipelineByPropertyName=$true, 
            ValueFromRemainingArguments=$false, 
            Position=0)]
        [ValidateNotNull()]
        [Alias(pName)]
        [string]$Computername,

        [Parameter(Mandatory=$true, 
			ValueFromPipeline=$true,
			ValueFromPipelineByPropertyName=$true, 
            ValueFromRemainingArguments=$false, 
            Position=1)]
        [ValidateNotNull()]
        [Alias(pIP)]
        [string]$IP
    )	  
    $sb = {
        param($Name,$Address)
            $FQDN = [System.Net.Dns]::GetHostByName($Name)
            $feature = Get-WindowsFeature -Name "DHCP"
            If (!$feature.Installed) {
				Install-WindowsFeature "DHCP" -IncludeManagementTools | out-null
				Write-Log -message ("DHCP Server has been installed on computer"+$Name+) -level Info
            }
           Add-DhcpServerInDC -DnsName $FQDN.HostName -IPAddress $Address
		   Write-Log -message ("DHCP Server has been authorised AD") -level Info
    }
	if (Test-connection -ComputerName $Computername -Count 1) {
        write-host "Connection is established to computer:" $Computername -ForegroundColor Green
        try {
            invoke-command -ComputerName $Computername -ScriptBlock $sb -ArgumentList $Computername, $IP
        }
        catch {
            Write-host $error.Exception -ForegroundColor Red;
			Write-Log -message $error.Exception -level Error
        }
     }
     else {
	    write-host "Can't connect to computer:" $Computername -ForegroundColor Red
		Write-Log -message "Can't connect to computer: "+$Computername -level Warn
     }
}	

###Main Logic for script###

Change-RouterIP `
    -pName "" `
    -pInterface "" `
    -pIP "" `
    -pSubnet ""

Install-DHCP `
	-pName "" `
	-pIP "" 
	

Change-StoreDHCP `
    -pName "" `
    -pScope "" `
    -pStartIP "" `
    -pEndIP "" `
    -pSubnet "" `

	
##Sample###	
	
<#	
[string]$DHCP_Computername = "Server1"
[string]$DHCP_Scope = "StoreAAA"
[string]$DHCP_Network = "172.22.75.0"
[string]$DHCP_Subnet = "255.255.255.0"
[string]$DHCP_StartIP = "172.22.75.1"
[string]$DHCP_EndIP = "172.22.75.254"	

Change-StoreDHCP `
	-ComputerName $DHCP_Computername `
	-Scope $DHCP_Scope `
	-StartIP $DHCP_StartIP `
	-EndIP $DHCP_EndIP `
	-Subnet $DHCP_Subnet
#>
