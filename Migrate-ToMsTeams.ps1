Set-Executionpolicy RemoteSigned
$global:tenantID = ".onmicrosoft.com"

#$global:username = ""
#$global:password = '' | convertto-securestring -AsPlainText -Force

$global:username = ""
$global:password = '' | convertto-securestring -AsPlainText -Force

$global:credentials = New-Object System.Management.Automation.PSCredential $global:username,$global:password
$global:Files = Get-ChildItem "C:\_POWERSHELL\_MigrateToTeams\Ring1-migration\_SPLIT_EXTENDED"
$global:LogFolder = "C:\_POWERSHELL\_MigrateToTeams\Ring1-migration\_LOGS"
#$global:usersToMigrate = import-csv -Path "C:\_POWERSHELL\_MigrateToTeams\Ring1-migration\_SPLIT\RING1-Migration-1.csv" -Delimiter "," | select -First 1

Function Connect-Azure ($creds, $tenant, $mfa){
    Import-module AzureAD
    
    if ($mfa) {
        Connect-AzureAD -TenantId $tenant
    }
    else {
        Connect-AzureAD -TenantId $tenant -Credential $creds   
    }
    write-host "Connected to Azure"
}

Function Connect-S4BOnline ($creds, $mfa) {
    Import-Module "C:\\Program Files\\Common Files\\Skype for Business Online\\Modules\\SkypeOnlineConnector\\SkypeOnlineConnector.psd1"
    
	if ($mfa) {
        $sfbSession = New-CsOnlineSession    
    }
    else {
        $sfbSession = New-CsOnlineSession -Credential $creds
    }
    Import-PSSession $sfbSession -AllowClobber
    write-host "Connected to Skype for Business Online"
}

Function Assign-License ($AzureUser, $License){

	try {
        #Set-AzureADUserLicense -ObjectId "$($AzureADUser.ObjectId)" -AssignedLicenses $LicensesToAssign
		Set-AzureADUserLicense -ObjectId $($AzureUser.ObjectId) -AssignedLicenses $License
        #Write-Host "--AC license successfully assigned to $($AzureUser.UserPrincipalName)" -ForegroundColor yellow
        Write-Log -Message "AC license successfully assigned to $($AzureUser.UserPrincipalName)" -Path $global:logs
    }
    catch {
		#write-host "Error while assigning license to user $($AzureUser.UserPrincipalName)"
        #write-host "$($error[0].Exception.Message)"
        Write-Log -Message "Error while assigning license to user $($AzureUser.UserPrincipalName)" -Path $global:LogsErrors -Level Error
        Write-Log -Message "$($error[0].Exception.Message)" -Path $global:LogsErrors
        continue;
    }
}

Function Assign-CsTeamsUpgradePolicy ($AzureUser, $CsTeamsUpgradePolicy) {
	
	try {
        Grant-CsTeamsUpgradePolicy -PolicyName $($CsTeamsUpgradePolicy) -Identity $($AzureUser.UserPrincipalName) -WarningAction SilentlyContinue -InformationAction SilentlyContinue -ErrorAction SilentlyContinue
        #Write-Host "Policy $($CsTeamsUpgradePolicy) has been successfully assigned to $($AzureUser.UserPrincipalName)" -ForegroundColor Green
        Write-Log -Message "Policy $($CsTeamsUpgradePolicy) has been successfully assigned to $($AzureUser.UserPrincipalName)" -Path $global:logs
    }
    catch {
		#write-host "Error while migrating user to $($CsTeamsUpgradePolicy) $($AzureUser.UserPrincipalName)"
        #write-host "$($error[0].Exception.Message)"
        Write-Log -Message "Error while migrating  $($AzureUser.UserPrincipalName) to $($CsTeamsUpgradePolicy)" -Path $global:LogsErrors -Level Error
        Write-Log -Message "$($error[0].Exception.Message)" -Path $global:LogsErrors
        continue;
    }
}

Function Assign-CsTeamsMeetingPolicy ($AzureUser, $CsTeamsMeetingPolicy) {
	try {
        Grant-CsTeamsMeetingPolicy -identity $($AzureUser.UserPrincipalName) -PolicyName $CsTeamsMeetingPolicy
        #Write-Host "Policy $($CsTeamsMeetingPolicy) has been successfully assigned to $($AzureUser.UserPrincipalName)" -ForegroundColor Green
        Write-Log -Message "Policy $($CsTeamsMeetingPolicy) has been successfully assigned to $($AzureUser.UserPrincipalName)" -Path $global:logs
    }
    catch {
        #write-host "Error while assigning policy $($CsTeamsMeetingPolicy) to user $($AzureUser.UserPrincipalName)"
        #write-host "$($error[0].Exception.Message)"
        Write-Log -Message "Error while assigning policy $($CsTeamsMeetingPolicy) to user $($AzureUser.UserPrincipalName)" -Path $global:LogsErrors -Level Error
        Write-Log -Message "$($error[0].Exception.Message)" -Path $global:LogsErrors
		continue;
    }
}

Function Assign-CsTeamsMeetingBroadcastPolicy ($AzureUser, $CsTeamsMeetingBroadcastPolicy) {
	try {
        Grant-CsTeamsMeetingBroadcastPolicy -identity $($AzureUser.UserPrincipalName) -PolicyName $($CsTeamsMeetingBroadcastPolicy)
        #Write-Host "Policy $($CsTeamsMeetingBroadcastPolicy) has been successfully assigned to $($AzureUser.UserPrincipalName)" -ForegroundColor Green
        Write-Log -Message "Policy $($CsTeamsMeetingBroadcastPolicy) has been successfully assigned to $($AzureUser.UserPrincipalName)" -Path $global:logs
    }
    catch {
		#write-host "Error while assigning policy $($CsTeamsMeetingBroadcastPolicy) to user $($AzureUser.UserPrincipalName)"
        #write-host "$($error[0].Exception.Message)"
        Write-Log -Message "Error while assigning policy $($CsTeamsMeetingBroadcastPolicy) to user $($AzureUser.UserPrincipalName)" -Path $global:LogsErrors -Level Error
        Write-Log -Message "$($error[0].Exception.Message)" -Path $global:LogsErrors
		continue;
    }
}

Function Assign-CsTeamsMessagingPolicy ($AzureUser, $CsTeamsMessagingPolicy) {
	try {
        Grant-CsTeamsMessagingPolicy -identity $($AzureUser.UserPrincipalName) -PolicyName $($CsTeamsMessagingPolicy)
        #Write-Host "Policy $($CsTeamsMessagingPolicy) has been successfully assigned to $($AzureUser.UserPrincipalName)" -ForegroundColor Green
        Write-Log -Message "Policy $($CsTeamsMessagingPolicy) has been successfully assigned to $($AzureUser.UserPrincipalName)" -Path $global:logs

    }
    catch {
		#write-host "Error while assigning policy $($CsTeamsMessagingPolicy) to user $($AzureUser.UserPrincipalName)"
        #write-host "$($error[0].Exception.Message)"
        Write-Log -Message "Error while assigning policy $($CsTeamsMessagingPolicy) to user $($AzureUser.UserPrincipalName)" -Path $global:LogsErrors -Level Error
        Write-Log -Message "$($error[0].Exception.Message)" -Path $global:LogsErrors
		continue;
    }
}

Function Write-Log {
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory=$true,
                   ValueFromPipelineByPropertyName=$true)]
        [ValidateNotNullOrEmpty()]
        [Alias("LogContent")]
        [string]$Message,

        [Parameter(Mandatory=$false)]
        [Alias('LogPath')]
        [string]$Path='C:\Logs\PowerShellLog.log',
        
        [Parameter(Mandatory=$false)]
        [ValidateSet("Error","Warn","Info")]
        [string]$Level="Info",
        
        [Parameter(Mandatory=$false)]
        [switch]$NoClobber
    )

    Begin
    {
        # Set VerbosePreference to Continue so that verbose messages are displayed.
        $VerbosePreference = 'Continue'
    }
    Process
    {
        
        # If the file already exists and NoClobber was specified, do not write to the log.
        if ((Test-Path $Path) -AND $NoClobber) {
            Write-Error "Log file $Path already exists, and you specified NoClobber. Either delete the file or specify a different name."
            Return
            }

        # If attempting to write to a log file in a folder/path that doesn't exist create the file including the path.
        elseif (!(Test-Path $Path)) {
            Write-Verbose "Creating $Path."
            $NewLogFile = New-Item $Path -Force -ItemType File
            }

        else {
            # Nothing to see here yet.
            }

        # Format Date for our Log File
        $FormattedDate = Get-Date -Format "yyyy-MM-dd HH:mm:ss"

        # Write message to error, warning, or verbose pipeline and specify $LevelText
        switch ($Level) {
            'Error' {
                Write-Error $Message
                $LevelText = 'ERROR:'
                }
            'Warn' {
                Write-Warning $Message
                $LevelText = 'WARNING:'
                }
            'Info' {
                Write-Verbose $Message
                $LevelText = 'INFO:'
                }
            }
        
        # Write log entry to $Path
        "$FormattedDate $LevelText $Message" | Out-File -FilePath $Path -Append
    }
    End
    {
    }
}

Function Main {

    Connect-Azure -creds $global:credentials -tenant $global:tenantID -mfa $false
	
	$_CsTeamsUpgradePolicy = "UpgradeToTeams"
    $_CsTeamsPolicyTemplate = "BAT-Teams-Migration"
    $_CsTeamsMeetingPolicy = $_CsTeamsPolicyTemplate + "-Meeting-Policy"
	$_CsTeamsMeetingBroadcastPolicy = $_CsTeamsPolicyTemplate + "-Events-Policy"
	$_CsTeamsMessagingPolicy = $_CsTeamsPolicyTemplate + "-Messaging-Policy"
	
	$License = New-Object -TypeName Microsoft.Open.AzureAD.Model.AssignedLicense
    $License.SkuId = "0c266dff-15dd-4b49-8397-2bb16070ed52"
    $LicensesToAssign = New-Object -TypeName Microsoft.Open.AzureAD.Model.AssignedLicenses
    $LicensesToAssign.AddLicenses = $License

    foreach ($file in $global:Files) {

        Get-PSSession | Remove-PSSession
        Connect-S4BOnline -creds $global:credentials -mfa $false   

        $global:Logs = "$($global:LogFolder)\$($file.BaseName)-extended.log"
        $global:LogsErrors = "$($global:LogFolder)\$($file.BaseName)-extended-Errors.log"
        Write-Host "Working with file $($file.FullName)" -ForegroundColor Cyan
        Write-Log -Message "BATCH 1" -Path $global:logs
        $usersToMigrate = $null;
        $usersToMigrate = import-csv -path $($file.FullName) -Delimiter "," #| select -First 10
        Write-Host "$($usersToMigrate.Count) in file $($file.FullName)" -ForegroundColor Green
        $i = 0;

        foreach ($userToMigrate in $usersToMigrate) {
            $i++
            $AzureADUser = $null;
            $AzureADUser = Get-AzureADUser -ObjectId $userToMigrate.'O365 account' -ErrorAction SilentlyContinue
            
            if ($AzureADUser.UserPrincipalName) {
                Write-Log -Message "Staring the migration process for $($AzureADUser.UserPrincipalName)" -Path $global:logs -Level Warn

                Assign-CsTeamsUpgradePolicy -AzureUser $AzureADUser -CsTeamsUpgradePolicy $_CsTeamsUpgradePolicy
			    Assign-CsTeamsMeetingPolicy -AzureUser $AzureADUser -CsTeamsMeetingPolicy $_CsTeamsMeetingPolicy
			    Assign-CsTeamsMeetingBroadcastPolicy -AzureUser $AzureADUser -CsTeamsMeetingBroadcastPolicy $_CsTeamsMeetingBroadcastPolicy
			    Assign-CsTeamsMessagingPolicy -AzureUser $AzureADUser -CsTeamsMessagingPolicy $_CsTeamsMessagingPolicy
			
			    if ($userToMigrate.'AC enablement' -eq "AC enablement") {		
				    Assign-License -AzureUser $AzureADUser -License $LicensesToAssign
                }
                else {
                }
                Write-Log -Message "Finishing the migration process for $($AzureADUser.UserPrincipalName)" -Path $global:logs -Level Warn	
		    }
            else {
                Write-Log -Message "$($userToMigrate) has not been found in organization" -Path $global:logs -Level Error	
            }
            if ($i % 250 -eq 0) {
                write-host $i
                Get-PSSession | Remove-PSSession
                Connect-S4BOnline -creds $global:credentials -mfa $false   
            }
        }
        Get-PSSession | Remove-PSSession
        Connect-S4BOnline -creds $global:credentials -mfa $false   
    }
}
Main